/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/
USE [master]
GO
/****** Object:  Database [PreventiveAsset]    Script Date: 3/20/2018 6:41:54 AM ******/
CREATE DATABASE [PreventiveAsset]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PreventiveAsset', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\PreventiveAsset.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PreventiveAsset_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\PreventiveAsset_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [PreventiveAsset] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PreventiveAsset].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PreventiveAsset] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PreventiveAsset] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PreventiveAsset] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PreventiveAsset] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PreventiveAsset] SET ARITHABORT OFF 
GO
ALTER DATABASE [PreventiveAsset] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PreventiveAsset] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PreventiveAsset] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PreventiveAsset] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PreventiveAsset] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PreventiveAsset] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PreventiveAsset] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PreventiveAsset] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PreventiveAsset] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PreventiveAsset] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PreventiveAsset] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PreventiveAsset] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PreventiveAsset] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PreventiveAsset] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PreventiveAsset] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PreventiveAsset] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PreventiveAsset] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PreventiveAsset] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PreventiveAsset] SET  MULTI_USER 
GO
ALTER DATABASE [PreventiveAsset] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PreventiveAsset] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PreventiveAsset] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PreventiveAsset] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PreventiveAsset] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PreventiveAsset] SET QUERY_STORE = OFF
GO
USE [PreventiveAsset]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [PreventiveAsset]
GO
/****** Object:  Table [dbo].[AssetIntervalInformation]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetIntervalInformation](
	[IntervalInformationID] [int] NOT NULL,
	[IKCode] [nvarchar](300) NULL,
	[Interval] [varchar](50) NULL,
	[MaintenanceDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_AssetIntervalInformation] PRIMARY KEY CLUSTERED 
(
	[IntervalInformationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IKCategory]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IKCategory](
	[IKCategory] [nvarchar](100) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[MasterIK] [nvarchar](300) NULL,
 CONSTRAINT [PK_IKCategory] PRIMARY KEY CLUSTERED 
(
	[IKCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IKCondition]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IKCondition](
	[IKConditionId] [int] IDENTITY(1,1) NOT NULL,
	[WorkOrderManualID] [nvarchar](100) NULL,
	[Condition] [varchar](50) NULL,
	[Remarks] [varchar](500) NULL,
	[IKCode] [nvarchar](300) NULL,
	[IKCategory] [nvarchar](100) NULL,
	[IKitemID] [int] NULL,
 CONSTRAINT [PK_IKCondition] PRIMARY KEY CLUSTERED 
(
	[IKConditionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IKConditionVendor]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IKConditionVendor](
	[IKConditionVendor] [int] IDENTITY(1,1) NOT NULL,
	[VendorName] [varchar](100) NULL,
	[StartedBy] [varchar](50) NULL,
	[WorkStartDate] [datetime] NULL,
	[WorkEndDate] [datetime] NULL,
	[CompletedBy] [varchar](50) NULL,
	[PICRemarks] [varchar](300) NULL,
	[WorkOrderManual] [nvarchar](100) NULL,
	[IKCode] [nvarchar](300) NULL,
	[IKCategory] [nvarchar](300) NULL,
 CONSTRAINT [PK_IKConditionVendor] PRIMARY KEY CLUSTERED 
(
	[IKConditionVendor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IKItem]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IKItem](
	[IKItem] [int] IDENTITY(1,1) NOT NULL,
	[IKCategory] [nvarchar](100) NULL,
	[Item] [varchar](300) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_IKItem] PRIMARY KEY CLUSTERED 
(
	[IKItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Interval]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Interval](
	[IntervalID] [int] IDENTITY(1,1) NOT NULL,
	[Interval] [varchar](300) NULL,
	[MaintenanceDate] [datetime] NULL,
	[IKCode] [nvarchar](300) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[AssetCategoryID] [nvarchar](300) NULL,
 CONSTRAINT [PK_Interval] PRIMARY KEY CLUSTERED 
(
	[IntervalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterAssetCategory]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterAssetCategory](
	[AssetCategoryID] [nvarchar](300) NOT NULL,
	[AssetGroup] [varchar](50) NULL,
	[Departement] [varchar](50) NULL,
	[AssetCategory] [varchar](50) NULL,
	[Description] [varchar](max) NULL,
	[Location] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[InstalmentDate] [datetime] NULL,
	[Lifetime] [int] NULL,
	[UserGroup] [nvarchar](50) NULL,
 CONSTRAINT [PK_MasterAssetCategory] PRIMARY KEY CLUSTERED 
(
	[AssetCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterAssetType]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterAssetType](
	[MasterAssetTypeID] [nvarchar](300) NOT NULL,
	[Description] [varchar](max) NULL,
	[AssetCategoryID] [nvarchar](300) NULL,
	[ReplacementDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[Lifetime] [varchar](50) NULL,
	[Replacement] [int] NULL,
	[InstallmentDate] [datetime] NULL,
 CONSTRAINT [PK_MasterAssetType] PRIMARY KEY CLUSTERED 
(
	[MasterAssetTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterIK]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterIK](
	[IKCode] [nvarchar](300) NOT NULL,
	[IKDescription] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
 CONSTRAINT [PK_MasterIK] PRIMARY KEY CLUSTERED 
(
	[IKCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterLocation]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterLocation](
	[LocationCode] [nvarchar](100) NOT NULL,
	[Subportofolio] [varchar](30) NULL,
	[Floor] [varchar](30) NULL,
	[LocationArea] [varchar](100) NULL,
	[Description] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
 CONSTRAINT [PK_MasterLocation] PRIMARY KEY CLUSTERED 
(
	[LocationCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterUser]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterUser](
	[UserID] [varchar](100) NOT NULL,
	[Password] [nvarchar](200) NULL,
	[Name] [varchar](300) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_MasterUser] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserGroupDetail]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroupDetail](
	[UserGroupDetailID] [int] IDENTITY(1,1) NOT NULL,
	[UserGroupID] [nvarchar](50) NULL,
	[UserID] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreratedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_UserGroupDetail] PRIMARY KEY CLUSTERED 
(
	[UserGroupDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserGroupHeader]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroupHeader](
	[UserGroupId] [nvarchar](50) NOT NULL,
	[UserGroupName] [varchar](50) NULL,
	[UserGroupLeader] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateBy] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_UserGroupHeader] PRIMARY KEY CLUSTERED 
(
	[UserGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkOrderManual]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkOrderManual](
	[WorkOderManualID] [nvarchar](100) NOT NULL,
	[WorkOrderDate] [datetime] NULL,
	[CategoryAssetID] [nvarchar](300) NULL,
	[IKCode] [nvarchar](300) NULL,
	[VendorName] [nvarchar](300) NULL,
	[WorkStartDate] [datetime] NULL,
	[StartedBy] [varchar](50) NULL,
	[WorkEndDate] [datetime] NULL,
	[CompleteBy] [varchar](50) NULL,
	[PICRemarks] [varchar](max) NULL,
	[Status] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[Subportopolio] [varchar](50) NULL,
	[CategoryDescription] [varchar](300) NULL,
	[WorkOrderLeader] [varchar](50) NULL,
	[AssetLocationCode] [nvarchar](100) NULL,
	[IKDescription] [varchar](300) NULL,
	[IKCategory] [varchar](100) NULL,
 CONSTRAINT [PK_WorkOrderManual] PRIMARY KEY CLUSTERED 
(
	[WorkOderManualID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkOrderManualMaintenance]    Script Date: 3/20/2018 6:41:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkOrderManualMaintenance](
	[MaintenanceID] [int] IDENTITY(1,1) NOT NULL,
	[AssetTypeID] [nvarchar](300) NULL,
	[WorkOrderManualID] [nvarchar](100) NULL,
	[Jumlah] [varchar](50) NULL,
	[Satuan] [varchar](50) NULL,
	[Remarks] [varchar](300) NULL,
	[maintenanceType] [varchar](50) NULL,
 CONSTRAINT [PK_WorkOrderManualMaintenance] PRIMARY KEY CLUSTERED 
(
	[MaintenanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'asdasd', NULL, NULL, NULL, NULL, 1, N'GEG-PM-00500')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'bbbb', NULL, NULL, NULL, NULL, 1, N'GEG-PM-00501')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'tesss', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'testestes', NULL, NULL, NULL, NULL, 1, N'GEG-PM-00501')
SET IDENTITY_INSERT [dbo].[IKCondition] ON 

INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1, N'xcx-xcx-2018-03-0069', N'bad-11', N'asd', NULL, N'bbbb', 11)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (2, N'xcx-xcx-2018-03-0070', N'bad-11', N'test', NULL, N'bbbb', 11)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (3, N'xcx-xcx-2018-03-0072', N'good', N'test', NULL, N'bbbb', 11)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (4, N'xcx-xcx-2018-03-0074', N'Bad', N'tes', NULL, N'bbbb', 11)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (5, N'xcx-xcx-2018-03-0074', N'Bad', N'tes', NULL, N'bbbb', 12)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (6, N'xcx-xcx-2018-03-0074', N'Good', N'', NULL, N'bbbb', 13)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (7, N'xcx-xcx-2018-03-0074', N'Good', N'', NULL, N'bbbb', 14)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (8, N'xcx-xcx-2018-03-0074', N'Good', N'', NULL, N'bbbb', 15)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (9, N'xcx-xcx-2018-03-0074', N'Good', N'', NULL, N'bbbb', 16)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (10, N'xcx-xcx-2018-03-0074', N'Good', N'', NULL, N'bbbb', 17)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (11, N'xcx-xcx-2018-03-0074', N'Good', N'', NULL, N'bbbb', 18)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (12, N'xcx-xcx-2018-03-0074', N'Good', N'test', NULL, N'bbbb', 8)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (13, N'xcx-xcx-2018-03-0074', N'Good', N'tes', NULL, N'bbbb', 9)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (14, N'xcx-xcx-2018-03-0074', N'Good', N'', NULL, N'bbbb', 10)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (81, N'xcx-xcx-2018-03-0075', N'Good', N'test', NULL, N'bbbb', 11)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (82, N'xcx-xcx-2018-03-0075', N'Bad', N'test', NULL, N'bbbb', 12)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (83, N'xcx-xcx-2018-03-0075', N'Good', N'ss', NULL, N'bbbb', 13)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (84, N'xcx-xcx-2018-03-0075', N'', N'', NULL, N'bbbb', 14)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (85, N'xcx-xcx-2018-03-0075', N'', N'', NULL, N'bbbb', 15)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (86, N'xcx-xcx-2018-03-0075', N'', N'', NULL, N'bbbb', 16)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (87, N'xcx-xcx-2018-03-0075', N'', N'', NULL, N'bbbb', 17)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (88, N'xcx-xcx-2018-03-0075', N'', N'', NULL, N'bbbb', 18)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (89, N'xcx-xcx-2018-03-0075', N'Good', N'', NULL, N'testestes', 8)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (90, N'xcx-xcx-2018-03-0075', N'Bad', N'test', NULL, N'testestes', 9)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (91, N'xcx-xcx-2018-03-0075', N'Bad', N'ss', NULL, N'testestes', 10)
SET IDENTITY_INSERT [dbo].[IKCondition] OFF
SET IDENTITY_INSERT [dbo].[IKConditionVendor] ON 

INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (1, N'test', N'ekky', CAST(N'2018-03-29T00:00:00.000' AS DateTime), CAST(N'2018-03-29T00:00:00.000' AS DateTime), NULL, N'test', N'xcx-xcx-2018-03-0070', NULL, N'bbbb')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (2, N'tes', N'ekky', CAST(N'2018-03-31T00:00:00.000' AS DateTime), CAST(N'2018-03-22T00:00:00.000' AS DateTime), NULL, N'test', N'xcx-xcx-2018-03-0070', NULL, N'testestes')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (3, N'test', N'ekky', CAST(N'2018-03-28T00:00:00.000' AS DateTime), CAST(N'2018-03-29T00:00:00.000' AS DateTime), NULL, N'test', N'xcx-xcx-2018-03-0072', NULL, N'bbbb')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (4, N'tes', N'ekky', CAST(N'2018-04-04T00:00:00.000' AS DateTime), CAST(N'2018-03-29T00:00:00.000' AS DateTime), NULL, N'test', N'xcx-xcx-2018-03-0072', NULL, N'testestes')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (5, N'tes', N'ekky', CAST(N'2018-03-29T00:00:00.000' AS DateTime), CAST(N'2018-03-29T00:00:00.000' AS DateTime), NULL, N'tes', N'xcx-xcx-2018-03-0074', NULL, N'bbbb')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (6, N'test', N'ekky', CAST(N'2018-03-28T00:00:00.000' AS DateTime), CAST(N'2018-03-21T00:00:00.000' AS DateTime), NULL, N'tes', N'xcx-xcx-2018-03-0074', NULL, N'testestes')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (19, N'tests', N'ekky', CAST(N'2018-03-13T00:00:00.000' AS DateTime), CAST(N'2018-03-15T00:00:00.000' AS DateTime), NULL, N'', N'xcx-xcx-2018-03-0075', NULL, N'bbbb')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (20, N'test2ss', N'ekky', CAST(N'2018-03-28T00:00:00.000' AS DateTime), CAST(N'2018-03-29T00:00:00.000' AS DateTime), NULL, N'', N'xcx-xcx-2018-03-0075', NULL, N'testestes')
SET IDENTITY_INSERT [dbo].[IKConditionVendor] OFF
SET IDENTITY_INSERT [dbo].[IKItem] ON 

INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (1, N'tesss', N'test', N'ekky', NULL, CAST(N'2018-03-18T19:36:38.297' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (2, N'tesss', N'stest', N'ekky', NULL, CAST(N'2018-03-18T19:36:38.310' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (5, N'asdasd', N'asdasd', N'ekky', NULL, CAST(N'2018-03-18T19:52:30.023' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (8, N'testestes', N'test22', N'ekky', NULL, CAST(N'2018-03-20T00:12:49.750' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (9, N'testestes', N'tes33', N'ekky', NULL, CAST(N'2018-03-20T00:12:49.763' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (10, N'testestes', N'tes44', N'ekky', NULL, CAST(N'2018-03-20T00:12:49.767' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (11, N'bbbb', N'bbbb', N'ekky', NULL, CAST(N'2018-03-20T00:16:51.090' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (12, N'bbbb', N'bbbcc', N'ekky', NULL, CAST(N'2018-03-20T00:16:51.097' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (13, N'bbbb', N'xxxx', N'ekky', NULL, CAST(N'2018-03-20T00:16:51.097' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (14, N'bbbb', N'zzz', N'ekky', NULL, CAST(N'2018-03-20T00:16:51.100' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (15, N'bbbb', N'123', N'ekky', NULL, CAST(N'2018-03-20T00:16:51.103' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (16, N'bbbb', N'ddd', N'ekky', NULL, CAST(N'2018-03-20T00:16:51.107' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (17, N'bbbb', N'23', N'ekky', NULL, CAST(N'2018-03-20T00:16:51.107' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (18, N'bbbb', N'xxx', N'ekky', NULL, CAST(N'2018-03-20T00:16:51.110' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[IKItem] OFF
SET IDENTITY_INSERT [dbo].[Interval] ON 

INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (1, N'on', CAST(N'2018-03-23T00:00:00.000' AS DateTime), N'GEG-PM-00500', N'ekky', CAST(N'2018-03-18T23:44:45.783' AS DateTime), NULL, NULL, N'testest')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (2, N'on', CAST(N'2018-03-16T00:00:00.000' AS DateTime), N'GEG-PM-00500', N'ekky', CAST(N'2018-03-18T23:44:48.560' AS DateTime), NULL, NULL, N'testest')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (3, N'on', CAST(N'2018-03-22T00:00:00.000' AS DateTime), N'GEG-PM-00500', N'ekky', CAST(N'2018-03-19T00:16:41.270' AS DateTime), NULL, NULL, N'asdasd')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (4, N'on', CAST(N'2018-03-13T00:00:00.000' AS DateTime), N'GEG-PM-00500', N'ekky', CAST(N'2018-03-19T00:16:41.280' AS DateTime), NULL, NULL, N'asdasd')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (5, N'on', CAST(N'2018-03-21T00:00:00.000' AS DateTime), N'GEG-PM-00500', N'ekky', CAST(N'2018-03-19T00:18:14.983' AS DateTime), NULL, NULL, N'assssss')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (6, N'500 Hour', CAST(N'2018-03-23T00:00:00.000' AS DateTime), N'GEG-PM-00500', N'ekky', CAST(N'2018-03-19T00:21:11.253' AS DateTime), NULL, NULL, N'bbb')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (13, N'500 Hour', CAST(N'2018-03-09T00:00:00.000' AS DateTime), N'GEG-PM-00500', N'ekky', CAST(N'2018-03-19T23:32:06.477' AS DateTime), NULL, NULL, N'xcxxc')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (14, N'Semester', CAST(N'2018-03-22T00:00:00.000' AS DateTime), N'GEG-PM-00500', N'ekky', CAST(N'2018-03-19T23:32:06.483' AS DateTime), NULL, NULL, N'xcxxc')
SET IDENTITY_INSERT [dbo].[Interval] OFF
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup]) VALUES (N'asdasd', N'asdasd', N'asdasd', N'asdasd', N'asdasd', N'SPC-P02-WRH-46', NULL, NULL, NULL, NULL, 1, CAST(N'2018-03-19T00:00:00.000' AS DateTime), 12, NULL)
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup]) VALUES (N'assssss', N'asd', N'ssss', N'sss', N'sss', N'SPC-P02-WRH-49', NULL, NULL, NULL, NULL, 1, CAST(N'2018-03-27T00:00:00.000' AS DateTime), 20, NULL)
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup]) VALUES (N'bbb', N'bbb', N'bbb', N'bbb', N'bbb', N'SPC-P02-WRH-48', NULL, NULL, NULL, NULL, 1, CAST(N'2018-03-29T00:00:00.000' AS DateTime), 4, NULL)
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup]) VALUES (N'testest', N'testest', N'testest', N'testest', N'test', NULL, NULL, NULL, NULL, NULL, 1, CAST(N'2018-03-21T00:00:00.000' AS DateTime), 2, NULL)
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup]) VALUES (N'xcxxc', N'xcxc', N'xcxc', N'xcxc', N'xcxc', N'SPC-P02-WRH-47', NULL, NULL, NULL, NULL, 1, CAST(N'2018-03-21T00:00:00.000' AS DateTime), 2, N'Eng1')
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'Test-21', N'testss', N'asdasd', NULL, 1, NULL, CAST(N'2018-03-19T21:30:30.240' AS DateTime), NULL, N'ekky', N'2', NULL, CAST(N'2018-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'test22', N'test', N'bbb', NULL, 1, CAST(N'2018-03-20T01:39:57.530' AS DateTime), NULL, N'ekky', NULL, N'2', NULL, CAST(N'2018-03-29T00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'xxxv', N'asd', N'xcxxc', NULL, 1, CAST(N'2018-03-20T01:45:30.543' AS DateTime), NULL, N'ekky', NULL, N'33', NULL, CAST(N'2018-03-21T00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'xxxx', N'xxxx', N'xcxxc', NULL, 1, CAST(N'2018-03-20T01:45:15.753' AS DateTime), NULL, N'ekky', NULL, N'12', NULL, CAST(N'2018-03-21T00:00:00.000' AS DateTime))
INSERT [dbo].[MasterIK] ([IKCode], [IKDescription], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy]) VALUES (N'GEG-PM-00500', N'engineering ik', 1, CAST(N'2018-03-18T18:20:16.593' AS DateTime), CAST(N'2018-03-18T18:22:06.440' AS DateTime), N'ekky', N'ekky')
INSERT [dbo].[MasterIK] ([IKCode], [IKDescription], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy]) VALUES (N'GEG-PM-00501', N'test', 1, CAST(N'2018-03-18T18:22:18.057' AS DateTime), CAST(N'2018-03-20T00:13:10.910' AS DateTime), N'ekky', N'ekky')
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'--', NULL, NULL, NULL, NULL, NULL, CAST(N'2018-03-18T01:01:47.073' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P01-Warehouse', N'SPC', N'P01', N'Warehouse', N'TESTing', 1, CAST(N'2018-03-17T19:20:44.090' AS DateTime), N'ekky', CAST(N'2018-03-17T19:36:18.827' AS DateTime), N'ekky')
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-0', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:51.840' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-1', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:51.970' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-10', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.003' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-11', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.003' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-12', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.007' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-13', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.010' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-14', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.010' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-15', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.013' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-16', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.013' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-17', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.017' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-18', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.020' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-19', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.020' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-2', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:51.973' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-20', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.023' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-21', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.023' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-22', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.023' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-23', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.027' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-24', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.030' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-25', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.030' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-26', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.033' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-27', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.033' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-28', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.037' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-29', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.040' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-3', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:51.980' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-30', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.040' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-31', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.043' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-32', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.043' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-33', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.047' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-34', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.047' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-35', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.050' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-36', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.050' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-37', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.053' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-38', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.053' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-39', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.057' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-4', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:51.983' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-40', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.057' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-41', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.060' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-42', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.063' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-43', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.063' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-44', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.067' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-45', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.067' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-46', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.070' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-47', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.073' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-48', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.073' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-49', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.077' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-5', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:51.987' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-6', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:51.987' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-7', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:51.990' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-8', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:51.993' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-P02-WRH-9', N'SPC', N'P02', N'WRH', NULL, 1, CAST(N'2018-03-17T20:19:52.000' AS DateTime), N'ekky', NULL, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted]) VALUES (N'ekky', N'vPelNrzLabeGOYF6LMYvZA==', N'daniel ekky', NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted]) VALUES (N'ekky2', N'vPelNrzLabeGOYF6LMYvZA==', N'ekky', CAST(N'2018-03-18T01:06:19.353' AS DateTime), N'ekky', NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[UserGroupDetail] ON 

INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (3, N'Eng1', N'ekky', CAST(N'2018-03-18T16:26:21.020' AS DateTime), N'ekky', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (4, N'Eng1', N'ekky2', CAST(N'2018-03-18T16:26:22.220' AS DateTime), N'ekky', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserGroupDetail] OFF
INSERT [dbo].[UserGroupHeader] ([UserGroupId], [UserGroupName], [UserGroupLeader], [CreatedDate], [CreatedBy], [UpdateDate], [UpdateBy], [IsActive]) VALUES (N'Eng1', N'Engineering 1', N'ekky2', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'asd-asd-2018-03-0001', CAST(N'2018-03-19T23:22:13.460' AS DateTime), N'asdasd', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SPC-P02-WRH-46', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'asd-asd-2018-03-0002', CAST(N'2018-03-19T23:24:07.703' AS DateTime), N'asdasd', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SPC-P02-WRH-46', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'asd-asd-2018-03-0003', CAST(N'2018-03-19T23:25:02.313' AS DateTime), N'asdasd', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SPC-P02-WRH-46', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0004', CAST(N'2018-03-19T23:32:21.173' AS DateTime), N'xcxxc', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0005', CAST(N'2018-03-19T23:34:11.770' AS DateTime), N'xcxxc', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0006', CAST(N'2018-03-19T23:35:19.320' AS DateTime), N'xcxxc', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0007', CAST(N'2018-03-19T23:36:01.133' AS DateTime), N'xcxxc', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0008', CAST(N'2018-03-19T23:36:23.100' AS DateTime), N'xcxxc', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0009', CAST(N'2018-03-19T23:37:13.680' AS DateTime), N'xcxxc', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0010', CAST(N'2018-03-19T23:41:58.620' AS DateTime), N'xcxxc', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0011', CAST(N'2018-03-19T23:44:36.923' AS DateTime), N'xcxxc', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0012', CAST(N'2018-03-19T23:47:46.650' AS DateTime), N'xcxxc', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0013', CAST(N'2018-03-20T00:08:56.513' AS DateTime), N'xcxxc', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0014', CAST(N'2018-03-20T00:10:35.013' AS DateTime), N'xcxxc', N'GEG-PM-00500', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'engineering ik', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0015', CAST(N'2018-03-20T00:13:16.800' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0016', CAST(N'2018-03-20T00:14:25.323' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0017', CAST(N'2018-03-20T00:15:01.273' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0018', CAST(N'2018-03-20T00:15:45.277' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0019', CAST(N'2018-03-20T00:17:00.697' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0020', CAST(N'2018-03-20T00:17:44.923' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0021', CAST(N'2018-03-20T00:19:05.027' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0022', CAST(N'2018-03-20T00:20:29.330' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0023', CAST(N'2018-03-20T00:20:46.017' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0024', CAST(N'2018-03-20T00:22:52.220' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0025', CAST(N'2018-03-20T00:48:52.073' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0026', CAST(N'2018-03-20T00:52:03.970' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0027', CAST(N'2018-03-20T00:54:01.230' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0028', CAST(N'2018-03-20T00:54:31.913' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0029', CAST(N'2018-03-20T00:55:16.973' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0030', CAST(N'2018-03-20T00:58:09.770' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0031', CAST(N'2018-03-20T00:59:11.820' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0032', CAST(N'2018-03-20T00:59:17.283' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0033', CAST(N'2018-03-20T00:59:39.510' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0034', CAST(N'2018-03-20T01:00:03.613' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0035', CAST(N'2018-03-20T01:02:37.407' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0036', CAST(N'2018-03-20T01:03:05.067' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0037', CAST(N'2018-03-20T01:03:22.657' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0038', CAST(N'2018-03-20T01:03:32.440' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0039', CAST(N'2018-03-20T01:03:53.343' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0040', CAST(N'2018-03-20T01:05:06.547' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0041', CAST(N'2018-03-20T01:05:59.290' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0042', CAST(N'2018-03-20T01:06:31.290' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0043', CAST(N'2018-03-20T01:19:03.927' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0044', CAST(N'2018-03-20T01:19:38.787' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0045', CAST(N'2018-03-20T01:20:24.520' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0046', CAST(N'2018-03-20T01:20:47.940' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0047', CAST(N'2018-03-20T01:23:13.557' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0048', CAST(N'2018-03-20T01:23:28.667' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0049', CAST(N'2018-03-20T01:26:23.413' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0050', CAST(N'2018-03-20T01:28:21.513' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0051', CAST(N'2018-03-20T01:31:27.223' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0052', CAST(N'2018-03-20T01:32:57.200' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0053', CAST(N'2018-03-20T01:34:14.990' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0054', CAST(N'2018-03-20T01:34:37.447' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0055', CAST(N'2018-03-20T01:38:59.913' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0056', CAST(N'2018-03-20T01:39:29.833' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0057', CAST(N'2018-03-20T01:40:02.123' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0058', CAST(N'2018-03-20T01:45:38.520' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0059', CAST(N'2018-03-20T01:57:29.610' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0060', CAST(N'2018-03-20T02:19:30.970' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0061', CAST(N'2018-03-20T02:19:52.863' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0062', CAST(N'2018-03-20T02:23:07.657' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0063', CAST(N'2018-03-20T02:25:56.720' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0064', CAST(N'2018-03-20T03:24:01.037' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0065', CAST(N'2018-03-20T03:25:55.637' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0066', CAST(N'2018-03-20T03:26:51.960' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0067', CAST(N'2018-03-20T03:29:32.530' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0068', CAST(N'2018-03-20T03:32:15.053' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0069', CAST(N'2018-03-20T03:34:29.997' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0070', CAST(N'2018-03-20T03:38:19.713' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0071', CAST(N'2018-03-20T03:43:09.813' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0072', CAST(N'2018-03-20T03:47:23.393' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0073', CAST(N'2018-03-20T03:52:04.310' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0074', CAST(N'2018-03-20T03:52:35.687' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0075', CAST(N'2018-03-20T04:07:12.137' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory]) VALUES (N'xcx-xcx-2018-03-0076', CAST(N'2018-03-20T04:10:48.753' AS DateTime), N'xcxxc', N'GEG-PM-00501', NULL, NULL, N'ekky', NULL, NULL, NULL, N'Open', NULL, NULL, NULL, NULL, NULL, N'xcxc', N'ekky2', N'SPC-P02-WRH-47', N'test', NULL)
SET IDENTITY_INSERT [dbo].[WorkOrderManualMaintenance] ON 

INSERT [dbo].[WorkOrderManualMaintenance] ([MaintenanceID], [AssetTypeID], [WorkOrderManualID], [Jumlah], [Satuan], [Remarks], [maintenanceType]) VALUES (14, N'xxxv', N'xcx-xcx-2018-03-0075', N'1', N'2', N'asdss', N'Replace')
INSERT [dbo].[WorkOrderManualMaintenance] ([MaintenanceID], [AssetTypeID], [WorkOrderManualID], [Jumlah], [Satuan], [Remarks], [maintenanceType]) VALUES (15, N'xxxv', N'xcx-xcx-2018-03-0075', N'3', N'4', N'asdfss', N'Service')
INSERT [dbo].[WorkOrderManualMaintenance] ([MaintenanceID], [AssetTypeID], [WorkOrderManualID], [Jumlah], [Satuan], [Remarks], [maintenanceType]) VALUES (16, N'xxxv', N'xcx-xcx-2018-03-0075', N'1', N'2', N'asdss', N'Replace')
SET IDENTITY_INSERT [dbo].[WorkOrderManualMaintenance] OFF
ALTER TABLE [dbo].[AssetIntervalInformation]  WITH CHECK ADD  CONSTRAINT [FK_AssetIntervalInformation_MasterIK] FOREIGN KEY([IKCode])
REFERENCES [dbo].[MasterIK] ([IKCode])
GO
ALTER TABLE [dbo].[AssetIntervalInformation] CHECK CONSTRAINT [FK_AssetIntervalInformation_MasterIK]
GO
ALTER TABLE [dbo].[IKCategory]  WITH CHECK ADD  CONSTRAINT [FK_IKCategory_MasterIK] FOREIGN KEY([MasterIK])
REFERENCES [dbo].[MasterIK] ([IKCode])
GO
ALTER TABLE [dbo].[IKCategory] CHECK CONSTRAINT [FK_IKCategory_MasterIK]
GO
ALTER TABLE [dbo].[IKCondition]  WITH CHECK ADD  CONSTRAINT [FK_IKCondition_WorkOrderManual] FOREIGN KEY([WorkOrderManualID])
REFERENCES [dbo].[WorkOrderManual] ([WorkOderManualID])
GO
ALTER TABLE [dbo].[IKCondition] CHECK CONSTRAINT [FK_IKCondition_WorkOrderManual]
GO
ALTER TABLE [dbo].[IKConditionVendor]  WITH CHECK ADD  CONSTRAINT [FK_IKConditionVendor_WorkOrderManual] FOREIGN KEY([WorkOrderManual])
REFERENCES [dbo].[WorkOrderManual] ([WorkOderManualID])
GO
ALTER TABLE [dbo].[IKConditionVendor] CHECK CONSTRAINT [FK_IKConditionVendor_WorkOrderManual]
GO
ALTER TABLE [dbo].[IKItem]  WITH CHECK ADD  CONSTRAINT [FK_IKItem_IKCategory] FOREIGN KEY([IKCategory])
REFERENCES [dbo].[IKCategory] ([IKCategory])
GO
ALTER TABLE [dbo].[IKItem] CHECK CONSTRAINT [FK_IKItem_IKCategory]
GO
ALTER TABLE [dbo].[Interval]  WITH CHECK ADD  CONSTRAINT [FK_Interval_MasterAssetCategory] FOREIGN KEY([AssetCategoryID])
REFERENCES [dbo].[MasterAssetCategory] ([AssetCategoryID])
GO
ALTER TABLE [dbo].[Interval] CHECK CONSTRAINT [FK_Interval_MasterAssetCategory]
GO
ALTER TABLE [dbo].[Interval]  WITH CHECK ADD  CONSTRAINT [FK_Interval_MasterIK] FOREIGN KEY([IKCode])
REFERENCES [dbo].[MasterIK] ([IKCode])
GO
ALTER TABLE [dbo].[Interval] CHECK CONSTRAINT [FK_Interval_MasterIK]
GO
ALTER TABLE [dbo].[MasterAssetCategory]  WITH CHECK ADD  CONSTRAINT [FK_MasterAssetCategory_MasterLocation] FOREIGN KEY([Location])
REFERENCES [dbo].[MasterLocation] ([LocationCode])
GO
ALTER TABLE [dbo].[MasterAssetCategory] CHECK CONSTRAINT [FK_MasterAssetCategory_MasterLocation]
GO
ALTER TABLE [dbo].[MasterAssetCategory]  WITH CHECK ADD  CONSTRAINT [FK_MasterAssetCategory_UserGroupHeader] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroupHeader] ([UserGroupId])
GO
ALTER TABLE [dbo].[MasterAssetCategory] CHECK CONSTRAINT [FK_MasterAssetCategory_UserGroupHeader]
GO
ALTER TABLE [dbo].[MasterAssetType]  WITH CHECK ADD  CONSTRAINT [FK_MasterAssetType_MasterAssetType] FOREIGN KEY([AssetCategoryID])
REFERENCES [dbo].[MasterAssetCategory] ([AssetCategoryID])
GO
ALTER TABLE [dbo].[MasterAssetType] CHECK CONSTRAINT [FK_MasterAssetType_MasterAssetType]
GO
ALTER TABLE [dbo].[UserGroupDetail]  WITH CHECK ADD  CONSTRAINT [FK_UserGroupDetail_UserGroupDetail] FOREIGN KEY([UserGroupID])
REFERENCES [dbo].[UserGroupHeader] ([UserGroupId])
GO
ALTER TABLE [dbo].[UserGroupDetail] CHECK CONSTRAINT [FK_UserGroupDetail_UserGroupDetail]
GO
ALTER TABLE [dbo].[UserGroupDetail]  WITH CHECK ADD  CONSTRAINT [FK_UserGroupDetail_UserGroupHeader] FOREIGN KEY([UserID])
REFERENCES [dbo].[MasterUser] ([UserID])
GO
ALTER TABLE [dbo].[UserGroupDetail] CHECK CONSTRAINT [FK_UserGroupDetail_UserGroupHeader]
GO
ALTER TABLE [dbo].[WorkOrderManual]  WITH CHECK ADD  CONSTRAINT [FK_WorkOrderManual_MasterAssetCategory] FOREIGN KEY([CategoryAssetID])
REFERENCES [dbo].[MasterAssetCategory] ([AssetCategoryID])
GO
ALTER TABLE [dbo].[WorkOrderManual] CHECK CONSTRAINT [FK_WorkOrderManual_MasterAssetCategory]
GO
ALTER TABLE [dbo].[WorkOrderManual]  WITH CHECK ADD  CONSTRAINT [FK_WorkOrderManual_MasterIK] FOREIGN KEY([IKCode])
REFERENCES [dbo].[MasterIK] ([IKCode])
GO
ALTER TABLE [dbo].[WorkOrderManual] CHECK CONSTRAINT [FK_WorkOrderManual_MasterIK]
GO
ALTER TABLE [dbo].[WorkOrderManualMaintenance]  WITH CHECK ADD  CONSTRAINT [FK_WorkOrderManualMaintenance_WorkOrderManual] FOREIGN KEY([WorkOrderManualID])
REFERENCES [dbo].[WorkOrderManual] ([WorkOderManualID])
GO
ALTER TABLE [dbo].[WorkOrderManualMaintenance] CHECK CONSTRAINT [FK_WorkOrderManualMaintenance_WorkOrderManual]
GO
USE [master]
GO
ALTER DATABASE [PreventiveAsset] SET  READ_WRITE 
GO
