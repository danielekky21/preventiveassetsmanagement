/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Express Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [PreventiveAsset]
GO

/****** Object:  Table [dbo].[MasterInterval]    Script Date: 4/1/2018 10:22:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MasterInterval](
	[IntervalID] [int] IDENTITY(1,1) NOT NULL,
	[IntervalGroup] [varchar](100) NULL,
	[IntervalCode] [varchar](100) NULL,
	[IntervalValue] [int] NULL,
	[Description] [varchar](300) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdadatedBy] [varchar](50) NULL,
	[IsEnabled] [bit] NULL,
	[IntervalType] [varchar](50) NULL,
 CONSTRAINT [PK_MasterInterval] PRIMARY KEY CLUSTERED 
(
	[IntervalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


