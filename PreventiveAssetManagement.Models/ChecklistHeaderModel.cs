﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.Models
{
    public class ChecklistHeaderModel
    {
        public string ChecklistHeaderID { get; set; }
        public string Description { get; set; }
    }
}
