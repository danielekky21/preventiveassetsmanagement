﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PreventiveAssetManagement.Models
{
    public class DataItem
    {
        public string LocationCode { get; set; }
        public string SubPortoFolio { get; set; }
        public string Category { get; set; }
        public string Floor { get; set; }
        public string Status { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string UserGroupId { get; set; }
        public string UserGroupName { get; set; }
        public string UserGroupLeader { get; set; }
        public string IKCode { get; set; }
        public string IKDescription { get; set; }
        public string AssetCategoryID { get; set; }
        public string AssetGroup { get; set; }
        public string AssetCategory { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string Departement { get; set; }
        public string InstallmentDate { get; set; }
        public string Lifetime { get; set; }
        public string AssetTypeID { get; set; }
        public string ReplacementDate { get; set; }
        public string WorkOrderNo { get; set; }
        public string WorkOrderDate { get; set; }
        public string CategoryAssetID { get; set; }
        public string Replacement { get; set; }
        public string LocationArea { get; set; }
        public string CategoryAssetDescription { get; set; }
        public string VendorID { get; set; }
        public string VendorName { get; set; }
        public string VendorDescription { get; set; }
        public string GroupItemID { get; set; }
        public string ChecklistDetailID { get; set; }
        public string ChecklistHeaderID { get; set; }


    }
    public class DataTableData
    {

        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<DataItem> ListData { get; set; }


    }
    
}
