﻿namespace PreventiveAssetManagement.dataRepo.GenericRepository
{
    public interface IRepositoryFactory
    {
        IRepository Repository();
    }
}
