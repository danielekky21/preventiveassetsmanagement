﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class MasterVendor
    {
        public static IQueryable<MasterVendor> GetAllActiveVendor()
        {
            return CurrentDataContext.CurrentContext.MasterVendors.Where(x => x.IsActive == true);
        }
        public static MasterVendor GetVendorByID(int ID)
        {
            return CurrentDataContext.CurrentContext.MasterVendors.FirstOrDefault(x => x.VendorID == ID);
        }
        public static IQueryable<MasterVendor> VendorList(string prefix)
        {
            return CurrentDataContext.CurrentContext.MasterVendors.Where(x => x.VendorName.Contains(prefix));
        }
    }
}
