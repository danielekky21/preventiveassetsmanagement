﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class ChecklistHeaderDetail
    {
        public static IQueryable<ChecklistHeaderDetail> GetByChecklistHeader(string id)
        {
           return CurrentDataContext.CurrentContext.ChecklistHeaderDetails.Where(x => x.ItemChecklistHeader == id);
        }
    }
}
