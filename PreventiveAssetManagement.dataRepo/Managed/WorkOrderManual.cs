﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class WorkOrderManual
    {
        public static string countWorkOrderzeroformat()
        {
            int count = CurrentDataContext.CurrentContext.WorkOrderManuals.Count() + 1;
            string format = count.ToString().PadLeft(4, '0');
            return format;
        }
        public static IQueryable<IKCondition> GetAllWorkOrderCondition(string id)
        {
            return CurrentDataContext.CurrentContext.IKConditions.Where(x => x.WorkOrderManualID == id);
        }
        public static IQueryable<IKConditionVendor> GetAllVendor(string id)
        {
            return CurrentDataContext.CurrentContext.IKConditionVendors.Where(x => x.WorkOrderManual == id);
        }
        public static IQueryable<WorkOrderManualMaintenance> GetAllMaintenance(string id)
        {
            return CurrentDataContext.CurrentContext.WorkOrderManualMaintenances.Where(x => x.WorkOrderManualID == id);
        }
        public static IQueryable<WorkOrderManualMaintenance> GetAllMaintenances()
        {
            return CurrentDataContext.CurrentContext.WorkOrderManualMaintenances;
        }
        public static IQueryable<WorkOrderManual> GetAllWorkOrder()
        {
            return CurrentDataContext.CurrentContext.WorkOrderManuals;
        }
        public static IQueryable<WorkOrderManual> GetAllRiskWorkOrder()
        {
            return CurrentDataContext.CurrentContext.WorkOrderManuals.Where(x => x.RiskStatus == null);
        }
        public static WorkOrderManual GetWorkOderByID(string id)
        {
            return CurrentDataContext.CurrentContext.WorkOrderManuals.FirstOrDefault(x => x.WorkOderManualID == id);
        }
        public static IQueryable<IKCondition> GetIKConditionByWOID(string id)
        {
            return CurrentDataContext.CurrentContext.IKConditions.Where(x => x.WorkOrderManualID == id);
        }
        public static IQueryable<WorkOrderManualMaintenance> GetMaintenanceByWOID(string id)
        {
            return CurrentDataContext.CurrentContext.WorkOrderManualMaintenances.Where(x => x.WorkOrderManualID == id);
        }
        public static IQueryable<IKConditionVendor> GetVendorByWOID(string id)
        {
            return CurrentDataContext.CurrentContext.IKConditionVendors.Where(x => x.WorkOrderManual == id);
        }
        public static IQueryable<WorkOrderManual> GetWorkOrderByGroup(string groupid)
        {
            return CurrentDataContext.CurrentContext.WorkOrderManuals.Where(x => x.MasterAssetCategory.UserGroup == groupid);
        }
        public static IQueryable<WorkOrderManual> GetWorkOrderByAsset(string groupid)
        {
            return CurrentDataContext.CurrentContext.WorkOrderManuals.Where(x => x.MasterAssetCategory.AssetGroup == groupid);
        }
    }
}
