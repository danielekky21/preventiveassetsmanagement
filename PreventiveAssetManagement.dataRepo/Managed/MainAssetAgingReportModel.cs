﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo.Managed
{
    public class MainAssetAgingReportModel
    {
        public string AssetCategoryCode { get; set; }
        public string AssetGroup { get; set; }
        public string AssetCategory { get; set; }
        public string Description { get; set; }
        public string InstallmentDate { get; set; }
        public string Lifetime { get; set; }
        public string Age { get; set; }

    }
}
