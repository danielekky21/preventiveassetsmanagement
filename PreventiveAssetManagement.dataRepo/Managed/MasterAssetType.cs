﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class MasterAssetType
    {
        public static IQueryable<MasterAssetType> GetAllAssetType()
        {
            return CurrentDataContext.CurrentContext.MasterAssetTypes;
        }
        public static MasterAssetType GetAllAssetTypeByID(string id)
        {
            return CurrentDataContext.CurrentContext.MasterAssetTypes.FirstOrDefault(x => x.MasterAssetTypeID == id);
        }
        public static IQueryable<MasterAssetType> GetAllAssetTypeByCat(string catid)
        {
            return CurrentDataContext.CurrentContext.MasterAssetTypes.Where(x => x.AssetCategoryID == catid);
        }
        public static WorkOrderManualMaintenance GetMaintenanceDateByCode(string assettypeid)
        {
            return CurrentDataContext.CurrentContext.WorkOrderManualMaintenances.FirstOrDefault(x => x.AssetTypeID == assettypeid);
        }

    }
}
