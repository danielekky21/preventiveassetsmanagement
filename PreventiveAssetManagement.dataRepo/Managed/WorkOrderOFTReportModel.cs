﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo.Managed
{
    public class WorkOrderOFTReportModel
    {
        public string WorkOrderDate { get; set; }
        public string WONo { get; set; }
        public string WorkStartDate { get; set; }
        public string WorkEndDate { get; set; }
        public string AssetCategoryCode { get; set; }
        public string AssetcategoryDesc { get; set; }
        public string AssetLocationCode { get; set; }
        public string IntervalDescription { get; set; }
        public string PreviousWODate { get; set; }
        public string Status { get; set; }
        public string SubmittedBy { get; set; }
        public string SubmittedDate { get; set; }
        public string ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }
        public string Memo { get; set; }
        public string Leader { get; set; }
    }
}
