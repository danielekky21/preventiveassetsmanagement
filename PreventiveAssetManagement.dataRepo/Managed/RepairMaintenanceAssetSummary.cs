﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo.Managed
{
    public class RepairMaintenanceAssetSummary
    {
        public string Replace { get; set; }
        public string Service { get; set; }
    }
}
