﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo.Managed
{
    public class MainAssetReportModel
    {
        public string AssetGroup { get; set; }
        public string AssetCategoryCode { get; set; }
        public string AssetCategory { get; set; }
        public string Description { get; set; }
        public string AssetLocationCode { get; set; }
        public string InstallmentDate { get; set; }
        public string Lifetime { get; set; }
        public string LastWONo { get; set; }
        public string LastMaintenanceDate { get; set; }
        public string Leader { get; set; }
        public string PICRemark { get; set; }
        public string NextMaintenanceDate { get; set; }
        public string NextWONo { get; set; }
    }
}
