﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class GroupItem
    {
        public static IQueryable<GroupItem> GetAllGroupItem()
        {
            return CurrentDataContext.CurrentContext.GroupItems;
        }
        public static GroupItem GetGroupItemByID(string id)
        {
            return CurrentDataContext.CurrentContext.GroupItems.FirstOrDefault(x => x.GroupItemID == id);
        }
        public static int CountItem()
        {
            var data = GetAllGroupItem();
            return data.Count();
        }
    }
}
