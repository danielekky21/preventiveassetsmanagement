﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class MasterLocation
    {
        public static IQueryable<MasterLocation> GetAllActiveLocation()
        {
            return CurrentDataContext.CurrentContext.MasterLocations;
        }
        public static int AllAcitveLocationTotal()
        {
            return CurrentDataContext.CurrentContext.MasterLocations.Count();
        }
        public static MasterLocation GetLocationByID(string id)
        {
            return CurrentDataContext.CurrentContext.MasterLocations.FirstOrDefault(x => x.IsActive == true && x.LocationCode == id);
        }
    }
}
