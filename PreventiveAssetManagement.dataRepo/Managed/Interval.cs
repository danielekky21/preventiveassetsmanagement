﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class Interval
    {
        public static IQueryable<MasterInterval> GetAllActiveMasterInterval()
        {
            return CurrentDataContext.CurrentContext.MasterIntervals.Where(x => x.IsActive == true);
        }
        public static IQueryable<MasterInterval> GetAllEnableInterval()
        {
            return CurrentDataContext.CurrentContext.MasterIntervals.Where(x => x.IsActive == true && x.IsEnabled == true);
        }
        public static MasterInterval GetMasterIntervalByID(int id)
        {
            return CurrentDataContext.CurrentContext.MasterIntervals.FirstOrDefault(x => x.IntervalID == id);
        }
        public static MasterInterval GetMasterIntervalByCode(string code)
        {
            return CurrentDataContext.CurrentContext.MasterIntervals.FirstOrDefault(x => x.IntervalCode == code);
        }
        
    }
}
