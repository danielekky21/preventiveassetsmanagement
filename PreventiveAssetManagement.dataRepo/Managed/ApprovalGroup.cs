﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class ApprovalGroup
    {
        public static IQueryable<ApprovalGroup> GetAllApprovalGroupByGroupID(string id)
        {
            return CurrentDataContext.CurrentContext.ApprovalGroups.Where(x => x.ApprovalUserGroup == id);
        }
        public static bool CheckIfIsLeader(string usergroup,string userid)
        {
            bool isTrue = false;
            var Leader = CurrentDataContext.CurrentContext.ApprovalGroups.Where(x => x.ApprovalUserGroup == usergroup && x.ApprovalId == userid);
            if (Leader != null)
            {
                isTrue = true;
            }
            return isTrue;
        }
    }
}
