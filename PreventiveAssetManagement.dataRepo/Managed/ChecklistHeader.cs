﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PreventiveAssetManagement.Models;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class ChecklistHeader
    {
        public static IQueryable<ChecklistHeader> GetAllChecklistHeader()
        {
            return CurrentDataContext.CurrentContext.ChecklistHeaders;
        }
        public static ChecklistHeader GetChecklistHeaderById(string id)
        {
            return CurrentDataContext.CurrentContext.ChecklistHeaders.FirstOrDefault(x => x.ChecklistHeaderID == id);
        }
        public static int CountChkHeader()
        {
            return CurrentDataContext.CurrentContext.ChecklistHeaders.Count();
        }
        public static IQueryable<ChecklistHeader> GetByGroupID(string id)
        {
            return CurrentDataContext.CurrentContext.ChecklistHeaders.Where(x => x.GroupItem == id);
        }
        public static IQueryable<ChecklistHeaderModel> MapByGroupID(string id)
        {
            var data = GetByGroupID(id);
            var model = from a in data
                        select new ChecklistHeaderModel
                        {
                            ChecklistHeaderID = a.ChecklistHeaderID,
                            Description = a.Description
                        };

            return model;
        }
        public static IQueryable<ChecklistHeaderDetail> GetChkbyheaderID(string id)
        {
            return CurrentDataContext.CurrentContext.ChecklistHeaderDetails.Where(x => x.ItemChecklistHeader == id);
        }
        public static IQueryable<MapitemDetail> MapItemDetailByHeader(string id)
        {
            var data = GetChkbyheaderID(id);
            var model = from a in data
                        select new MapitemDetail
                        {
                            kondisi = a.ItemChecklistDetail1.Description,
                            detailid = a.ItemChecklistDetail1.ChecklistDetail,
                            
                        };
            return model;
        }
    }
}
