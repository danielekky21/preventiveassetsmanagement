﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo.Managed
{
    public class AssetTypeReportModel
    {
        
        public string AssetCategoryCode { get; set; }
        public string AssetTypeCode { get; set; }
        public string Description { get; set; }
        public string InstallmentDate { get; set; }
        public string Lifetime { get; set; }
        public string ReplacementKe { get; set; }
        public string ReplacementDate { get; set; }
        public string LastWONo { get; set; }
        public string LastMaintenanceDate { get; set; }
        public string Duration { get; set; }
        public string LastVendor { get; set; }
        public string LastStartedBy { get; set; }
        public string LastCompletedBy { get; set; }
        public string PICRemark { get; set; }
        public string NextMaintenanceDate { get; set; }
        public string NextWONo { get; set; }
    }
}
