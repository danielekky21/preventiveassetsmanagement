﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class MasterDepartement
    {
        public static IQueryable<MasterDepartement> GetAllDepartementList()
        {
            return CurrentDataContext.CurrentContext.MasterDepartements.Where(x => x.IsDeleted == false);
        }
        public static MasterDepartement GetDepartementByID(int id)
        {
            return CurrentDataContext.CurrentContext.MasterDepartements.FirstOrDefault(x => x.DepartementID == id);
        }
        public static string GetDepartmenetColorByName(string name)
        {
            return CurrentDataContext.CurrentContext.MasterDepartements.FirstOrDefault(x => x.DepartementName == name).color;
        }
    }
}
