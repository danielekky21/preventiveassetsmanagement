﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class MasterUser
    {
        public static MasterUser GetUserByUserNameAndPassword(string username, string password)
        {
            return CurrentDataContext.CurrentContext.MasterUsers.FirstOrDefault(x => x.UserID == username && x.Password == password && x.IsDeleted == false);
        }
        public static MasterUser GetUserByID(string id)
        {
            return CurrentDataContext.CurrentContext.MasterUsers.FirstOrDefault(x => x.UserID == id);
        }
        public static IQueryable<MasterUser> GetAllUser()
        {
            return CurrentDataContext.CurrentContext.MasterUsers;
        }
        public static IQueryable<MasterUser> GetAlActivelUser()
        {
            return CurrentDataContext.CurrentContext.MasterUsers.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<MasterUser> GetUserByGroup(string groupid)
        {
            var userGroup = CurrentDataContext.CurrentContext.UserGroupDetails.Where(x => x.UserGroupID == groupid);
            List<MasterUser> listuser = new List<MasterUser>();
            foreach (var item in userGroup)
            {
                MasterUser user = new MasterUser();
                user = item.MasterUser;
                listuser.Add(user);
            }
            return listuser.AsQueryable();
        }
    }
}
