﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class MasterAssetCategory
    {

        public static IQueryable<MasterAssetCategory> GetAllMasterAssetCategory()
        {
            return CurrentDataContext.CurrentContext.MasterAssetCategories;
        }
        public static MasterAssetCategory GetMasterAssetByID(string id)
        {
            return CurrentDataContext.CurrentContext.MasterAssetCategories.FirstOrDefault(x => x.AssetCategoryID == id);
        }
        public static IQueryable<Interval> GetAllIntervalByAssetCategoryID(string id)
        {
            return CurrentDataContext.CurrentContext.Intervals.Where(x => x.AssetCategoryID == id);
        }
        public static IQueryable<MasterAssetCategory> GetAllActiveMasterAssetCategory()
        {
            return CurrentDataContext.CurrentContext.MasterAssetCategories.Where(x => x.IsActive == true);
        }
        public static IQueryable<MasterAssetCategory> GetCategoryByAsset(string id)
        {
            return CurrentDataContext.CurrentContext.MasterAssetCategories.Where(x => x.AssetCategoryID.Contains(id));
        }
        public static bool CheckCatCode(string search_keyword)
        {
            var data = CurrentDataContext.CurrentContext.MasterAssetCategories.FirstOrDefault(x => x.AssetCategoryID.ToLower() == search_keyword);
            if (data != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static string GetUserGroupByCategoryID(string categoryID)
        {
            return CurrentDataContext.CurrentContext.MasterAssetCategories.FirstOrDefault(x => x.AssetCategoryID == categoryID).UserGroup;
        }
    }
}
