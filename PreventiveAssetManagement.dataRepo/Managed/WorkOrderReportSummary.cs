﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo.Managed
{
    public class WorkOrderReportSummary
    {
        public string TotalWO { get; set; }
        public string Open { get; set; }
        public string Submitted { get; set; }
        public string Approved { get; set; }
        public string Rejected { get; set; }
    }
}
