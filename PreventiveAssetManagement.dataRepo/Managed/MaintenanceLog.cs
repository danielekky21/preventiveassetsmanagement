﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class MaintenanceLog
    {
        public static IQueryable<MaintenanceLog> GetHistoryByAssetID(string assetid)
        {
            return CurrentDataContext.CurrentContext.MaintenanceLogs.Where(x => x.AssetCategoryCode == assetid);
        }
        public static MaintenanceLog GetByassetIDFirst(string assetid)
        {
            return CurrentDataContext.CurrentContext.MaintenanceLogs.FirstOrDefault(x => x.AssetCategoryCode == assetid);
        }
    }
}
