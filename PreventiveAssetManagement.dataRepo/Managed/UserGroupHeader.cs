﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class UserGroupHeader
    {
        public static UserGroupHeader GetGroupHeaderByID(string id)
        {
            return CurrentDataContext.CurrentContext.UserGroupHeaders.FirstOrDefault(x => x.UserGroupId == id);
        }
        public static IQueryable<UserGroupDetail> GetUserGroupDetailByHeaderID(string id)
        {
            return CurrentDataContext.CurrentContext.UserGroupDetails.Where(x => x.UserGroupID == id);
        }
        public static IQueryable<UserGroupHeader> GetAllGroupHeader()
        {
            return CurrentDataContext.CurrentContext.UserGroupHeaders;
        }
        public static IQueryable<UserGroupDetail> GetAllGroupIDbyUserID(string id)
        {
            return CurrentDataContext.CurrentContext.UserGroupDetails.Where(x => x.UserID == id);
        }
        public static UserGroupDetail GetGroupIDbyUserID(string id)
        {
            return CurrentDataContext.CurrentContext.UserGroupDetails.FirstOrDefault(x => x.UserID == id);
        }
        public static IQueryable<UserGroupHeader> GetGroupLeaderById(string id)
        {
            return CurrentDataContext.CurrentContext.UserGroupHeaders.Where(x => x.UserGroupId == id);
        }
        public static bool CheckUserGroup(string search)
        {
            bool result;
            var userResult = CurrentDataContext.CurrentContext.UserGroupHeaders.FirstOrDefault(x => x.UserGroupId.ToLower() == search);
            if (userResult != null)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
    }
}
