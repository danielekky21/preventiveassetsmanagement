﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class RiskVisit
    {
        public static RiskVisit GetRiskVisitByID(string id)
        {
            return CurrentDataContext.CurrentContext.RiskVisits.FirstOrDefault(x => x.VisitID == id);
        }
        public static RiskVisit GetRiskVisitByWOID(string id)
        {
            return CurrentDataContext.CurrentContext.RiskVisits.FirstOrDefault(x => x.WorkOrderID == id);
        }
        public static IQueryable<RiskVisitChecklist> GetRiskVisitChecklistByVisitID(string riskvisitid)
        {
            return CurrentDataContext.CurrentContext.RiskVisitChecklists.Where(x => x.RiskVisitId == riskvisitid);
        }
        public static int riskCount()
        {
            return CurrentDataContext.CurrentContext.RiskVisits.Where(x => x.CreatedDate.Value.Year == DateTime.Now.Year).Count();
        }
        public static RiskVisitChecklist GetRiskVisitDetail(Guid id)
        {
            return CurrentDataContext.CurrentContext.RiskVisitChecklists.FirstOrDefault(x => x.ChecklistID == id);
        }
        public static IQueryable<RiskVisit> GetOpenRiskVisit()
        {
            return CurrentDataContext.CurrentContext.RiskVisits.Where(x => x.ApprovalStatus == "OPEN" || x.ApprovalStatus == "REJECT");
        }
    }
}
