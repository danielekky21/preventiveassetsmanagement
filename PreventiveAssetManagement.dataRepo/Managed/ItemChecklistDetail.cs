﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class ItemChecklistDetail
    {
        public static IQueryable<ItemChecklistDetail> GetAllItemCheclistDetail()
        {
            return CurrentDataContext.CurrentContext.ItemChecklistDetails.Where(x => x.IsActive == true);
        }
        public static ItemChecklistDetail GetItemDetailByID(long id)
        {
            return CurrentDataContext.CurrentContext.ItemChecklistDetails.FirstOrDefault(x => x.ChecklistDetail == id);
        }

    }
}
