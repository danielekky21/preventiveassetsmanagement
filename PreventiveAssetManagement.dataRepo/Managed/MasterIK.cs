﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class MasterIK
    {
        public static IQueryable<MasterIK> GetAllIK() {

            return CurrentDataContext.CurrentContext.MasterIKs;
        }
        public static MasterIK GetIKByID(string id)
        {
            return CurrentDataContext.CurrentContext.MasterIKs.FirstOrDefault(x => x.IKCode == id);
        }
        public static IQueryable<IKCategory> GetCategoryByIKCode(string ikcode)
        {
            return CurrentDataContext.CurrentContext.IKCategories.Where(x => x.MasterIK1.IKCode == ikcode);
        }
        public static IKCategory GetCategoryByCategoryID(string id)
        {
            return CurrentDataContext.CurrentContext.IKCategories.FirstOrDefault(x => x.IKCategory1 == id);
        }
        public static IQueryable<IKItem> GetAllIKItemByCategory(string id)
        {
            return CurrentDataContext.CurrentContext.IKItems.Where(x => x.IKCategory == id);
        }
        public static bool CheckIKcode(string search)
        {
            var ikcode = CurrentDataContext.CurrentContext.MasterIKs.FirstOrDefault(x => x.IKCode.ToLower() == search);
            bool stat;
            if (ikcode != null)
            {
                stat = true;
            }
            else
            {
                stat = false;
            }
            return stat;
        }
        public static bool CheckIKCate(string search)
        {
            var ikcode = CurrentDataContext.CurrentContext.IKCategories.FirstOrDefault(x => x.IKCategory1.ToLower() == search);
            bool stat;
            if (ikcode != null)
            {
                stat = true;
            }
            else
            {
                stat = false;
            }
            return stat;
        }
    }
}
