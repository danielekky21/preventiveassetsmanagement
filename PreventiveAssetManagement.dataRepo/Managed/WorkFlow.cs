﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public partial class WorkFlow
    {
        public static IQueryable<WorkFlow> GetWorkFlowOpenByApproval(string id)
        {
            return CurrentDataContext.CurrentContext.WorkFlows.Where(x => x.WorkFlowStatus == "D" && x.WorkFlowApproval == "id");
        }
        public static int CountWorkFlow(string id)
        {
            return CurrentDataContext.CurrentContext.WorkFlows.Where(x => x.WorkFlowApproval == id && x.WorkFlowStatus == "S").Count();
        }
        public static IQueryable<WorkFlow> GetWorkFlowSubmitByApproval(string id)
        {
            return CurrentDataContext.CurrentContext.WorkFlows.Where(x => x.WorkFlowStatus == "S" && x.WorkFlowApproval == id);
        }
        public static WorkFlow GetWorkFlowByWorkFlowID(int id)
        {
            return CurrentDataContext.CurrentContext.WorkFlows.FirstOrDefault(x => x.WorkFlowID == id);
        }
        public static WorkFlow GetWorkFlowByWorkOrderID(string id)
        {
            return CurrentDataContext.CurrentContext.WorkFlows.FirstOrDefault(x => x.WorkFlowWorkOrderID == id);
        }
        public static IQueryable<WorkFlow> GetAllOtherWorkFlow(string id)
        {
            return CurrentDataContext.CurrentContext.WorkFlows.Where(x => x.WorkFlowWorkOrderID == id);
        }
    }
}
