﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo.Managed
{
    public class RepairMaintenanceAssetReport
    {
        public string AssetCategoryCode { get; set; }
        public string AssetTypeCode { get; set; }
        public string AssetTypeDesc { get; set; }
        public string WorkOrderDate { get; set; }
        public string WoNo { get; set; }
        public string MaintenanceType { get; set; }
        public string Jumlah { get; set; }
        public string satuan { get; set; }
        public string Remarks { get; set; }
    }
}
