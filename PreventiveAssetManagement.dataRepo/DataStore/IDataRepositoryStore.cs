﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreventiveAssetManagement.dataRepo
{
    public interface IDataRepositoryStore
    {
        object this[string key] { get; set; }
    }
}
