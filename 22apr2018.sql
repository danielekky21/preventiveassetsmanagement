USE [master]
GO
/****** Object:  Database [PreventiveAsset]    Script Date: 4/22/2018 4:23:57 PM ******/
CREATE DATABASE [PreventiveAsset]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PreventiveAsset', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\PreventiveAsset.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PreventiveAsset_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\PreventiveAsset_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PreventiveAsset] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PreventiveAsset].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PreventiveAsset] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PreventiveAsset] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PreventiveAsset] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PreventiveAsset] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PreventiveAsset] SET ARITHABORT OFF 
GO
ALTER DATABASE [PreventiveAsset] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PreventiveAsset] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PreventiveAsset] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PreventiveAsset] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PreventiveAsset] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PreventiveAsset] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PreventiveAsset] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PreventiveAsset] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PreventiveAsset] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PreventiveAsset] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PreventiveAsset] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PreventiveAsset] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PreventiveAsset] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PreventiveAsset] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PreventiveAsset] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PreventiveAsset] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PreventiveAsset] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PreventiveAsset] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PreventiveAsset] SET  MULTI_USER 
GO
ALTER DATABASE [PreventiveAsset] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PreventiveAsset] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PreventiveAsset] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PreventiveAsset] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [PreventiveAsset]
GO
/****** Object:  Table [dbo].[AssetIntervalInformation]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssetIntervalInformation](
	[IntervalInformationID] [int] NOT NULL,
	[IKCode] [nvarchar](300) NULL,
	[Interval] [varchar](50) NULL,
	[MaintenanceDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_AssetIntervalInformation] PRIMARY KEY CLUSTERED 
(
	[IntervalInformationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IKCategory]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IKCategory](
	[IKCategory] [nvarchar](100) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[MasterIK] [nvarchar](300) NULL,
 CONSTRAINT [PK_IKCategory] PRIMARY KEY CLUSTERED 
(
	[IKCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IKCondition]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IKCondition](
	[IKConditionId] [int] IDENTITY(1,1) NOT NULL,
	[WorkOrderManualID] [nvarchar](100) NULL,
	[Condition] [varchar](50) NULL,
	[Remarks] [varchar](500) NULL,
	[IKCode] [nvarchar](300) NULL,
	[IKCategory] [nvarchar](100) NULL,
	[IKitemID] [int] NULL,
 CONSTRAINT [PK_IKCondition] PRIMARY KEY CLUSTERED 
(
	[IKConditionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IKConditionVendor]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IKConditionVendor](
	[IKConditionVendor] [int] IDENTITY(1,1) NOT NULL,
	[VendorName] [varchar](100) NULL,
	[StartedBy] [varchar](50) NULL,
	[WorkStartDate] [datetime] NULL,
	[WorkEndDate] [datetime] NULL,
	[CompletedBy] [varchar](50) NULL,
	[PICRemarks] [varchar](300) NULL,
	[WorkOrderManual] [nvarchar](100) NULL,
	[IKCode] [nvarchar](300) NULL,
	[IKCategory] [nvarchar](300) NULL,
 CONSTRAINT [PK_IKConditionVendor] PRIMARY KEY CLUSTERED 
(
	[IKConditionVendor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IKItem]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IKItem](
	[IKItem] [int] IDENTITY(1,1) NOT NULL,
	[IKCategory] [nvarchar](100) NULL,
	[Item] [varchar](300) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_IKItem] PRIMARY KEY CLUSTERED 
(
	[IKItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Interval]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Interval](
	[IntervalID] [int] IDENTITY(1,1) NOT NULL,
	[Interval] [varchar](300) NULL,
	[MaintenanceDate] [datetime] NULL,
	[IKCode] [nvarchar](300) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[AssetCategoryID] [nvarchar](300) NULL,
 CONSTRAINT [PK_Interval] PRIMARY KEY CLUSTERED 
(
	[IntervalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterAssetCategory]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterAssetCategory](
	[AssetCategoryID] [nvarchar](300) NOT NULL,
	[AssetGroup] [varchar](50) NULL,
	[Departement] [varchar](50) NULL,
	[AssetCategory] [varchar](50) NULL,
	[Description] [varchar](max) NULL,
	[Location] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[InstalmentDate] [datetime] NULL,
	[Lifetime] [int] NULL,
	[UserGroup] [nvarchar](50) NULL,
	[isGenerated] [bit] NULL,
 CONSTRAINT [PK_MasterAssetCategory] PRIMARY KEY CLUSTERED 
(
	[AssetCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterAssetType]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterAssetType](
	[MasterAssetTypeID] [nvarchar](300) NOT NULL,
	[Description] [varchar](max) NULL,
	[AssetCategoryID] [nvarchar](300) NULL,
	[ReplacementDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[Lifetime] [varchar](50) NULL,
	[Replacement] [int] NULL,
	[InstallmentDate] [datetime] NULL,
 CONSTRAINT [PK_MasterAssetType] PRIMARY KEY CLUSTERED 
(
	[MasterAssetTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterIK]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterIK](
	[IKCode] [nvarchar](300) NOT NULL,
	[IKDescription] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[Departement] [varchar](300) NULL,
 CONSTRAINT [PK_MasterIK] PRIMARY KEY CLUSTERED 
(
	[IKCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterInterval]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterInterval](
	[IntervalID] [int] IDENTITY(1,1) NOT NULL,
	[IntervalGroup] [varchar](100) NULL,
	[IntervalCode] [varchar](100) NULL,
	[IntervalValue] [int] NULL,
	[Description] [varchar](300) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdadatedBy] [varchar](50) NULL,
	[IsEnabled] [bit] NOT NULL,
	[IntervalType] [varchar](50) NULL,
 CONSTRAINT [PK_MasterInterval] PRIMARY KEY CLUSTERED 
(
	[IntervalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterLocation]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterLocation](
	[LocationCode] [nvarchar](100) NOT NULL,
	[Subportofolio] [varchar](30) NULL,
	[Floor] [varchar](30) NULL,
	[LocationArea] [varchar](100) NULL,
	[Description] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
 CONSTRAINT [PK_MasterLocation] PRIMARY KEY CLUSTERED 
(
	[LocationCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterUser]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterUser](
	[UserID] [varchar](100) NOT NULL,
	[Password] [nvarchar](200) NULL,
	[Name] [varchar](300) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[IsDeleted] [bit] NULL,
	[Role] [varchar](50) NULL,
 CONSTRAINT [PK_MasterUser] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserGroupDetail]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserGroupDetail](
	[UserGroupDetailID] [int] IDENTITY(1,1) NOT NULL,
	[UserGroupID] [nvarchar](50) NULL,
	[UserID] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreratedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_UserGroupDetail] PRIMARY KEY CLUSTERED 
(
	[UserGroupDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserGroupHeader]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserGroupHeader](
	[UserGroupId] [nvarchar](50) NOT NULL,
	[UserGroupName] [varchar](50) NULL,
	[UserGroupLeader] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateBy] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_UserGroupHeader] PRIMARY KEY CLUSTERED 
(
	[UserGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WorkFlow]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WorkFlow](
	[WorkFlowID] [int] IDENTITY(1,1) NOT NULL,
	[WorkFlowWorkOrderID] [nvarchar](300) NULL,
	[WorkFlowApproval] [varchar](50) NULL,
	[WorkFlowStatus] [varchar](50) NULL,
	[WorkFlowRemark] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[ApprovedTime] [datetime] NULL,
	[Remark] [varchar](max) NULL,
 CONSTRAINT [PK_WorkFlow] PRIMARY KEY CLUSTERED 
(
	[WorkFlowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WorkFlowHistory]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WorkFlowHistory](
	[WorkFlowLogID] [int] IDENTITY(1,1) NOT NULL,
	[WorkFlowStatus] [varchar](50) NULL,
	[WorkFlowUpdatedDate] [datetime] NULL,
	[WorkFlowUpdatedBy] [varchar](50) NULL,
	[WorkOrderID] [nvarchar](300) NULL,
 CONSTRAINT [PK_WorkFlowHistory] PRIMARY KEY CLUSTERED 
(
	[WorkFlowLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WorkOrderManual]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WorkOrderManual](
	[WorkOderManualID] [nvarchar](100) NOT NULL,
	[WorkOrderDate] [datetime] NULL,
	[CategoryAssetID] [nvarchar](300) NULL,
	[IKCode] [nvarchar](300) NULL,
	[VendorName] [nvarchar](300) NULL,
	[WorkStartDate] [datetime] NULL,
	[StartedBy] [varchar](50) NULL,
	[WorkEndDate] [datetime] NULL,
	[CompleteBy] [varchar](50) NULL,
	[PICRemarks] [varchar](max) NULL,
	[Status] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[Subportopolio] [varchar](50) NULL,
	[CategoryDescription] [varchar](300) NULL,
	[WorkOrderLeader] [varchar](50) NULL,
	[AssetLocationCode] [nvarchar](100) NULL,
	[IKDescription] [varchar](300) NULL,
	[IKCategory] [varchar](100) NULL,
	[isactive] [bit] NULL,
 CONSTRAINT [PK_WorkOrderManual] PRIMARY KEY CLUSTERED 
(
	[WorkOderManualID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WorkOrderManualMaintenance]    Script Date: 4/22/2018 4:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WorkOrderManualMaintenance](
	[MaintenanceID] [int] IDENTITY(1,1) NOT NULL,
	[AssetTypeID] [nvarchar](300) NULL,
	[WorkOrderManualID] [nvarchar](100) NULL,
	[Jumlah] [varchar](50) NULL,
	[Satuan] [varchar](50) NULL,
	[Remarks] [varchar](300) NULL,
	[maintenanceType] [varchar](50) NULL,
 CONSTRAINT [PK_WorkOrderManualMaintenance] PRIMARY KEY CLUSTERED 
(
	[MaintenanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'AUX - Trafo AUX Gas Engine 1Y', NULL, NULL, NULL, NULL, 1, N'GEG-1YEARLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'BT1 - Battery 200AH 1Y', NULL, NULL, NULL, NULL, 1, N'GEG-1YEARLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'BT1 - Battery Monthly', NULL, NULL, NULL, NULL, 1, N'GEG-MONTHLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'BT2 - Battery 18 AH 1Y', NULL, NULL, NULL, NULL, 1, N'GEG-1YEARLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'BT2- Battery Monthly', NULL, NULL, NULL, NULL, 1, N'GEG-MONTHLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'EXH - Exhaust Fan 1Y', NULL, NULL, NULL, NULL, 1, N'GEG-1YEARLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'EXH - Exhaust Fan Monthly', NULL, NULL, NULL, NULL, 1, N'GEG-MONTHLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'GCP - Panel GCP Monthly', NULL, NULL, NULL, NULL, 1, N'GEG-MONTHLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'GEG - Gas Engine 1000', NULL, NULL, NULL, NULL, 1, N'GEG-PM01000')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'GEG - Gas Engine 2000', NULL, NULL, NULL, NULL, 1, N'GEG-PM02000')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'GEG - Gas Engine 500', NULL, NULL, NULL, NULL, 1, N'GEG-PM00500')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'IT1 - Intake Motor Fan 1 Monthly', NULL, NULL, NULL, NULL, 1, N'GEG-MONTHLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'IT1 - Motor Intake Fan 1 1Y', NULL, NULL, NULL, NULL, 1, N'GEG-1YEARLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'IT2 - Intake Motor Fan 2 Monthly', NULL, NULL, NULL, NULL, 1, N'GEG-MONTHLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'IT2 - Motor Intake Fan 2 1Y', NULL, NULL, NULL, NULL, 1, N'GEG-1YEARLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'PIF - Panel GEG Monthly', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'PIF - Panel Monthly', NULL, NULL, NULL, NULL, 1, N'GEG-MONTHLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'PMB - Panel Junction 2WEEKLY', NULL, NULL, NULL, NULL, 1, N'GEG-2WEEKLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'RAD - Radiator 2M', NULL, NULL, NULL, NULL, 1, N'GEG-2MONTHLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'TGE - Trafo Gas Engine 1Y', NULL, NULL, NULL, NULL, 1, N'GEG-1YEARLY')
INSERT [dbo].[IKCategory] ([IKCategory], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsActive], [MasterIK]) VALUES (N'TRX - Panel Travo Monthly', NULL, NULL, NULL, NULL, 1, N'GEG-MONTHLY')
SET IDENTITY_INSERT [dbo].[IKCondition] ON 

INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1469, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT1 - Battery Monthly', 311)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1470, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT1 - Battery Monthly', 312)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1471, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT1 - Battery Monthly', 313)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1472, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT1 - Battery Monthly', 314)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1473, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT1 - Battery Monthly', 315)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1474, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT1 - Battery Monthly', 316)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1475, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT1 - Battery Monthly', 317)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1476, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT1 - Battery Monthly', 318)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1477, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT2- Battery Monthly', 319)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1478, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT2- Battery Monthly', 320)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1479, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT2- Battery Monthly', 321)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1480, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT2- Battery Monthly', 322)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1481, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT2- Battery Monthly', 323)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1482, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT2- Battery Monthly', 324)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1483, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT2- Battery Monthly', 325)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1484, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'BT2- Battery Monthly', 326)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1485, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 291)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1486, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 292)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1487, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 293)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1488, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 294)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1489, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 295)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1490, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'GCP - Panel GCP Monthly', 286)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1491, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'GCP - Panel GCP Monthly', 287)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1492, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'GCP - Panel GCP Monthly', 288)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1493, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'GCP - Panel GCP Monthly', 289)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1494, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'GCP - Panel GCP Monthly', 290)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1495, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'IT1 - Intake Motor Fan 1 Monthly', 296)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1496, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'IT1 - Intake Motor Fan 1 Monthly', 297)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1497, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'IT1 - Intake Motor Fan 1 Monthly', 298)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1498, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'IT1 - Intake Motor Fan 1 Monthly', 299)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1499, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'IT1 - Intake Motor Fan 1 Monthly', 300)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1500, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 301)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1501, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 302)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1502, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 303)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1503, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 304)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1504, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 305)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1505, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'PIF - Panel Monthly', 281)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1506, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'PIF - Panel Monthly', 282)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1507, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'PIF - Panel Monthly', 283)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1508, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'PIF - Panel Monthly', 284)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1509, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'PIF - Panel Monthly', 285)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1510, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'TRX - Panel Travo Monthly', 306)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1511, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'TRX - Panel Travo Monthly', 307)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1512, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'TRX - Panel Travo Monthly', 308)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1513, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'TRX - Panel Travo Monthly', 309)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1514, N'ENG-GEG-2018-04-0002', N'Good', N'', NULL, N'TRX - Panel Travo Monthly', 310)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1515, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT1 - Battery Monthly', 311)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1516, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT1 - Battery Monthly', 312)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1517, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT1 - Battery Monthly', 313)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1518, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT1 - Battery Monthly', 314)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1519, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT1 - Battery Monthly', 315)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1520, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT1 - Battery Monthly', 316)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1521, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT1 - Battery Monthly', 317)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1522, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT1 - Battery Monthly', 318)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1523, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT2- Battery Monthly', 319)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1524, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT2- Battery Monthly', 320)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1525, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT2- Battery Monthly', 321)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1526, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT2- Battery Monthly', 322)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1527, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT2- Battery Monthly', 323)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1528, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT2- Battery Monthly', 324)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1529, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT2- Battery Monthly', 325)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1530, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'BT2- Battery Monthly', 326)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1531, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 291)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1532, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 292)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1533, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 293)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1534, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 294)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1535, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 295)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1536, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'GCP - Panel GCP Monthly', 286)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1537, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'GCP - Panel GCP Monthly', 287)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1538, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'GCP - Panel GCP Monthly', 288)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1539, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'GCP - Panel GCP Monthly', 289)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1540, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'GCP - Panel GCP Monthly', 290)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1541, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'IT1 - Intake Motor Fan 1 Monthly', 296)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1542, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'IT1 - Intake Motor Fan 1 Monthly', 297)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1543, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'IT1 - Intake Motor Fan 1 Monthly', 298)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1544, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'IT1 - Intake Motor Fan 1 Monthly', 299)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1545, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'IT1 - Intake Motor Fan 1 Monthly', 300)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1546, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 301)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1547, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 302)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1548, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 303)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1549, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 304)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1550, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 305)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1551, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'PIF - Panel Monthly', 281)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1552, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'PIF - Panel Monthly', 282)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1553, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'PIF - Panel Monthly', 283)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1554, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'PIF - Panel Monthly', 284)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1555, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'PIF - Panel Monthly', 285)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1556, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'TRX - Panel Travo Monthly', 306)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1557, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'TRX - Panel Travo Monthly', 307)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1558, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'TRX - Panel Travo Monthly', 308)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1559, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'TRX - Panel Travo Monthly', 309)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1560, N'ENG-GEG-2018-04-0001', N'Good', N'', NULL, N'TRX - Panel Travo Monthly', 310)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1561, N'ENG-GEG-2018-04-0003', N'Good', N'', NULL, N'BT1 - Battery Monthly', 311)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1562, N'ENG-GEG-2018-04-0003', N'Good', N'', NULL, N'BT1 - Battery Monthly', 312)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1563, N'ENG-GEG-2018-04-0003', N'Good', N'', NULL, N'BT1 - Battery Monthly', 313)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1564, N'ENG-GEG-2018-04-0003', N'Good', N'', NULL, N'BT2- Battery Monthly', 319)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1565, N'ENG-GEG-2018-04-0003', N'Good', N'', NULL, N'BT2- Battery Monthly', 320)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1566, N'ENG-GEG-2018-04-0003', N'Good', N'', NULL, N'BT2- Battery Monthly', 321)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1567, N'ENG-GEG-2018-04-0003', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 291)
GO
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1568, N'ENG-GEG-2018-04-0003', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 292)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1569, N'ENG-GEG-2018-04-0003', N'Good', N'', NULL, N'EXH - Exhaust Fan Monthly', 293)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1570, N'ENG-GEG-2018-04-0003', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 301)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1571, N'ENG-GEG-2018-04-0003', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 302)
INSERT [dbo].[IKCondition] ([IKConditionId], [WorkOrderManualID], [Condition], [Remarks], [IKCode], [IKCategory], [IKitemID]) VALUES (1572, N'ENG-GEG-2018-04-0003', N'Good', N'', NULL, N'IT2 - Intake Motor Fan 2 Monthly', 303)
SET IDENTITY_INSERT [dbo].[IKCondition] OFF
SET IDENTITY_INSERT [dbo].[IKConditionVendor] ON 

INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (211, N'PT ABC', N'001638', CAST(N'2018-04-12 00:00:00.000' AS DateTime), CAST(N'2018-04-12 16:08:57.000' AS DateTime), NULL, N'', N'ENG-GEG-2018-04-0002', NULL, N'BT1 - Battery Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (212, N'Vendor A', N'001638', CAST(N'2018-04-12 00:00:00.000' AS DateTime), CAST(N'2018-04-14 00:00:00.000' AS DateTime), NULL, N'', N'ENG-GEG-2018-04-0002', NULL, N'BT2- Battery Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (213, N'Vendor A', N'001638', CAST(N'2018-04-13 00:00:00.000' AS DateTime), CAST(N'2018-04-12 16:08:57.000' AS DateTime), NULL, N'', N'ENG-GEG-2018-04-0002', NULL, N'EXH - Exhaust Fan Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (214, N'Vendor A', N'001638', CAST(N'2018-04-12 00:00:00.000' AS DateTime), CAST(N'2018-04-12 16:08:57.000' AS DateTime), NULL, N'', N'ENG-GEG-2018-04-0002', NULL, N'GCP - Panel GCP Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (215, N'Vendor A', N'001638', CAST(N'2018-04-12 00:00:00.000' AS DateTime), CAST(N'2018-04-12 16:08:57.000' AS DateTime), NULL, N'', N'ENG-GEG-2018-04-0002', NULL, N'IT1 - Intake Motor Fan 1 Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (216, N'Vendor A', N'001638', CAST(N'2018-04-12 00:00:00.000' AS DateTime), CAST(N'2018-04-12 16:08:57.000' AS DateTime), NULL, N'', N'ENG-GEG-2018-04-0002', NULL, N'IT2 - Intake Motor Fan 2 Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (217, N'Vendor A', N'001638', CAST(N'2018-04-12 00:00:00.000' AS DateTime), CAST(N'2018-04-12 16:08:57.000' AS DateTime), NULL, N'', N'ENG-GEG-2018-04-0002', NULL, N'PIF - Panel Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (218, N'Vendor A', N'001638', CAST(N'2018-04-12 00:00:00.000' AS DateTime), CAST(N'2018-04-12 16:08:57.000' AS DateTime), NULL, N'', N'ENG-GEG-2018-04-0002', NULL, N'TRX - Panel Travo Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (219, N'Vendor A', N'bangun', CAST(N'2018-04-10 00:00:00.000' AS DateTime), CAST(N'2018-04-13 19:43:18.460' AS DateTime), N'ananta', N'pekerjaan berlangsung 2 hari
dikerjakan oleh Bangun  dan diselesaikan oleh Ananta.
pekrjaan berjalan bagus', N'ENG-GEG-2018-04-0001', NULL, N'BT1 - Battery Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (220, N'Vendor A', N'001609', CAST(N'2018-03-20 00:00:00.000' AS DateTime), CAST(N'2018-04-13 19:43:18.460' AS DateTime), N'001623', N'', N'ENG-GEG-2018-04-0001', NULL, N'BT2- Battery Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (221, N'Vendor A', N'001609', CAST(N'2018-05-02 00:00:00.000' AS DateTime), CAST(N'2018-04-13 19:43:18.460' AS DateTime), N'1213232', N'', N'ENG-GEG-2018-04-0001', NULL, N'EXH - Exhaust Fan Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (222, N'Vendor A', N'001609', CAST(N'2018-05-03 00:00:00.000' AS DateTime), CAST(N'2018-04-13 19:43:18.460' AS DateTime), N'1213232', N'', N'ENG-GEG-2018-04-0001', NULL, N'GCP - Panel GCP Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (223, N'PT ABC', N'001609', CAST(N'2018-05-04 00:00:00.000' AS DateTime), CAST(N'2018-04-13 19:43:18.460' AS DateTime), N'Vebbi', N'', N'ENG-GEG-2018-04-0001', NULL, N'IT1 - Intake Motor Fan 1 Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (224, N'Vendor A', N'001609', CAST(N'2018-04-20 00:00:00.000' AS DateTime), CAST(N'2018-04-13 19:43:18.460' AS DateTime), N'Vebbi', N'', N'ENG-GEG-2018-04-0001', NULL, N'IT2 - Intake Motor Fan 2 Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (225, N'Vendor A', N'001609', CAST(N'2018-04-27 00:00:00.000' AS DateTime), CAST(N'2018-04-13 19:43:18.477' AS DateTime), N'Vebbi', N'', N'ENG-GEG-2018-04-0001', NULL, N'PIF - Panel Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (226, N'Vendor A', N'001609', CAST(N'2018-05-05 00:00:00.000' AS DateTime), CAST(N'2018-04-13 19:43:18.477' AS DateTime), N'Vebbi', N'', N'ENG-GEG-2018-04-0001', NULL, N'TRX - Panel Travo Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (227, N'', N'001513', CAST(N'2018-04-17 00:00:00.000' AS DateTime), CAST(N'2018-04-17 00:00:00.000' AS DateTime), N'Jupi', N'', N'ENG-GEG-2018-04-0003', NULL, N'BT1 - Battery Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (228, N'', N'001513', CAST(N'2018-04-17 00:00:00.000' AS DateTime), CAST(N'2018-04-17 00:00:00.000' AS DateTime), N'Jupi', N'', N'ENG-GEG-2018-04-0003', NULL, N'BT2- Battery Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (229, N'', N'001513', CAST(N'2018-04-18 00:00:00.000' AS DateTime), CAST(N'2018-04-18 00:00:00.000' AS DateTime), N'Jupi', N'', N'ENG-GEG-2018-04-0003', NULL, N'EXH - Exhaust Fan Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (230, N'', N'001513', NULL, NULL, N'', N'', N'ENG-GEG-2018-04-0003', NULL, N'GCP - Panel GCP Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (231, N'', N'001513', NULL, NULL, N'', N'', N'ENG-GEG-2018-04-0003', NULL, N'IT1 - Intake Motor Fan 1 Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (232, N'', N'001513', CAST(N'2018-04-17 00:00:00.000' AS DateTime), CAST(N'2018-04-17 00:00:00.000' AS DateTime), N'', N'', N'ENG-GEG-2018-04-0003', NULL, N'IT2 - Intake Motor Fan 2 Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (233, N'', N'001513', NULL, NULL, N'', N'', N'ENG-GEG-2018-04-0003', NULL, N'PIF - Panel Monthly')
INSERT [dbo].[IKConditionVendor] ([IKConditionVendor], [VendorName], [StartedBy], [WorkStartDate], [WorkEndDate], [CompletedBy], [PICRemarks], [WorkOrderManual], [IKCode], [IKCategory]) VALUES (234, N'', N'001513', NULL, NULL, N'', N'', N'ENG-GEG-2018-04-0003', NULL, N'TRX - Panel Travo Monthly')
SET IDENTITY_INSERT [dbo].[IKConditionVendor] OFF
SET IDENTITY_INSERT [dbo].[IKItem] ON 

INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (221, N'GEG - Gas Engine 500', N'Engine running Idle ( no load )', N'001642', NULL, CAST(N'2018-04-11 22:25:32.443' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (222, N'GEG - Gas Engine 500', N'Ambil lube oil sample test lab', N'001642', NULL, CAST(N'2018-04-11 22:25:32.460' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (223, N'GEG - Gas Engine 500', N'Engine Off', N'001642', NULL, CAST(N'2018-04-11 22:25:32.460' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (224, N'GEG - Gas Engine 500', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 22:25:32.460' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (225, N'GEG - Gas Engine 500', N'Pasang tanta LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 22:25:32.460' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (226, N'GEG - Gas Engine 500', N'Cleaning Air Bag Filter', N'001642', NULL, CAST(N'2018-04-11 22:25:32.460' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (227, N'GEG - Gas Engine 500', N'Cleaning filter oil Separator', N'001642', NULL, CAST(N'2018-04-11 22:25:32.460' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (228, N'GEG - Gas Engine 500', N'Cleaning bag filter room', N'001642', NULL, CAST(N'2018-04-11 22:25:32.477' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (229, N'GEG - Gas Engine 500', N'Cleaning Sensor Air Intake', N'001642', NULL, CAST(N'2018-04-11 22:25:32.477' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (230, N'GEG - Gas Engine 500', N'Test Idle rpm dan dengan Load kw', N'001642', NULL, CAST(N'2018-04-11 22:25:32.477' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (231, N'GEG - Gas Engine 1000', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 22:35:28.277' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (232, N'GEG - Gas Engine 1000', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 22:35:28.277' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (233, N'GEG - Gas Engine 1000', N'Cleaning body Generator', N'001642', NULL, CAST(N'2018-04-11 22:35:28.277' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (234, N'GEG - Gas Engine 1000', N'Greasing Bearing generator D Side & ND Side', N'001642', NULL, CAST(N'2018-04-11 22:35:28.277' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (235, N'GEG - Gas Engine 1000', N'Check Rubber coupling Generator- Engine ( Visual )', N'001642', NULL, CAST(N'2018-04-11 22:35:28.277' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (236, N'GEG - Gas Engine 1000', N'Check Bolt & Nut Generator', N'001642', NULL, CAST(N'2018-04-11 22:35:28.277' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (237, N'GEG - Gas Engine 1000', N'Cleaning body Throthle valve', N'001642', NULL, CAST(N'2018-04-11 22:35:28.293' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (238, N'GEG - Gas Engine 1000', N'Greasing Throthle valve', N'001642', NULL, CAST(N'2018-04-11 22:35:28.293' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (239, N'GEG - Gas Engine 1000', N'Check rod end ,Bolt & Nut', N'001642', NULL, CAST(N'2018-04-11 22:35:28.293' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (240, N'GEG - Gas Engine 1000', N'Check Spark plug conector', N'001642', NULL, CAST(N'2018-04-11 22:35:28.293' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (241, N'GEG - Gas Engine 1000', N'Check Spark plug, Regap 0,3mm', N'001642', NULL, CAST(N'2018-04-11 22:35:28.293' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (242, N'GEG - Gas Engine 1000', N'Check Prechamber gas valve', N'001642', NULL, CAST(N'2018-04-11 22:35:28.293' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (243, N'GEG - Gas Engine 1000', N'Test Idle rpm dan dengan Load kw', N'001642', NULL, CAST(N'2018-04-11 22:35:28.310' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (244, N'GEG - Gas Engine 1000', N'Check & Regap electroda spark plug ( 0.3 MM )', N'001642', NULL, CAST(N'2018-04-11 22:35:28.310' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (245, N'GEG - Gas Engine 1000', N'Install Spark Plug', N'001642', NULL, CAST(N'2018-04-11 22:35:28.310' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (246, N'GEG - Gas Engine 1000', N'Instal Cable conector', N'001642', NULL, CAST(N'2018-04-11 22:35:28.310' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (247, N'GEG - Gas Engine 1000', N'Test Idle rpm', N'001642', NULL, CAST(N'2018-04-11 22:35:28.310' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (248, N'GEG - Gas Engine 2000', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 22:43:32.670' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (249, N'GEG - Gas Engine 2000', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 22:43:32.670' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (250, N'GEG - Gas Engine 2000', N'Cleaning body Generator', N'001642', NULL, CAST(N'2018-04-11 22:43:32.670' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (251, N'GEG - Gas Engine 2000', N'Greasing Bearing generator D Side & ND Side', N'001642', NULL, CAST(N'2018-04-11 22:43:32.670' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (252, N'GEG - Gas Engine 2000', N'Check Rubber coupling Generator- Engine ( Visual )', N'001642', NULL, CAST(N'2018-04-11 22:43:32.670' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (253, N'GEG - Gas Engine 2000', N'Check Bolt & Nut Generator', N'001642', NULL, CAST(N'2018-04-11 22:43:32.670' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (254, N'GEG - Gas Engine 2000', N'Cleaning body Throthle valve', N'001642', NULL, CAST(N'2018-04-11 22:43:32.687' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (255, N'GEG - Gas Engine 2000', N'Greasing Throthle valve', N'001642', NULL, CAST(N'2018-04-11 22:43:32.687' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (256, N'GEG - Gas Engine 2000', N'Check rod end ,Bolt & Nut', N'001642', NULL, CAST(N'2018-04-11 22:43:32.687' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (257, N'GEG - Gas Engine 2000', N'Check Spark plug conector', N'001642', NULL, CAST(N'2018-04-11 22:43:32.687' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (258, N'GEG - Gas Engine 2000', N'Check Spark plug, Regap 0,3mm', N'001642', NULL, CAST(N'2018-04-11 22:43:32.687' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (259, N'GEG - Gas Engine 2000', N'Check Prechamber gas valve', N'001642', NULL, CAST(N'2018-04-11 22:43:32.687' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (260, N'GEG - Gas Engine 2000', N'Check O-ring Holder Spark plug', N'001642', NULL, CAST(N'2018-04-11 22:43:32.703' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (261, N'GEG - Gas Engine 2000', N'Check O-ring tutup cyl.Haed', N'001642', NULL, CAST(N'2018-04-11 22:43:32.703' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (262, N'GEG - Gas Engine 2000', N'Check dan Redjust valve Clearance ( IN : 0.8 mm / EX : 1 mm )', N'001642', NULL, CAST(N'2018-04-11 22:43:32.703' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (263, N'GEG - Gas Engine 2000', N'Check Spring dan Tappet extension join', N'001642', NULL, CAST(N'2018-04-11 22:43:32.703' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (264, N'GEG - Gas Engine 2000', N'Visual Check Gas Mixer valve', N'001642', NULL, CAST(N'2018-04-11 22:43:32.703' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (265, N'GEG - Gas Engine 2000', N'Test Idle rpm dan dengan Load kw', N'001642', NULL, CAST(N'2018-04-11 22:43:32.703' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (266, N'PMB - Panel Junction 2WEEKLY', N'GE ruuning load min 1350 kw', N'001642', NULL, CAST(N'2018-04-11 22:44:33.870' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (267, N'PMB - Panel Junction 2WEEKLY', N'Visual check Ignition coil', N'001642', NULL, CAST(N'2018-04-11 22:44:33.887' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (268, N'PMB - Panel Junction 2WEEKLY', N'Visual Check Cable Ignition Coil', N'001642', NULL, CAST(N'2018-04-11 22:44:33.887' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (269, N'PMB - Panel Junction 2WEEKLY', N'Pengukuran voltage Spark plug Min.18KV', N'001642', NULL, CAST(N'2018-04-11 22:44:33.887' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (281, N'PIF - Panel Monthly', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 22:50:27.767' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (282, N'PIF - Panel Monthly', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 22:50:27.767' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (283, N'PIF - Panel Monthly', N'Visual Check Aksesories Panel', N'001642', NULL, CAST(N'2018-04-11 22:50:27.783' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (284, N'PIF - Panel Monthly', N'Kencangkan Mur dan baut ', N'001642', NULL, CAST(N'2018-04-11 22:50:27.783' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (285, N'PIF - Panel Monthly', N'Bersihkan dan Vacum aksesories panel', N'001642', NULL, CAST(N'2018-04-11 22:50:27.783' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (286, N'GCP - Panel GCP Monthly', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 22:52:06.540' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (287, N'GCP - Panel GCP Monthly', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 22:52:06.557' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (288, N'GCP - Panel GCP Monthly', N'Visual Check Aksesories Panel', N'001642', NULL, CAST(N'2018-04-11 22:52:06.557' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (289, N'GCP - Panel GCP Monthly', N'Kencangkan Mur dan baut ', N'001642', NULL, CAST(N'2018-04-11 22:52:06.557' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (290, N'GCP - Panel GCP Monthly', N'Bersihkan dan Vacum aksesories panel', N'001642', NULL, CAST(N'2018-04-11 22:52:06.557' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (291, N'EXH - Exhaust Fan Monthly', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:07:44.753' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (292, N'EXH - Exhaust Fan Monthly', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:07:44.753' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (293, N'EXH - Exhaust Fan Monthly', N'Visual Check Aksesories Panel', N'001642', NULL, CAST(N'2018-04-11 23:07:44.753' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (294, N'EXH - Exhaust Fan Monthly', N'Kencangkan Mur dan baut ', N'001642', NULL, CAST(N'2018-04-11 23:07:44.753' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (295, N'EXH - Exhaust Fan Monthly', N'Bersihkan dan Vacum aksesories panel', N'001642', NULL, CAST(N'2018-04-11 23:07:44.767' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (296, N'IT1 - Intake Motor Fan 1 Monthly', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:08:33.587' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (297, N'IT1 - Intake Motor Fan 1 Monthly', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:08:33.587' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (298, N'IT1 - Intake Motor Fan 1 Monthly', N'Visual Check Aksesories Panel', N'001642', NULL, CAST(N'2018-04-11 23:08:33.587' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (299, N'IT1 - Intake Motor Fan 1 Monthly', N'Kencangkan Mur dan baut', N'001642', NULL, CAST(N'2018-04-11 23:08:33.587' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (300, N'IT1 - Intake Motor Fan 1 Monthly', N'Bersihkan dan Vacum aksesories panel', N'001642', NULL, CAST(N'2018-04-11 23:08:33.587' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (301, N'IT2 - Intake Motor Fan 2 Monthly', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:09:13.947' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (302, N'IT2 - Intake Motor Fan 2 Monthly', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:09:13.947' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (303, N'IT2 - Intake Motor Fan 2 Monthly', N'Visual Check Aksesories Panel', N'001642', NULL, CAST(N'2018-04-11 23:09:13.947' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (304, N'IT2 - Intake Motor Fan 2 Monthly', N'Kencangkan Mur dan baut', N'001642', NULL, CAST(N'2018-04-11 23:09:13.960' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (305, N'IT2 - Intake Motor Fan 2 Monthly', N'Bersihkan dan Vacum aksesories panel', N'001642', NULL, CAST(N'2018-04-11 23:09:13.960' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (306, N'TRX - Panel Travo Monthly', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:09:47.397' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (307, N'TRX - Panel Travo Monthly', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:09:47.397' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (308, N'TRX - Panel Travo Monthly', N'Visual Check Aksesories Panel', N'001642', NULL, CAST(N'2018-04-11 23:09:47.397' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (309, N'TRX - Panel Travo Monthly', N'Kencangkan Mur dan baut', N'001642', NULL, CAST(N'2018-04-11 23:09:47.397' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (310, N'TRX - Panel Travo Monthly', N'Bersihkan dan Vacum aksesories panel', N'001642', NULL, CAST(N'2018-04-11 23:09:47.397' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (311, N'BT1 - Battery Monthly', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:11:26.397' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (312, N'BT1 - Battery Monthly', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:11:26.397' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (313, N'BT1 - Battery Monthly', N'Visual Check Aksesories Battery', N'001642', NULL, CAST(N'2018-04-11 23:11:26.397' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (314, N'BT1 - Battery Monthly', N'Check ketinggian air aki Min.Medium', N'001642', NULL, CAST(N'2018-04-11 23:11:26.397' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (315, N'BT1 - Battery Monthly', N'Test Voltage dan Test Healty Batttery dengan DHC', N'001642', NULL, CAST(N'2018-04-11 23:11:26.413' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (316, N'BT1 - Battery Monthly', N'Bersihkan Kepala Battery dan Body Battery', N'001642', NULL, CAST(N'2018-04-11 23:11:26.413' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (317, N'BT1 - Battery Monthly', N'Check Voltage Charger Battery Min.24 Volt max 26 Volt', N'001642', NULL, CAST(N'2018-04-11 23:11:26.413' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (318, N'BT1 - Battery Monthly', N'Test Running Idle 1500 rpm', N'001642', NULL, CAST(N'2018-04-11 23:11:26.413' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (319, N'BT2- Battery Monthly', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:12:38.123' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (320, N'BT2- Battery Monthly', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:12:38.123' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (321, N'BT2- Battery Monthly', N'Visual Check Aksesories Battery', N'001642', NULL, CAST(N'2018-04-11 23:12:38.123' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (322, N'BT2- Battery Monthly', N'Check ketinggian air aki Min.Medium', N'001642', NULL, CAST(N'2018-04-11 23:12:38.123' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (323, N'BT2- Battery Monthly', N'Test Voltage dan Test Healty Batttery dengan DHC', N'001642', NULL, CAST(N'2018-04-11 23:12:38.123' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (324, N'BT2- Battery Monthly', N'Bersihkan Kepala Battery dan Body Battery', N'001642', NULL, CAST(N'2018-04-11 23:12:38.140' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (325, N'BT2- Battery Monthly', N'Check Voltage Charger Battery Min.24 Volt max 26 Volt', N'001642', NULL, CAST(N'2018-04-11 23:12:38.140' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (326, N'BT2- Battery Monthly', N'Test Running Idle 1500 rpm', N'001642', NULL, CAST(N'2018-04-11 23:12:38.140' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (327, N'RAD - Radiator 2M', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:13:41.440' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (328, N'RAD - Radiator 2M', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:13:41.440' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (329, N'RAD - Radiator 2M', N'Visual Check Aksesories Panel', N'001642', NULL, CAST(N'2018-04-11 23:13:41.457' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (330, N'RAD - Radiator 2M', N'Kencangkan Mur dan baut', N'001642', NULL, CAST(N'2018-04-11 23:13:41.457' AS DateTime), NULL)
GO
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (331, N'RAD - Radiator 2M', N'Bersihkan dan Vacum aksesories panel', N'001642', NULL, CAST(N'2018-04-11 23:13:41.457' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (332, N'EXH - Exhaust Fan 1Y', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:15:06.937' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (333, N'EXH - Exhaust Fan 1Y', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:15:06.937' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (334, N'EXH - Exhaust Fan 1Y', N'Visual Check Aksesories Panel', N'001642', NULL, CAST(N'2018-04-11 23:15:06.937' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (335, N'EXH - Exhaust Fan 1Y', N'Kencangkan Mur dan baut', N'001642', NULL, CAST(N'2018-04-11 23:15:06.937' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (336, N'EXH - Exhaust Fan 1Y', N'Bersihkan dan Vacum aksesories panel', N'001642', NULL, CAST(N'2018-04-11 23:15:06.937' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (337, N'IT1 - Motor Intake Fan 1 1Y', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:15:42.077' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (338, N'IT1 - Motor Intake Fan 1 1Y', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:15:42.077' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (339, N'IT1 - Motor Intake Fan 1 1Y', N'Visual Check Aksesories Panel', N'001642', NULL, CAST(N'2018-04-11 23:15:42.077' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (340, N'IT1 - Motor Intake Fan 1 1Y', N'Kencangkan Mur dan baut', N'001642', NULL, CAST(N'2018-04-11 23:15:42.090' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (341, N'IT1 - Motor Intake Fan 1 1Y', N'Bersihkan dan Vacum aksesories panel', N'001642', NULL, CAST(N'2018-04-11 23:15:42.090' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (342, N'IT2 - Motor Intake Fan 2 1Y', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:16:16.507' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (343, N'IT2 - Motor Intake Fan 2 1Y', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:16:16.507' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (344, N'IT2 - Motor Intake Fan 2 1Y', N'Visual Check Aksesories Panel', N'001642', NULL, CAST(N'2018-04-11 23:16:16.507' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (345, N'IT2 - Motor Intake Fan 2 1Y', N'Kencangkan Mur dan baut', N'001642', NULL, CAST(N'2018-04-11 23:16:16.507' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (346, N'IT2 - Motor Intake Fan 2 1Y', N'Bersihkan dan Vacum aksesories panel', N'001642', NULL, CAST(N'2018-04-11 23:16:16.507' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (347, N'TGE - Trafo Gas Engine 1Y', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:17:54.147' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (348, N'TGE - Trafo Gas Engine 1Y', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:17:54.163' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (349, N'TGE - Trafo Gas Engine 1Y', N'Visual Check Aksesories Transformer', N'001642', NULL, CAST(N'2018-04-11 23:17:54.163' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (350, N'TGE - Trafo Gas Engine 1Y', N'Bersihkan Body Transfromer', N'001642', NULL, CAST(N'2018-04-11 23:17:54.163' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (351, N'TGE - Trafo Gas Engine 1Y', N'Check ketinggian oli di gelas ukur Transformer min.Medium', N'001642', NULL, CAST(N'2018-04-11 23:17:54.163' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (352, N'AUX - Trafo AUX Gas Engine 1Y', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:18:35.677' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (353, N'AUX - Trafo AUX Gas Engine 1Y', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:18:35.697' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (354, N'AUX - Trafo AUX Gas Engine 1Y', N'Visual Check Aksesories Transformer', N'001642', NULL, CAST(N'2018-04-11 23:18:35.697' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (355, N'AUX - Trafo AUX Gas Engine 1Y', N'Bersihkan Body Trasnfromer', N'001642', NULL, CAST(N'2018-04-11 23:18:35.697' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (356, N'AUX - Trafo AUX Gas Engine 1Y', N'Check ketinggian oli di gelas ukur Transformer min.Medium', N'001642', NULL, CAST(N'2018-04-11 23:18:35.697' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (357, N'BT1 - Battery 200AH 1Y', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:20:18.230' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (358, N'BT1 - Battery 200AH 1Y', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:20:18.230' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (359, N'BT1 - Battery 200AH 1Y', N'Visual Check Aksesories Battery', N'001642', NULL, CAST(N'2018-04-11 23:20:18.230' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (360, N'BT1 - Battery 200AH 1Y', N'Check ketinggian air aki Min.Medium', N'001642', NULL, CAST(N'2018-04-11 23:20:18.247' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (361, N'BT1 - Battery 200AH 1Y', N'Test Voltage dan Test Healty Batttery dengan DHC', N'001642', NULL, CAST(N'2018-04-11 23:20:18.247' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (362, N'BT1 - Battery 200AH 1Y', N'Bersihkan Kepala Battery dan Body Battery', N'001642', NULL, CAST(N'2018-04-11 23:20:18.247' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (363, N'BT1 - Battery 200AH 1Y', N'Check Voltage Charger Battery Min.24 Volt max 26 Volt', N'001642', NULL, CAST(N'2018-04-11 23:20:18.247' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (364, N'BT1 - Battery 200AH 1Y', N'Isi air zuur aki pada battery baru', N'001642', NULL, CAST(N'2018-04-11 23:20:18.247' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (365, N'BT1 - Battery 200AH 1Y', N'Ganti battery baru', N'001642', NULL, CAST(N'2018-04-11 23:20:18.247' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (366, N'BT1 - Battery 200AH 1Y', N'Test Running Idle 1500 rpm', N'001642', NULL, CAST(N'2018-04-11 23:20:18.260' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (367, N'BT2 - Battery 18 AH 1Y', N'Service selection swicth posisi OFF', N'001642', NULL, CAST(N'2018-04-11 23:21:03.230' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (368, N'BT2 - Battery 18 AH 1Y', N'Pasang Tanda LOTO di panel GCP', N'001642', NULL, CAST(N'2018-04-11 23:21:03.230' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (369, N'BT2 - Battery 18 AH 1Y', N'Visual Check Aksesories Battery', N'001642', NULL, CAST(N'2018-04-11 23:21:03.243' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (370, N'BT2 - Battery 18 AH 1Y', N'Check ketinggian air aki Min.Medium', N'001642', NULL, CAST(N'2018-04-11 23:21:03.243' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (371, N'BT2 - Battery 18 AH 1Y', N'Test Voltage dan Test Healty Batttery dengan DHC', N'001642', NULL, CAST(N'2018-04-11 23:21:03.243' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (372, N'BT2 - Battery 18 AH 1Y', N'Bersihkan Kepala Battery dan Body Battery', N'001642', NULL, CAST(N'2018-04-11 23:21:03.243' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (373, N'BT2 - Battery 18 AH 1Y', N'Check Voltage Charger Battery Min.24 Volt max 26 Volt', N'001642', NULL, CAST(N'2018-04-11 23:21:03.243' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (374, N'BT2 - Battery 18 AH 1Y', N'Isi air zuur aki pada battery baru', N'001642', NULL, CAST(N'2018-04-11 23:21:03.243' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (375, N'BT2 - Battery 18 AH 1Y', N'Ganti battery baru', N'001642', NULL, CAST(N'2018-04-11 23:21:03.260' AS DateTime), NULL)
INSERT [dbo].[IKItem] ([IKItem], [IKCategory], [Item], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (376, N'BT2 - Battery 18 AH 1Y', N'Test Running Idle 1500 rpm', N'001642', NULL, CAST(N'2018-04-11 23:21:03.260' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[IKItem] OFF
SET IDENTITY_INSERT [dbo].[Interval] ON 

INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (93, N'2M', CAST(N'2018-05-03 00:00:00.000' AS DateTime), N'GEG-2MONTHLY', N'001642', CAST(N'2018-04-12 00:37:45.510' AS DateTime), NULL, NULL, N'ENG-GEG-003')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (94, N'500', CAST(N'2018-05-04 00:00:00.000' AS DateTime), N'GEG-PM00500', N'001642', CAST(N'2018-04-12 00:37:57.597' AS DateTime), NULL, NULL, N'ENG-GEG-004')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (95, N'1000', CAST(N'2018-05-05 00:00:00.000' AS DateTime), N'GEG-PM01000', N'001642', CAST(N'2018-04-12 00:38:08.667' AS DateTime), NULL, NULL, N'ENG-GEG-005')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (96, N'2000', CAST(N'2018-05-06 00:00:00.000' AS DateTime), N'GEG-PM02000', N'001642', CAST(N'2018-04-12 00:38:19.997' AS DateTime), NULL, NULL, N'ENG-GEG-006')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (97, N'500', CAST(N'2018-05-07 00:00:00.000' AS DateTime), N'GEG-PM00500', N'001642', CAST(N'2018-04-12 00:38:29.283' AS DateTime), NULL, NULL, N'ENG-GEG-007')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (98, N'2M', CAST(N'2018-05-09 00:00:00.000' AS DateTime), N'GEG-2MONTHLY', N'001642', CAST(N'2018-04-12 00:38:37.673' AS DateTime), NULL, NULL, N'ENG-GEG-009')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (99, N'2W', CAST(N'2018-05-08 00:00:00.000' AS DateTime), N'GEG-2WEEKLY', N'001642', CAST(N'2018-04-12 00:38:47.547' AS DateTime), NULL, NULL, N'ENG-GEG-008')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (100, N'2W', CAST(N'2018-05-02 00:00:00.000' AS DateTime), N'GEG-2WEEKLY', N'001638', CAST(N'2018-04-19 10:25:28.370' AS DateTime), NULL, NULL, N'ENG-GEG-002')
INSERT [dbo].[Interval] ([IntervalID], [Interval], [MaintenanceDate], [IKCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [AssetCategoryID]) VALUES (102, N'1M', CAST(N'2018-05-01 00:00:00.000' AS DateTime), N'GEG-MONTHLY', N'001638', CAST(N'2018-04-19 10:26:06.977' AS DateTime), NULL, NULL, N'ENG-GEG-001')
SET IDENTITY_INSERT [dbo].[Interval] OFF
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup], [isGenerated]) VALUES (N'ENG-GEG-001', N'ELECTRICAL', N'ENG', N'GEG', N'Gas Engine 1', N'SPC-L03-R.GEN', NULL, NULL, NULL, NULL, 1, CAST(N'2009-03-01 00:00:00.000' AS DateTime), 25, N'ENG-ELE-001', NULL)
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup], [isGenerated]) VALUES (N'ENG-GEG-002', N'ELECTRICAL', N'ENG', N'GEG', N'Gas Engine 2', N'SPC-L03-R.GEN', NULL, NULL, NULL, NULL, 1, CAST(N'2009-03-01 00:00:00.000' AS DateTime), 25, N'ENG-ELE-001', NULL)
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup], [isGenerated]) VALUES (N'ENG-GEG-003', N'ELECTRICAL', N'ENG', N'GEG', N'Gas Engine 3', N'SPC-L03-R.GEN', NULL, NULL, NULL, NULL, 0, CAST(N'2009-03-01 00:00:00.000' AS DateTime), 25, N'ENG-ELE-001', NULL)
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup], [isGenerated]) VALUES (N'ENG-GEG-004', N'ELECTRICAL', N'ENG', N'GEG', N'Gas Engine 4', N'SPC-L03-R.GEN', NULL, NULL, NULL, NULL, 0, CAST(N'2009-03-01 00:00:00.000' AS DateTime), 25, N'ENG-ELE-001', NULL)
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup], [isGenerated]) VALUES (N'ENG-GEG-005', N'ELECTRICAL', N'ENG', N'GEG', N'Gas Engine 5', N'SPC-L03-R.GEN', NULL, NULL, NULL, NULL, 0, CAST(N'2009-03-01 00:00:00.000' AS DateTime), 25, N'ENG-ELE-001', NULL)
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup], [isGenerated]) VALUES (N'ENG-GEG-006', N'ELECTRICAL', N'ENG', N'GEG', N'Gas Engine 6', N'SPC-L03-R.GEN', NULL, NULL, NULL, NULL, 0, CAST(N'2009-03-01 00:00:00.000' AS DateTime), 25, N'ENG-ELE-001', NULL)
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup], [isGenerated]) VALUES (N'ENG-GEG-007', N'ELECTRICAL', N'ENG', N'GEG', N'Gas Engine 7', N'SPC-L03-R.GEN', NULL, NULL, NULL, NULL, 0, CAST(N'2009-03-01 00:00:00.000' AS DateTime), 25, N'ENG-ELE-001', NULL)
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup], [isGenerated]) VALUES (N'ENG-GEG-008', N'ELECTRICAL', N'ENG', N'GEG', N'Gas Engine 8', N'SPC-L03-R.GEN', NULL, NULL, NULL, NULL, 0, CAST(N'2009-03-01 00:00:00.000' AS DateTime), 25, N'ENG-ELE-001', NULL)
INSERT [dbo].[MasterAssetCategory] ([AssetCategoryID], [AssetGroup], [Departement], [AssetCategory], [Description], [Location], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [IsActive], [InstalmentDate], [Lifetime], [UserGroup], [isGenerated]) VALUES (N'ENG-GEG-009', N'ELECTRICAL', N'ENG', N'GEG', N'Gas Engine 9', N'SPC-L03-R.GEN', NULL, NULL, NULL, NULL, 0, CAST(N'2009-03-01 00:00:00.000' AS DateTime), 25, N'ENG-ELE-001', NULL)
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT1-001', N'Battery 200AH GE 1
', N'ENG-GEG-001', NULL, 1, CAST(N'2018-04-11 23:46:36.340' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT1-002', N'Battery 200AH GE 2
', N'ENG-GEG-002', NULL, 1, CAST(N'2018-04-12 00:11:50.873' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT1-003', N'Battery 200AH GE 3
', N'ENG-GEG-003', NULL, 1, CAST(N'2018-04-12 00:15:18.043' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT1-004', N'Battery 200AH GE 4
', N'ENG-GEG-004', NULL, 1, CAST(N'2018-04-12 00:18:30.267' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT1-005', N'Battery 200AH GE 5
', N'ENG-GEG-005', NULL, 1, CAST(N'2018-04-12 00:24:51.033' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT1-006', N'Battery 200AH GE 6
', N'ENG-GEG-006', NULL, 1, CAST(N'2018-04-12 00:27:23.520' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT1-007', N'Battery 200AH GE 7
', N'ENG-GEG-007', NULL, 1, CAST(N'2018-04-12 00:30:14.040' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT1-008', N'Battery 200AH GE 8
', N'ENG-GEG-008', NULL, 1, CAST(N'2018-04-12 00:32:44.590' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT1-009', N'Battery 200AH GE 9
', N'ENG-GEG-009', NULL, 1, CAST(N'2018-04-12 00:35:05.100' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT2-001', N'Battery 18AH GE 1
', N'ENG-GEG-001', NULL, 1, CAST(N'2018-04-11 23:46:54.687' AS DateTime), NULL, N'001642', NULL, N'1', 1, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT2-002', N'Battery 18AH GE 2
', N'ENG-GEG-002', NULL, 1, CAST(N'2018-04-12 00:12:04.517' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT2-003', N'Battery 18AH GE 3
', N'ENG-GEG-003', NULL, 1, CAST(N'2018-04-12 00:15:34.777' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT2-004', N'Battery 18AH GE 4
', N'ENG-GEG-004', NULL, 1, CAST(N'2018-04-12 00:18:47.570' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT2-005', N'Battery 18AH GE 5
', N'ENG-GEG-005', NULL, 1, CAST(N'2018-04-12 00:25:07.387' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT2-006', N'Battery 18AH GE 6
', N'ENG-GEG-006', NULL, 1, CAST(N'2018-04-12 00:27:35.640' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT2-007', N'Battery 18AH GE 7
', N'ENG-GEG-007', NULL, 1, CAST(N'2018-04-12 00:30:27.103' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT2-008', N'Battery 18AH GE 8
', N'ENG-GEG-008', NULL, 1, CAST(N'2018-04-12 00:32:58.390' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'BT2-009', N'Battery 18AH GE 9
', N'ENG-GEG-009', NULL, 1, CAST(N'2018-04-12 00:35:20.190' AS DateTime), NULL, N'001642', NULL, N'1', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'EXH-001', N'Motor Exhaust Fan GE 1
', N'ENG-GEG-001', NULL, 1, CAST(N'2018-04-11 23:45:39.880' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'EXH-002', N'Motor Exhaust Fan GE 2
', N'ENG-GEG-002', NULL, 1, CAST(N'2018-04-12 00:11:03.307' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'EXH-003', N'Motor Exhaust Fan GE 3
', N'ENG-GEG-003', NULL, 1, CAST(N'2018-04-12 00:14:24.427' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'EXH-004', N'Motor Exhaust Fan GE 4
', N'ENG-GEG-004', NULL, 1, CAST(N'2018-04-12 00:17:47.717' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'EXH-005', N'Motor Exhaust Fan GE 5
', N'ENG-GEG-005', NULL, 1, CAST(N'2018-04-12 00:24:07.013' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'EXH-006', N'Motor Exhaust Fan GE 6
', N'ENG-GEG-006', NULL, 1, CAST(N'2018-04-12 00:26:43.287' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'EXH-007', N'Motor Exhaust Fan GE 7
', N'ENG-GEG-007', NULL, 1, CAST(N'2018-04-12 00:29:33.027' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'EXH-008', N'Motor Exhaust Fan GE 8
', N'ENG-GEG-008', NULL, 1, CAST(N'2018-04-12 00:31:55.827' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'EXH-009', N'Motor Exhaust Fan GE 9
', N'ENG-GEG-009', NULL, 1, CAST(N'2018-04-12 00:34:26.097' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'GCP-001', N'Panel GCP GE 1
', N'ENG-GEG-001', NULL, 1, CAST(N'2018-04-11 23:44:48.287' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'GCP-002', N'Panel GCP GE 2
', N'ENG-GEG-002', NULL, 1, CAST(N'2018-04-11 23:51:57.157' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'GCP-003', N'Panel GCP GE 3
', N'ENG-GEG-003', NULL, 1, CAST(N'2018-04-12 00:13:54.740' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'GCP-004', N'Panel GCP GE 4
', N'ENG-GEG-004', NULL, 1, CAST(N'2018-04-12 00:17:16.643' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'GCP-005', N'Panel GCP GE 5
', N'ENG-GEG-005', NULL, 1, CAST(N'2018-04-12 00:23:38.867' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'GCP-006', N'Panel GCP GE 6
', N'ENG-GEG-006', NULL, 1, CAST(N'2018-04-12 00:25:58.747' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'GCP-007', N'Panel GCP GE 7
', N'ENG-GEG-007', NULL, 1, CAST(N'2018-04-12 00:28:56.367' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'GCP-008', N'Panel GCP GE 8
', N'ENG-GEG-008', NULL, 1, CAST(N'2018-04-12 00:31:23.373' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'GCP-009', N'Panel GCP GE 9
', N'ENG-GEG-009', NULL, 1, CAST(N'2018-04-12 00:33:58.963' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IGC-001', N'Ignition Coil GE 1 ', N'ENG-GEG-001', NULL, 1, CAST(N'2018-04-11 23:33:37.427' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IGC-002', N'Ignition Coil GE 2
', N'ENG-GEG-002', NULL, 1, CAST(N'2018-04-11 23:50:52.070' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IGC-003', N'Ignition Coil GE 3
', N'ENG-GEG-003', NULL, 1, CAST(N'2018-04-12 00:13:28.227' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IGC-004', N'Ignition Coil GE 4
', N'ENG-GEG-004', NULL, 1, CAST(N'2018-04-12 00:16:44.830' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IGC-005', N'Ignition Coil GE 5
', N'ENG-GEG-005', NULL, 1, CAST(N'2018-04-12 00:22:50.660' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IGC-006', N'Ignition Coil GE 6
', N'ENG-GEG-006', NULL, 1, CAST(N'2018-04-12 00:25:27.900' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IGC-007', N'Ignition Coil GE 7
', N'ENG-GEG-007', NULL, 1, CAST(N'2018-04-12 00:28:28.177' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IGC-008', N'Ignition Coil GE 8
', N'ENG-GEG-008', NULL, 1, CAST(N'2018-04-12 00:30:47.043' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IGC-009', N'Ignition Coil GE 9
', N'ENG-GEG-009', NULL, 1, CAST(N'2018-04-12 00:33:32.260' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT1-001', N'Motor Intake Fan 1 GE 1
', N'ENG-GEG-001', NULL, 1, CAST(N'2018-04-11 23:46:00.963' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT1-002', N'Motor Intake Fan 1 GE 2
', N'ENG-GEG-002', NULL, 1, CAST(N'2018-04-12 00:11:19.750' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT1-003', N'Motor Intake Fan 1 GE 3
', N'ENG-GEG-003', NULL, 1, CAST(N'2018-04-12 00:14:43.950' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT1-004', N'Motor Intake Fan 1 GE 4
', N'ENG-GEG-004', NULL, 1, CAST(N'2018-04-12 00:18:03.683' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT1-005', N'Motor Intake Fan 1 GE 5
', N'ENG-GEG-005', NULL, 1, CAST(N'2018-04-12 00:24:21.253' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT1-006', N'Motor Intake Fan 1 GE 6
', N'ENG-GEG-006', NULL, 1, CAST(N'2018-04-12 00:26:56.083' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT1-007', N'Motor Intake Fan 1 GE 7
', N'ENG-GEG-007', NULL, 1, CAST(N'2018-04-12 00:29:48.083' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT1-008', N'Motor Intake Fan 1 GE 8
', N'ENG-GEG-008', NULL, 1, CAST(N'2018-04-12 00:32:10.793' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT1-009', N'Motor Intake Fan 1 GE 9
', N'ENG-GEG-009', NULL, 1, CAST(N'2018-04-12 00:34:40.973' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT2-001', N'Motor Intake Fan 2 GE 1', N'ENG-GEG-001', NULL, 1, CAST(N'2018-04-11 23:46:18.143' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT2-002', N'Motor Intake Fan 2 GE 2
', N'ENG-GEG-002', NULL, 1, CAST(N'2018-04-12 00:11:35.507' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT2-003', N'Motor Intake Fan 2 GE 3
', N'ENG-GEG-003', NULL, 1, CAST(N'2018-04-12 00:15:03.697' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT2-004', N'Motor Intake Fan 2 GE 4
', N'ENG-GEG-004', NULL, 1, CAST(N'2018-04-12 00:18:17.573' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT2-005', N'Motor Intake Fan 2 GE 5
', N'ENG-GEG-005', NULL, 1, CAST(N'2018-04-12 00:24:37.370' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT2-006', N'Motor Intake Fan 2 GE 6
', N'ENG-GEG-006', NULL, 1, CAST(N'2018-04-12 00:27:11.570' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT2-007', N'Motor Intake Fan 2 GE 7
', N'ENG-GEG-007', NULL, 1, CAST(N'2018-04-12 00:30:00.627' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT2-008', N'Motor Intake Fan 2 GE 8
', N'ENG-GEG-008', NULL, 1, CAST(N'2018-04-12 00:32:31.470' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'IT2-009', N'Motor Intake Fan 2 GE 9
', N'ENG-GEG-009', NULL, 1, CAST(N'2018-04-12 00:34:53.087' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'PIF-001', N'Panel Interface GE 1
', N'ENG-GEG-001', NULL, 1, CAST(N'2018-04-11 23:44:29.700' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'PIF-002', N'Panel Interface GE 2
', N'ENG-GEG-002', NULL, 1, CAST(N'2018-04-11 23:51:39.950' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'PIF-003', N'Panel Interface GE 3
', N'ENG-GEG-003', NULL, 1, CAST(N'2018-04-12 00:13:41.697' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'PIF-004', N'Panel Interface GE 4
', N'ENG-GEG-004', NULL, 1, CAST(N'2018-04-12 00:17:00.970' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'PIF-005', N'Panel Interface GE 5
', N'ENG-GEG-005', NULL, 1, CAST(N'2018-04-12 00:23:23.020' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'PIF-006', N'Panel Interface GE 6
', N'ENG-GEG-006', NULL, 1, CAST(N'2018-04-12 00:25:42.630' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'PIF-007', N'Panel Interface GE 7
', N'ENG-GEG-007', NULL, 1, CAST(N'2018-04-12 00:28:41.537' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'PIF-008', N'Panel Interface GE 8
', N'ENG-GEG-008', NULL, 1, CAST(N'2018-04-12 00:31:00.827' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'PIF-009', N'Panel Interface GE 9
', N'ENG-GEG-009', NULL, 1, CAST(N'2018-04-12 00:33:45.363' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'RAD-001', N'Radiator GE 1
', N'ENG-GEG-001', NULL, 1, CAST(N'2018-04-11 23:45:19.140' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'RAD-002', N'Radiator GE 2
', N'ENG-GEG-002', NULL, 1, CAST(N'2018-04-12 00:10:44.007' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'RAD-003', N'Radiator GE 3
', N'ENG-GEG-003', NULL, 1, CAST(N'2018-04-12 00:14:07.723' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'RAD-004', N'Radiator GE 4
', N'ENG-GEG-004', NULL, 1, CAST(N'2018-04-12 00:17:31.783' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'RAD-005', N'Radiator GE 5
', N'ENG-GEG-005', NULL, 1, CAST(N'2018-04-12 00:23:53.323' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'RAD-006', N'Radiator GE 6
', N'ENG-GEG-006', NULL, 1, CAST(N'2018-04-12 00:26:30.903' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'RAD-007', N'Radiator GE 7
', N'ENG-GEG-007', NULL, 1, CAST(N'2018-04-12 00:29:19.743' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'RAD-008', N'Radiator GE 8
', N'ENG-GEG-008', NULL, 1, CAST(N'2018-04-12 00:31:37.837' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterAssetType] ([MasterAssetTypeID], [Description], [AssetCategoryID], [ReplacementDate], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Lifetime], [Replacement], [InstallmentDate]) VALUES (N'RAD-009', N'Radiator GE 9
', N'ENG-GEG-009', NULL, 1, CAST(N'2018-04-12 00:34:13.033' AS DateTime), NULL, N'001642', NULL, N'25', NULL, CAST(N'2009-03-01 00:00:00.000' AS DateTime))
INSERT [dbo].[MasterIK] ([IKCode], [IKDescription], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Departement]) VALUES (N'GEG-1YEARLY', N'Gas Engine maintenance yearly
', 1, CAST(N'2018-04-11 22:29:12.333' AS DateTime), NULL, N'001642', NULL, N'ENG')
INSERT [dbo].[MasterIK] ([IKCode], [IKDescription], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Departement]) VALUES (N'GEG-2MONTHLY', N'Gas Engine maintenance bimonthly', 1, CAST(N'2018-04-11 22:28:54.607' AS DateTime), NULL, N'001642', NULL, N'ENG')
INSERT [dbo].[MasterIK] ([IKCode], [IKDescription], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Departement]) VALUES (N'GEG-2WEEKLY', N'Gas Engine maintenance biweekly', 1, CAST(N'2018-04-11 22:28:15.183' AS DateTime), NULL, N'001642', NULL, N'ENG')
INSERT [dbo].[MasterIK] ([IKCode], [IKDescription], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Departement]) VALUES (N'GEG-MONTHLY', N'Gas Engine maintenance monthly', 1, CAST(N'2018-04-11 22:28:37.063' AS DateTime), NULL, N'001642', NULL, N'ENG')
INSERT [dbo].[MasterIK] ([IKCode], [IKDescription], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Departement]) VALUES (N'GEG-PM00500', N'Gas Engine maintenance running hours PM 500
', 1, CAST(N'2018-04-11 22:24:23.327' AS DateTime), NULL, N'001642', NULL, N'ENG')
INSERT [dbo].[MasterIK] ([IKCode], [IKDescription], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Departement]) VALUES (N'GEG-PM01000', N'Gas Engine maintenance running hours PM 1000
', 1, CAST(N'2018-04-11 22:26:03.883' AS DateTime), NULL, N'001642', NULL, N'ENG')
INSERT [dbo].[MasterIK] ([IKCode], [IKDescription], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Departement]) VALUES (N'GEG-PM02000', N'Gas Engine maintenance running hours PM 2000', 1, CAST(N'2018-04-11 22:26:24.980' AS DateTime), NULL, N'001642', NULL, N'ENG')
INSERT [dbo].[MasterIK] ([IKCode], [IKDescription], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Departement]) VALUES (N'GEG-PM03000', N'Gas Engine maintenance running hours PM 3000
', 1, CAST(N'2018-04-11 22:26:42.737' AS DateTime), NULL, N'001642', NULL, N'ENG')
INSERT [dbo].[MasterIK] ([IKCode], [IKDescription], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Departement]) VALUES (N'GEG-PM06000', N'Gas Engine maintenance running hours PM 6000', 1, CAST(N'2018-04-11 22:26:59.137' AS DateTime), NULL, N'001642', NULL, N'ENG')
INSERT [dbo].[MasterIK] ([IKCode], [IKDescription], [IsActive], [CreatedDate], [UpdatedDate], [CreatedBy], [UpdatedBy], [Departement]) VALUES (N'GEG-PM10000', N'Gas Engine maintenance running hours PM 10000
', 1, CAST(N'2018-04-11 22:27:15.647' AS DateTime), NULL, N'001642', NULL, N'ENG')
SET IDENTITY_INSERT [dbo].[MasterInterval] ON 

INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (11, N'Periodic', N'1M', 1, N'MONTHLY', 1, CAST(N'2018-04-11 22:00:40.407' AS DateTime), N'001642', CAST(N'2018-04-11 22:02:16.217' AS DateTime), N'001642', 1, N'Month')
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (12, N'Periodic', N'2M', 2, N'BI-MONTHLY', 1, CAST(N'2018-04-11 22:00:57.980' AS DateTime), N'001642', CAST(N'2018-04-11 22:02:35.507' AS DateTime), N'001642', 1, N'Month')
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (13, N'Periodic', N'3M', 3, N'3 MONTHLY', 1, CAST(N'2018-04-11 22:02:03.837' AS DateTime), N'001642', CAST(N'2018-04-12 16:38:37.427' AS DateTime), N'001638', 1, N'Month')
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (14, N'Periodic', N'4M', 4, N'4 MONTHLY', 1, CAST(N'2018-04-11 22:03:04.773' AS DateTime), N'001642', CAST(N'2018-04-12 16:38:26.197' AS DateTime), N'001638', 1, N'Month')
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (15, N'Periodic', N'6M', 6, N'SEMESTERLY', 1, CAST(N'2018-04-11 22:16:17.347' AS DateTime), N'001642', NULL, NULL, 1, N'Month')
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (16, N'Periodic', N'1Y', 1, N'YEARLY', 1, CAST(N'2018-04-11 22:16:51.567' AS DateTime), N'001642', NULL, NULL, 1, N'Year')
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (17, N'Periodic', N'1W', 7, N'WEEKLY', 1, CAST(N'2018-04-11 22:17:20.447' AS DateTime), N'001642', NULL, NULL, 1, N'Week')
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (18, N'Periodic', N'2W', 14, N'2 - WEEKLY', 1, CAST(N'2018-04-11 22:17:44.230' AS DateTime), N'001642', CAST(N'2018-04-12 16:38:12.480' AS DateTime), N'001638', 1, N'Week')
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (19, N'Hour', N'500', 0, N'500 HOURS', 1, CAST(N'2018-04-11 22:18:46.553' AS DateTime), N'001642', NULL, NULL, 1, NULL)
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (20, N'Hour', N'1000', 0, N'1000 HOURS', 1, CAST(N'2018-04-11 22:19:09.733' AS DateTime), N'001642', NULL, NULL, 1, NULL)
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (21, N'Hour', N'2000', 0, N'2000 HOURS', 1, CAST(N'2018-04-11 22:19:27.360' AS DateTime), N'001642', NULL, NULL, 1, NULL)
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (22, N'Hour', N'3000', 0, N'3000 HOURS', 1, CAST(N'2018-04-11 22:19:48.773' AS DateTime), N'001642', NULL, NULL, 1, NULL)
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (23, N'Hour', N'6000', 0, N'6000 HOURS', 1, CAST(N'2018-04-11 22:20:16.733' AS DateTime), N'001642', NULL, NULL, 1, NULL)
INSERT [dbo].[MasterInterval] ([IntervalID], [IntervalGroup], [IntervalCode], [IntervalValue], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdadatedBy], [IsEnabled], [IntervalType]) VALUES (24, N'Hour', N'10000', 0, N'10000HOURS', 1, CAST(N'2018-04-11 22:20:33.640' AS DateTime), N'001642', NULL, NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[MasterInterval] OFF
INSERT [dbo].[MasterLocation] ([LocationCode], [Subportofolio], [Floor], [LocationArea], [Description], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (N'SPC-L03-R.GEN', N'SPC', N'L03', N'R.GEN', N'Ruang Genset Lantai 3', 1, CAST(N'2018-04-11 22:21:26.037' AS DateTime), N'001642', NULL, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Role]) VALUES (N'000406', N'vPelNrzLabeGOYF6LMYvZA==', N'Istaryadi Sapta. S', CAST(N'2018-04-02 09:54:57.517' AS DateTime), N'ekky', NULL, NULL, 0, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Role]) VALUES (N'000418', N'vPelNrzLabeGOYF6LMYvZA==', N'Andri Leksmana', CAST(N'2018-04-02 09:55:25.033' AS DateTime), N'ekky', NULL, NULL, 0, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Role]) VALUES (N'000533', N'vPelNrzLabeGOYF6LMYvZA==', N'Mike Dwi Andriani', CAST(N'2018-04-02 09:55:59.627' AS DateTime), N'ekky', NULL, NULL, 0, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Role]) VALUES (N'001513', N'vPelNrzLabeGOYF6LMYvZA==', N'Edi Wahyono', CAST(N'2018-04-02 09:59:13.530' AS DateTime), N'ekky', NULL, NULL, 0, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Role]) VALUES (N'001532', N'vPelNrzLabeGOYF6LMYvZA==', N'Ratih Yuniarti', CAST(N'2018-04-02 09:45:45.760' AS DateTime), N'ekky', NULL, NULL, 0, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Role]) VALUES (N'001609', N'vPelNrzLabeGOYF6LMYvZA==', N'Bangun Sugiharto', CAST(N'2018-04-12 17:47:53.880' AS DateTime), N'001623', NULL, NULL, 0, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Role]) VALUES (N'001623', N'vPelNrzLabeGOYF6LMYvZA==', N'Ananta', CAST(N'2018-03-28 17:02:47.147' AS DateTime), N'ekky', NULL, NULL, 0, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Role]) VALUES (N'001638', N'vPelNrzLabeGOYF6LMYvZA==', N'Cahyo Agung N', CAST(N'2018-04-02 10:02:07.787' AS DateTime), N'ekky', NULL, NULL, 0, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Role]) VALUES (N'001642', N'vPelNrzLabeGOYF6LMYvZA==', N'Lovebbi', CAST(N'2018-03-20 10:42:44.367' AS DateTime), N'ekky', NULL, NULL, 0, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Role]) VALUES (N'001770', N'vPelNrzLabeGOYF6LMYvZA==', N'Vincentius Sunaryo', CAST(N'2018-04-02 09:46:04.027' AS DateTime), N'ekky', NULL, NULL, 0, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Role]) VALUES (N'ekky', N'vPelNrzLabeGOYF6LMYvZA==', N'daniel ekky', NULL, NULL, CAST(N'2018-04-10 15:44:50.347' AS DateTime), N'ekky2', 0, NULL)
INSERT [dbo].[MasterUser] ([UserID], [Password], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Role]) VALUES (N'ekky2', N'vPelNrzLabeGOYF6LMYvZA==', N'ekky', CAST(N'2018-03-18 01:06:19.353' AS DateTime), N'ekky', NULL, NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[UserGroupDetail] ON 

INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (26, N'IT-20', N'001642', CAST(N'2018-04-02 10:13:39.663' AS DateTime), N'ekky', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (27, N'IT-20', N'001532', CAST(N'2018-04-02 10:13:39.663' AS DateTime), N'ekky', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (28, N'IT-20', N'001770', CAST(N'2018-04-02 10:13:39.663' AS DateTime), N'ekky', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (29, N'IT-20', N'ekky2', CAST(N'2018-04-02 10:13:39.680' AS DateTime), N'ekky', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (51, N'ENG-ELE-001', N'001623', CAST(N'2018-04-12 17:48:04.477' AS DateTime), N'001623', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (52, N'ENG-ELE-001', N'000418', CAST(N'2018-04-12 17:48:04.477' AS DateTime), N'001623', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (53, N'ENG-ELE-001', N'001609', CAST(N'2018-04-12 17:48:04.477' AS DateTime), N'001623', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (54, N'ENG-ELE-001', N'001638', CAST(N'2018-04-12 17:48:04.477' AS DateTime), N'001623', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (55, N'ENG-ELE-001', N'001513', CAST(N'2018-04-12 17:48:04.493' AS DateTime), N'001623', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (56, N'ENG-ELE-001', N'000406', CAST(N'2018-04-12 17:48:04.493' AS DateTime), N'001623', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (57, N'ENG-ELE-001', N'000533', CAST(N'2018-04-12 17:48:04.493' AS DateTime), N'001623', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (58, N'ENG-ELE-001', N'ekky', CAST(N'2018-04-12 17:48:04.493' AS DateTime), N'001623', NULL, NULL, NULL)
INSERT [dbo].[UserGroupDetail] ([UserGroupDetailID], [UserGroupID], [UserID], [CreatedDate], [CreratedBy], [UpdatedBy], [UpdatedDate], [IsActive]) VALUES (59, N'ENG-ELE-001', N'ekky2', CAST(N'2018-04-12 17:48:04.493' AS DateTime), N'001623', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserGroupDetail] OFF
INSERT [dbo].[UserGroupHeader] ([UserGroupId], [UserGroupName], [UserGroupLeader], [CreatedDate], [CreatedBy], [UpdateDate], [UpdateBy], [IsActive]) VALUES (N'ENG-ELE-001', N'ELECTRICAL GE', N'001638', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[UserGroupHeader] ([UserGroupId], [UserGroupName], [UserGroupLeader], [CreatedDate], [CreatedBy], [UpdateDate], [UpdateBy], [IsActive]) VALUES (N'IT-20', N'IT', N'001642', NULL, NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[WorkFlow] ON 

INSERT [dbo].[WorkFlow] ([WorkFlowID], [WorkFlowWorkOrderID], [WorkFlowApproval], [WorkFlowStatus], [WorkFlowRemark], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ApprovedTime], [Remark]) VALUES (64, N'ENG-GEG-2018-04-0001', N'001638', N'R', NULL, CAST(N'2018-04-12 00:43:51.310' AS DateTime), N'001623', CAST(N'2018-04-12 00:45:45.120' AS DateTime), N'001638', CAST(N'2018-04-12 00:45:45.120' AS DateTime), N'test ')
INSERT [dbo].[WorkFlow] ([WorkFlowID], [WorkFlowWorkOrderID], [WorkFlowApproval], [WorkFlowStatus], [WorkFlowRemark], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ApprovedTime], [Remark]) VALUES (65, N'ENG-GEG-2018-04-0001', N'001638', N'R', NULL, CAST(N'2018-04-12 10:48:36.310' AS DateTime), N'001638', CAST(N'2018-04-12 16:11:18.047' AS DateTime), N'001638', CAST(N'2018-04-12 16:11:18.047' AS DateTime), N'Coba lihat untuk Panel TRX')
INSERT [dbo].[WorkFlow] ([WorkFlowID], [WorkFlowWorkOrderID], [WorkFlowApproval], [WorkFlowStatus], [WorkFlowRemark], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ApprovedTime], [Remark]) VALUES (66, N'ENG-GEG-2018-04-0002', N'001638', N'A', NULL, CAST(N'2018-04-12 16:08:57.430' AS DateTime), N'001623', CAST(N'2018-04-12 16:10:48.853' AS DateTime), N'001638', CAST(N'2018-04-12 16:10:48.853' AS DateTime), N'Sudah OK ')
INSERT [dbo].[WorkFlow] ([WorkFlowID], [WorkFlowWorkOrderID], [WorkFlowApproval], [WorkFlowStatus], [WorkFlowRemark], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ApprovedTime], [Remark]) VALUES (67, N'ENG-GEG-2018-04-0001', N'001638', N'S', NULL, CAST(N'2018-04-13 19:43:18.430' AS DateTime), N'001609', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[WorkFlow] OFF
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0001', CAST(N'2018-05-01 00:00:00.000' AS DateTime), N'ENG-GEG-001', N'GEG-MONTHLY', NULL, NULL, N'001638', NULL, N'001609', NULL, N'Submit', NULL, CAST(N'2018-04-12 00:39:27.967' AS DateTime), CAST(N'2018-04-13 19:43:18.383' AS DateTime), N'001609', NULL, N'Gas Engine 1', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance monthly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0002', CAST(N'2018-06-01 00:00:00.000' AS DateTime), N'ENG-GEG-001', N'GEG-MONTHLY', NULL, NULL, N'001638', NULL, N'001623', NULL, N'Approved', NULL, CAST(N'2018-04-12 00:39:27.983' AS DateTime), CAST(N'2018-04-12 16:08:57.400' AS DateTime), N'001623', NULL, N'Gas Engine 1', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance monthly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0003', CAST(N'2018-07-01 00:00:00.000' AS DateTime), N'ENG-GEG-001', N'GEG-MONTHLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-12 00:39:28.000' AS DateTime), NULL, NULL, NULL, N'Gas Engine 1', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance monthly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0004', CAST(N'2018-08-01 00:00:00.000' AS DateTime), N'ENG-GEG-001', N'GEG-MONTHLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-12 00:39:28.000' AS DateTime), NULL, NULL, NULL, N'Gas Engine 1', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance monthly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0005', CAST(N'2018-09-01 00:00:00.000' AS DateTime), N'ENG-GEG-001', N'GEG-MONTHLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-12 00:39:28.000' AS DateTime), NULL, NULL, NULL, N'Gas Engine 1', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance monthly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0006', CAST(N'2018-10-01 00:00:00.000' AS DateTime), N'ENG-GEG-001', N'GEG-MONTHLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-12 00:39:28.013' AS DateTime), NULL, NULL, NULL, N'Gas Engine 1', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance monthly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0007', CAST(N'2018-11-01 00:00:00.000' AS DateTime), N'ENG-GEG-001', N'GEG-MONTHLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-12 00:39:28.013' AS DateTime), NULL, NULL, NULL, N'Gas Engine 1', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance monthly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0008', CAST(N'2018-12-01 00:00:00.000' AS DateTime), N'ENG-GEG-001', N'GEG-MONTHLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-12 00:39:28.030' AS DateTime), NULL, NULL, NULL, N'Gas Engine 1', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance monthly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0009', CAST(N'2018-05-02 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.183' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0010', CAST(N'2018-05-16 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.230' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0011', CAST(N'2018-05-30 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.230' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0012', CAST(N'2018-06-13 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.230' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0013', CAST(N'2018-06-27 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.247' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0014', CAST(N'2018-07-11 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.247' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0015', CAST(N'2018-07-25 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.260' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0016', CAST(N'2018-08-08 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.260' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0017', CAST(N'2018-08-22 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.277' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0018', CAST(N'2018-09-05 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.277' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0019', CAST(N'2018-09-19 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.277' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0020', CAST(N'2018-10-03 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.293' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0021', CAST(N'2018-10-17 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.293' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0022', CAST(N'2018-10-31 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.307' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0023', CAST(N'2018-11-14 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.307' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0024', CAST(N'2018-11-28 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.307' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0025', CAST(N'2018-12-12 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.323' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
INSERT [dbo].[WorkOrderManual] ([WorkOderManualID], [WorkOrderDate], [CategoryAssetID], [IKCode], [VendorName], [WorkStartDate], [StartedBy], [WorkEndDate], [CompleteBy], [PICRemarks], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [Subportopolio], [CategoryDescription], [WorkOrderLeader], [AssetLocationCode], [IKDescription], [IKCategory], [isactive]) VALUES (N'ENG-GEG-2018-04-0026', CAST(N'2018-12-26 00:00:00.000' AS DateTime), N'ENG-GEG-002', N'GEG-2WEEKLY', NULL, NULL, N'001638', NULL, NULL, NULL, N'Open', NULL, CAST(N'2018-04-19 10:25:40.323' AS DateTime), NULL, NULL, NULL, N'Gas Engine 2', N'001638', N'SPC-L03-R.GEN', N'Gas Engine maintenance biweekly', NULL, 1)
SET IDENTITY_INSERT [dbo].[WorkOrderManualMaintenance] ON 

INSERT [dbo].[WorkOrderManualMaintenance] ([MaintenanceID], [AssetTypeID], [WorkOrderManualID], [Jumlah], [Satuan], [Remarks], [maintenanceType]) VALUES (76, N'BT2-001', N'ENG-GEG-2018-04-0002', N'1', N'PCS', N'', N'Replace')
INSERT [dbo].[WorkOrderManualMaintenance] ([MaintenanceID], [AssetTypeID], [WorkOrderManualID], [Jumlah], [Satuan], [Remarks], [maintenanceType]) VALUES (77, N'BT1-001', N'ENG-GEG-2018-04-0002', N'1', N'GRAM', N'', N'Service')
INSERT [dbo].[WorkOrderManualMaintenance] ([MaintenanceID], [AssetTypeID], [WorkOrderManualID], [Jumlah], [Satuan], [Remarks], [maintenanceType]) VALUES (78, N'IGC-001', N'ENG-GEG-2018-04-0001', N'1', N'PCS', N'1', N'Replace')
INSERT [dbo].[WorkOrderManualMaintenance] ([MaintenanceID], [AssetTypeID], [WorkOrderManualID], [Jumlah], [Satuan], [Remarks], [maintenanceType]) VALUES (79, N'BT2-001', N'ENG-GEG-2018-04-0003', N'', N'PCS', N'', N'--Select One--')
SET IDENTITY_INSERT [dbo].[WorkOrderManualMaintenance] OFF
ALTER TABLE [dbo].[AssetIntervalInformation]  WITH CHECK ADD  CONSTRAINT [FK_AssetIntervalInformation_MasterIK] FOREIGN KEY([IKCode])
REFERENCES [dbo].[MasterIK] ([IKCode])
GO
ALTER TABLE [dbo].[AssetIntervalInformation] CHECK CONSTRAINT [FK_AssetIntervalInformation_MasterIK]
GO
ALTER TABLE [dbo].[IKCategory]  WITH CHECK ADD  CONSTRAINT [FK_IKCategory_MasterIK] FOREIGN KEY([MasterIK])
REFERENCES [dbo].[MasterIK] ([IKCode])
GO
ALTER TABLE [dbo].[IKCategory] CHECK CONSTRAINT [FK_IKCategory_MasterIK]
GO
ALTER TABLE [dbo].[IKCondition]  WITH CHECK ADD  CONSTRAINT [FK_IKCondition_WorkOrderManual] FOREIGN KEY([WorkOrderManualID])
REFERENCES [dbo].[WorkOrderManual] ([WorkOderManualID])
GO
ALTER TABLE [dbo].[IKCondition] CHECK CONSTRAINT [FK_IKCondition_WorkOrderManual]
GO
ALTER TABLE [dbo].[IKConditionVendor]  WITH CHECK ADD  CONSTRAINT [FK_IKConditionVendor_WorkOrderManual] FOREIGN KEY([WorkOrderManual])
REFERENCES [dbo].[WorkOrderManual] ([WorkOderManualID])
GO
ALTER TABLE [dbo].[IKConditionVendor] CHECK CONSTRAINT [FK_IKConditionVendor_WorkOrderManual]
GO
ALTER TABLE [dbo].[IKItem]  WITH CHECK ADD  CONSTRAINT [FK_IKItem_IKCategory] FOREIGN KEY([IKCategory])
REFERENCES [dbo].[IKCategory] ([IKCategory])
GO
ALTER TABLE [dbo].[IKItem] CHECK CONSTRAINT [FK_IKItem_IKCategory]
GO
ALTER TABLE [dbo].[Interval]  WITH CHECK ADD  CONSTRAINT [FK_Interval_MasterAssetCategory] FOREIGN KEY([AssetCategoryID])
REFERENCES [dbo].[MasterAssetCategory] ([AssetCategoryID])
GO
ALTER TABLE [dbo].[Interval] CHECK CONSTRAINT [FK_Interval_MasterAssetCategory]
GO
ALTER TABLE [dbo].[Interval]  WITH CHECK ADD  CONSTRAINT [FK_Interval_MasterIK] FOREIGN KEY([IKCode])
REFERENCES [dbo].[MasterIK] ([IKCode])
GO
ALTER TABLE [dbo].[Interval] CHECK CONSTRAINT [FK_Interval_MasterIK]
GO
ALTER TABLE [dbo].[MasterAssetCategory]  WITH CHECK ADD  CONSTRAINT [FK_MasterAssetCategory_MasterLocation] FOREIGN KEY([Location])
REFERENCES [dbo].[MasterLocation] ([LocationCode])
GO
ALTER TABLE [dbo].[MasterAssetCategory] CHECK CONSTRAINT [FK_MasterAssetCategory_MasterLocation]
GO
ALTER TABLE [dbo].[MasterAssetCategory]  WITH CHECK ADD  CONSTRAINT [FK_MasterAssetCategory_UserGroupHeader] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroupHeader] ([UserGroupId])
GO
ALTER TABLE [dbo].[MasterAssetCategory] CHECK CONSTRAINT [FK_MasterAssetCategory_UserGroupHeader]
GO
ALTER TABLE [dbo].[MasterAssetType]  WITH CHECK ADD  CONSTRAINT [FK_MasterAssetType_MasterAssetType] FOREIGN KEY([AssetCategoryID])
REFERENCES [dbo].[MasterAssetCategory] ([AssetCategoryID])
GO
ALTER TABLE [dbo].[MasterAssetType] CHECK CONSTRAINT [FK_MasterAssetType_MasterAssetType]
GO
ALTER TABLE [dbo].[UserGroupDetail]  WITH CHECK ADD  CONSTRAINT [FK_UserGroupDetail_UserGroupDetail] FOREIGN KEY([UserGroupID])
REFERENCES [dbo].[UserGroupHeader] ([UserGroupId])
GO
ALTER TABLE [dbo].[UserGroupDetail] CHECK CONSTRAINT [FK_UserGroupDetail_UserGroupDetail]
GO
ALTER TABLE [dbo].[UserGroupDetail]  WITH CHECK ADD  CONSTRAINT [FK_UserGroupDetail_UserGroupHeader] FOREIGN KEY([UserID])
REFERENCES [dbo].[MasterUser] ([UserID])
GO
ALTER TABLE [dbo].[UserGroupDetail] CHECK CONSTRAINT [FK_UserGroupDetail_UserGroupHeader]
GO
ALTER TABLE [dbo].[WorkOrderManual]  WITH CHECK ADD  CONSTRAINT [FK_WorkOrderManual_MasterAssetCategory] FOREIGN KEY([CategoryAssetID])
REFERENCES [dbo].[MasterAssetCategory] ([AssetCategoryID])
GO
ALTER TABLE [dbo].[WorkOrderManual] CHECK CONSTRAINT [FK_WorkOrderManual_MasterAssetCategory]
GO
ALTER TABLE [dbo].[WorkOrderManual]  WITH CHECK ADD  CONSTRAINT [FK_WorkOrderManual_MasterIK] FOREIGN KEY([IKCode])
REFERENCES [dbo].[MasterIK] ([IKCode])
GO
ALTER TABLE [dbo].[WorkOrderManual] CHECK CONSTRAINT [FK_WorkOrderManual_MasterIK]
GO
ALTER TABLE [dbo].[WorkOrderManualMaintenance]  WITH CHECK ADD  CONSTRAINT [FK_WorkOrderManualMaintenance_WorkOrderManual] FOREIGN KEY([WorkOrderManualID])
REFERENCES [dbo].[WorkOrderManual] ([WorkOderManualID])
GO
ALTER TABLE [dbo].[WorkOrderManualMaintenance] CHECK CONSTRAINT [FK_WorkOrderManualMaintenance_WorkOrderManual]
GO
USE [master]
GO
ALTER DATABASE [PreventiveAsset] SET  READ_WRITE 
GO
