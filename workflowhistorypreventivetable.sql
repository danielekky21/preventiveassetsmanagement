/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Express Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [PreventiveAsset]
GO

/****** Object:  Table [dbo].[WorkFlowHistory]    Script Date: 3/26/2018 11:26:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WorkFlowHistory](
	[WorkFlowLogID] [int] IDENTITY(1,1) NOT NULL,
	[WorkFlowStatus] [varchar](50) NULL,
	[WorkFlowUpdatedDate] [datetime] NULL,
	[WorkFlowUpdatedBy] [varchar](50) NULL,
	[WorkOrderID] [nvarchar](300) NULL,
 CONSTRAINT [PK_WorkFlowHistory] PRIMARY KEY CLUSTERED 
(
	[WorkFlowLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


