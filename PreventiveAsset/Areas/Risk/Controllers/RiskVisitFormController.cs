﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using PreventiveAssetManagement.dataRepo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RazorEngine;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Areas.Risk.Controllers
{
    public partial class RiskVisitFormController : Controller
    {
        // GET: Risk/RiskVisitForm
        public virtual ActionResult Index(string id)
        {
            var data = RiskVisit.GetRiskVisitByWOID(id);
            if (data != null)
            {
                ViewBag.checklist = RiskVisit.GetRiskVisitChecklistByVisitID(data.VisitID).ToList();
                ViewData.Model = data;
            }
            else
            {
                RiskVisit newData = new RiskVisit();
                ViewBag.checklist = new List<RiskVisitChecklist>();
                newData.WorkOrderID = id;
                ViewData.Model = newData;
            }
            SelectList sel = new SelectList();
            ViewBag.WOR = WorkOrderManual.GetWorkOderByID(id);
            ViewBag.loc = sel.GetLocationAreaSelectList();
            ViewBag.dpt = sel.GetDepartementSelectlistItem();
            ViewBag.itemgroup = sel.GetItemGroupCode();
            return View();
        }
        public virtual JsonResult PickWorkOrder(string id)
        {
            try
            {
                string loginID = Session["UserId"].ToString();
                var res = WorkOrderManual.GetWorkOderByID(id);
                res.RiskStatus = "taken";
                res.UpdateSave<WorkOrderManual>();
                var newData = new RiskVisit();
                var count = RiskVisit.riskCount() + 1;
                newData.VisitID = res.MasterAssetCategory.Departement + "-HIRAC-"  + DateTime.Now.Year + DateTime.Now.Month + "-" + count.ToString().PadLeft(4, '0');
                newData.WorkOrderID = id;
                newData.ApprovalStatus = "OPEN";
                newData.CreatedDate = DateTime.Now;
                newData.CreatedBy = loginID;
                newData.InsertSave<RiskVisit>();
                return Json("success", JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json("fail", JsonRequestBehavior.AllowGet);
            }
            
           
        }
        public virtual JsonResult GetChecklistItemByGroup(string groupid)
        {
            var data = ChecklistHeader.MapByGroupID(groupid).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult GetMapDetailData(string id)
        {
            var selectlist = new SelectList();
            var deptList = selectlist.GetDepartementSelectList().ToList();
            var data = ChecklistHeader.MapItemDetailByHeader(id);
            var jsonData = new
            {
                data = data,
                dept = deptList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult GetPDF(string code)
        {
            PDFBuilder builder = new PDFBuilder();
            var data = RiskVisit.GetRiskVisitByID(code);
            builder.PdfBuilder(Server.MapPath("~/Areas/Risk/Views/PDFile/RiskChecklist.cshtml"), Server.MapPath("/Assets/Img/pir-logo.jpg"), data);
            return builder.GetPdf();
        }
        public class PDFBuilder
        {
            private string _file;
            private string _Url;
            private RiskVisit riskForm;
            public void PdfBuilder(string file, string imgUrl, RiskVisit Model)
            {
                _file = file;
                _Url = imgUrl;
                riskForm = Model;
            }
            public FileContentResult GetPdf()
            {
                var html = GetHtml();
                Byte[] bytes;
                using (var ms = new MemoryStream())
                {
                    using (var doc = new Document())
                    {
                        using (var writer = PdfWriter.GetInstance(doc, ms))
                        {
                            doc.Open();

                            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(_Url);
                            jpg.ScaleToFit(140f, 120f);
                            //jpg.SpacingBefore = 10f;
                            //jpg.SpacingAfter = 1f;
                            jpg.Alignment = Element.ALIGN_LEFT;
                            doc.Add(jpg);
                            try
                            {

                                using (var msHtml = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(html)))
                                {

                                    iTextSharp.tool.xml.XMLWorkerHelper.GetInstance()
                                    .ParseXHtml(writer, doc, msHtml, System.Text.Encoding.UTF8);
                                }
                            }
                            finally
                            {

                                doc.Close();
                            }
                        }
                    }
                    bytes = ms.ToArray();
                }
                return new FileContentResult(bytes, "application/pdf");
            }
            private string GetHtml()
            {
                var html = System.IO.File.ReadAllText(_file);
                return Razor.Parse(html, riskForm);
            }
        }

        [HttpPost]
        public virtual ActionResult index(RiskVisit model, Guid[] detailid, string[] condition, string[] kategori, string[] isupload, HttpPostedFileBase[] dokumentasi, string Departements, string[] Keterangan, string[] deadline, string submitButton,int[] ChecklistDetail,string[] area,string[] PICDept,string[] PICvis,string[] inspectors)
        {
            string loginID = Session["UserId"].ToString();
           
            var currentData = RiskVisit.GetRiskVisitByWOID(model.WorkOrderID);
            if (currentData != null)
            {
                currentData.VisitDate = model.VisitDate;
                currentData.VisitTimeFrom = model.VisitTimeFrom;
                currentData.VisitTimeTo = model.VisitTimeTo;
                if (PICvis != null)
                {
                    var pic = string.Empty;
                    foreach (var item in PICvis)
                    {
                        var last = PICvis.Last();
                        if (!item.Equals(last))
                        {
                            pic = pic + item + ",";
                        }
                        else
                        {
                            pic = pic + item;
                        }

                    }
                    currentData.PICDept = pic;
                }
                if (inspectors != null)
                {
                    var depts = string.Empty;
                    foreach (var item in inspectors)
                    {
                        var last = inspectors.Last();
                        if (!item.EndsWith(last))
                        {
                            depts = depts + item + ",";
                        }
                        else
                        {
                            depts = depts + item;
                        }
                    }
                    currentData.Inspector = depts;
                }
              
                currentData.GroupItem = model.GroupItem;
                currentData.PICVisit = model.PICVisit;
                currentData.TenantName = model.TenantName;
                currentData.ChecklistHeader = model.ChecklistHeader;
                currentData.Remark = model.Remark;
                if (submitButton == "done")
                {
                    currentData.ApprovalStatus = "SUBMIT";
                }
                currentData.UpdateDate = DateTime.Now;
                currentData.UpdateBy = loginID;
                currentData.UpdateSave<RiskVisit>();
                var currentChecklist = RiskVisit.GetRiskVisitChecklistByVisitID(model.VisitID).ToList();
                //if (currentChecklist != null)
                //{
                //    foreach (var item in currentChecklist)
                //    {
                //        item.Delete<RiskVisitChecklist>();
                //    }
                //}
                if (detailid != null)
                {
                    for (int i = 0; i < detailid.Length; i++)
                    {
                        var newChk = RiskVisit.GetRiskVisitDetail(detailid[i]);

                        if (newChk != null)
                        {
                            newChk.RiskVisitId = model.VisitID;
                            newChk.Category = kategori[i];
                            newChk.Area = area[i];
                            newChk.PICDept = PICDept[i];
                            newChk.Condition = condition[i];
                            newChk.Remark = Keterangan[i];
                            newChk.Deadline = DateTime.Parse(deadline[i]);
                            if (!string.IsNullOrEmpty(isupload[i]))
                            {
                                string UploadPath = "/assets/Image/RiskVisitDocument/";
                                var filename = Regex.Replace(dokumentasi[i].FileName.Replace(' ', '_'), "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + Guid.NewGuid() + Path.GetExtension(dokumentasi[i].FileName);
                                string path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), filename);
                                dokumentasi[i].SaveAs(path);
                                newChk.Documentation = UploadPath + filename;
                            }
                            newChk.UpdateDate = DateTime.Now;
                            newChk.UpdatedBy = loginID;
                            newChk.UpdateSave<RiskVisitChecklist>();
                        }

                    }
                }
                else
                {
                    if (currentChecklist != null)
                    {
                        foreach (var item in currentChecklist)
                        {
                            item.Delete<RiskVisitChecklist>();
                        }
                    }
                    for (int i = 0; i < kategori.Length; i++)
                    {
                        var newChk = new RiskVisitChecklist();
                        newChk.Area = area[i];
                        newChk.Condition = condition[i];
                        newChk.ChecklistID = Guid.NewGuid();
                        newChk.PICDept = PICDept[i];
                        //if (PICvis != null)
                        //{
                        //    var pic = string.Empty;
                        //    foreach (var item in PICvis)
                        //    {
                        //        var last = PICvis.Last();
                        //        if (!item.Equals(last))
                        //        {
                        //            pic = pic + item + ",";
                        //        }
                        //        else
                        //        {
                        //            pic = pic + item;
                        //        }

                        //    }
                        //    newChk.PICDept = pic;
                        //}
                      
                        newChk.RiskVisitId = currentData.VisitID;
                        newChk.Category = kategori[i];
                        newChk.Remark = Keterangan[i];
                        newChk.Deadline = DateTime.Parse(deadline[i]);
                        if (!string.IsNullOrEmpty(isupload[i]))
                        {
                            string UploadPath = "/assets/Image/RiskVisitDocument/";
                            var filename = Regex.Replace(dokumentasi[i].FileName.Replace(' ', '_'), "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + Guid.NewGuid() + Path.GetExtension(dokumentasi[i].FileName);
                            string path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), filename);
                            dokumentasi[i].SaveAs(path);
                            newChk.Documentation = UploadPath + filename; ;
                        }
                        newChk.CreatedDate = DateTime.Now;
                        newChk.CreatedBy = loginID;
                        newChk.InsertSave<RiskVisitChecklist>();
                    }
                }
                if (submitButton == "done")
                {
                    var approval = ApprovalGroup.GetAllApprovalGroupByGroupID("RISK").ToList();
                    if (approval != null)
                    {
                        foreach (var item in approval)
                        {
                            WorkFlow wf = new WorkFlow();
                            wf.WorkFlowWorkOrderID = currentData.VisitID;
                            wf.WorkFlowApproval = item.ApprovalId;
                            wf.WorkFlowStatus = "S";
                            wf.CreatedDate = DateTime.Now;
                            wf.CreatedBy = loginID;
                            wf.InsertSave<WorkFlow>();
                        }
                    }

                }
            }
            else
            {
                var newData = new RiskVisit();
                //var workodr = WorkOrderManual.GetWorkOderByID(model.WorkOrderID);
                //workodr.UpdateSave<WorkOrderManual>();
                var count = RiskVisit.riskCount() + 1;
                newData.VisitID = Departements + "-" + model.ChecklistHeader + "-" + DateTime.Now.Year + DateTime.Now.Month + "-" + count.ToString().PadLeft(4, '0');
                newData.WorkOrderID = model.WorkOrderID;
                newData.VisitDate = model.VisitDate;
                newData.VisitTimeFrom = model.VisitTimeFrom;
                newData.VisitTimeTo = model.VisitTimeTo;
 
                newData.GroupItem = model.GroupItem;
                newData.PICVisit = model.PICVisit;
                newData.ApprovalStatus = "OPEN";
                if (submitButton == "done")
                {
                    newData.ApprovalStatus = "SUBMIT";
                }
                if (PICvis != null)
                {
                    var pic = string.Empty;
                    foreach (var item in PICvis)
                    {
                        var last = PICvis.Last();
                        if (!item.Equals(last))
                        {
                            pic = pic + item + ",";
                        }
                        else
                        {
                            pic = pic + item;
                        }

                    }
                    newData.PICDept = pic;
                }
                if (inspectors != null)
                {
                    var depts = string.Empty;
                    foreach (var item in inspectors)
                    {
                        var last = inspectors.Last();
                        if (!item.EndsWith(last))
                        {
                            depts = depts + item + ",";
                        }
                        else
                        {
                            depts = depts + item;
                        }
                    }
                    newData.Inspector = depts;
                }

                newData.Remark = model.Remark;
                newData.ChecklistHeader = model.ChecklistHeader;
                newData.CreatedBy = loginID;
                newData.TenantName = model.TenantName;
                newData.CreatedDate = DateTime.Now;
                newData.InsertSave<RiskVisit>();

                var currentChecklist = RiskVisit.GetRiskVisitChecklistByVisitID(model.VisitID).ToList();
                if (currentChecklist != null)
                {
                    foreach (var item in currentChecklist)
                    {
                        item.Delete<RiskVisitChecklist>();
                    }
                }
                for (int i = 0; i < kategori.Length; i++)
                {
                    var newChk = new RiskVisitChecklist();
                    newChk.Area = area[i];
                    newChk.Condition = condition[i];
                    newChk.ChecklistID = Guid.NewGuid();
                    newChk.RiskVisitId = newData.VisitID;
                    newChk.Category = kategori[i];
                    newChk.Remark = Keterangan[i];
                    newChk.Deadline = DateTime.Parse(deadline[i]);
                    if (!string.IsNullOrEmpty(isupload[i]))
                    {
                        string UploadPath = "/assets/Image/RiskVisitDocument/";
                        var filename = Regex.Replace(dokumentasi[i].FileName.Replace(' ', '_'), "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + Guid.NewGuid() + Path.GetExtension(dokumentasi[i].FileName);
                        string path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), filename);
                        dokumentasi[i].SaveAs(path);
                        newChk.Documentation = UploadPath + filename;
                    }
                    newChk.CreatedDate = DateTime.Now;
                    newChk.CreatedBy = loginID;
                    newChk.InsertSave<RiskVisitChecklist>();
                }
                if (submitButton == "done")
                {
                    var approval = UserGroupHeader.GetGroupHeaderByID("RISK");
                    if (approval != null)
                    {
                        foreach (var item in approval.UserGroupDetails)
                        {
                            WorkFlow wf = new WorkFlow();
                            wf.WorkFlowWorkOrderID = newData.VisitID;
                            wf.WorkFlowApproval = item.UserID;
                            wf.WorkFlowStatus = "S";
                            wf.CreatedDate = DateTime.Now;
                            wf.CreatedBy = loginID;
                            wf.InsertSave<WorkFlow>();
                        }
                    }

                }
            }

            return RedirectToAction(MVC.Risk.Todo.Index());
        }
    }
}