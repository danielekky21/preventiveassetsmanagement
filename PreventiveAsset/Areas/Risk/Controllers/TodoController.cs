﻿using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Areas.Risk.Controllers
{
    public partial class TodoController : Controller
    {
        // GET: Risk/Todo
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual JsonResult GetTodo()
        {
            try
            {
                var yesterday = DateTime.Now.AddDays(-1);
                var year = DateTime.Now.Year;
                var todo = RiskVisit.GetOpenRiskVisit().ToList().Where(x => x.CreatedDate.Value.Year == year);
                //.Where(x => x.WorkOrderManual.WorkOrderDate >= yesterday && x.WorkOrderManual.WorkOrderDate <= DateTime.Now)
                var eventList = todo.Select(x => new CalendarModel
                {
                    id = x.WorkOrderID,
                    title = x.VisitID + " - " + x.WorkOrderManual.AssetLocationCode + " - ( " +  x.ApprovalStatus + " )",
                    start = x.WorkOrderManual.WorkOrderDate.ToString(),
                    end = x.WorkOrderManual.WorkOrderDate.ToString(),
                    color = "#2980b9",
                    allDay = false
                }).ToArray();

                return Json(eventList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                throw;
            }
        }

    }
}