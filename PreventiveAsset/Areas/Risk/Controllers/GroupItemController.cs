﻿using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Areas.Risk.Controllers
{
    public partial class GroupItemController : Controller
    {
        // GET: Risk/GroupItem
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            return View();
        }
        public virtual JsonResult GetGroupItemGrid(int draw, int start, int length, string Departement, string Description)
        {
            try
            {
                int sortColumn = -1;
                var role = Session["Role"].ToString();

                var model = GroupItem.GetAllGroupItem().OrderByDescending(x => x.CreatedDate);
                //var Data = model.OrderBy(x => x.sorting).ThenBy(x => x.WorkOrderDate);
                if (!String.IsNullOrEmpty(Description))
                {
                    model = model.Where(x => x.Description.ToLower().Contains(Description.ToLower())).OrderByDescending(x => x.CreatedDate);
                }
                int countTotal = model.Count();
                var data = model.ToList();
                if (model.Count() > 10)
                {
                    data = data.GetRange(start, Math.Min(length, data.Count - start));
                }
                string sortDirection = "asc";
                if (length == -1)
                {
                    length = countTotal;
                }
                DataTableData dataTableData = new DataTableData();
                dataTableData.draw = draw;
                dataTableData.recordsTotal = countTotal;
                dataTableData.ListData = AssignGroupItem(data);
                int recordsFiltered = countTotal;
                return Json(new
                {
                    // this is what datatables wants sending back
                    draw = dataTableData.draw,
                    recordsTotal = 10,
                    recordsFiltered = recordsFiltered,
                    data = dataTableData.ListData
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                throw;
            }
        }
        private List<DataItem> AssignGroupItem(List<GroupItem> data)
        {
            List<DataItem> dataitem = new List<DataItem>();

            foreach (var item in data)
            {
                DataItem items = new DataItem();
                items.GroupItemID = item.GroupItemID;
                items.WorkOrderDate = item.CreatedDate.ToString();
                items.Description = item.Description;
                if (item.IsActive.GetValueOrDefault())
                {
                    items.Status = "Active";
                }
                else
                {
                    items.Status = "Not Active";
                }

                dataitem.Add(items);
            }
            return dataitem;
        }
        public virtual ActionResult AddEdit(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var data = GroupItem.GetGroupItemByID(id);
                ViewData.Model = data;
            }
            else
            {
                ViewData.Model = new GroupItem();
            }
            SelectList sel = new SelectList();
            ViewBag.act = sel.GetActiveSelectList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(GroupItem Model)
        {
            string loginID = Session["UserId"].ToString();
            if (!string.IsNullOrEmpty(Model.GroupItemID))
            {
                var data = GroupItem.GetGroupItemByID(Model.GroupItemID);
                if (data != null)
                {
                    data.Description = Model.Description;
                    data.IsActive = Model.IsActive;
                    data.UpdatedDate = DateTime.Now;
                    data.UpdatedBy = loginID;
                    data.UpdateSave<GroupItem>();
                }
            }
            else
            {
                GroupItem newData = new GroupItem();
                var count = GroupItem.CountItem();
                count = count + 1;
                newData.GroupItemID = "GITM/" + count.ToString().PadLeft(4, '0');
                newData.Description = Model.Description;
                newData.IsActive = Model.IsActive;
                newData.CreatedBy = loginID;
                newData.CreatedDate = DateTime.Now;
                newData.InsertSave<GroupItem>();
            }
            return RedirectToAction(MVC.Risk.GroupItem.List());
        }
    }
}