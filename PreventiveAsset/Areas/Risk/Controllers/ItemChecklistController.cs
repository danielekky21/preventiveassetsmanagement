﻿using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Areas.Risk.Controllers
{
    public partial class ItemChecklistController : Controller
    {
        // GET: Risk/ItemChecklist
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult CheckklistHeaderList()
        {
            return View();
        }
        public virtual JsonResult GetChecklistHeaderGrid(int draw, int start, int length, string Departement,string Description)
        {
            try
            {
                int sortColumn = -1;
                var role = Session["Role"].ToString();

                var model = ChecklistHeader.GetAllChecklistHeader().OrderByDescending(x => x.CreatedDate);
                //var Data = model.OrderBy(x => x.sorting).ThenBy(x => x.WorkOrderDate);
                if (!String.IsNullOrEmpty(Description))
                {
                    model = model.Where(x => x.Description.ToLower().Contains(Description.ToLower())).OrderByDescending(x => x.CreatedDate);
                }
                int countTotal = model.Count();
                var data = model.ToList();
                if (model.Count() > 10)
                {
                    data = data.GetRange(start, Math.Min(length, data.Count - start));
                }
                string sortDirection = "asc";
                if (length == -1)
                {
                    length = countTotal;
                }
                DataTableData dataTableData = new DataTableData();
                dataTableData.draw = draw;
                dataTableData.recordsTotal = countTotal;
                dataTableData.ListData = AssignCheklistHeader(data);

                int recordsFiltered = countTotal;
                return Json(new
                {
                    // this is what datatables wants sending back
                    draw = dataTableData.draw,
                    recordsTotal = 10,
                    recordsFiltered = recordsFiltered,
                    data = dataTableData.ListData
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                throw;
            }
        }
        public virtual JsonResult GetChecklistDetailGrid(int draw, int start, int length, string Departement,string Description)
        {
            try
            {
                int sortColumn = -1;
                var role = Session["Role"].ToString();

                var model = ItemChecklistDetail.GetAllItemCheclistDetail().OrderByDescending(x => x.CreatedDate);
                //var Data = model.OrderBy(x => x.sorting).ThenBy(x => x.WorkOrderDate);
                if (!string.IsNullOrEmpty(Description))
                {
                    model = model.Where(x => x.Description.ToLower().Contains(Description.ToLower())).OrderByDescending(x => x.CreatedDate);
                }
                int countTotal = model.Count();
                var data = model.ToList();
                if (model.Count() > 10)
                {
                    data = data.GetRange(start, Math.Min(length, data.Count - start));
                }
                string sortDirection = "asc";
                if (length == -1)
                {
                    length = countTotal;
                }
                DataTableData dataTableData = new DataTableData();
                dataTableData.draw = draw;
                dataTableData.recordsTotal = countTotal;
                dataTableData.ListData = AssignCheklistDetail(data);

                int recordsFiltered = countTotal;
                return Json(new
                {
                    // this is what datatables wants sending back
                    draw = dataTableData.draw,
                    recordsTotal = 10,
                    recordsFiltered = recordsFiltered,
                    data = dataTableData.ListData
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                throw;
            }
        }
        private List<DataItem> AssignCheklistHeader(List<ChecklistHeader> data)
        {
            List<DataItem> dataitem = new List<DataItem>();

            foreach (var item in data)
            {
                DataItem items = new DataItem();
                items.ChecklistHeaderID = item.ChecklistHeaderID;
                items.WorkOrderDate = item.CreatedDate.ToString();
                items.Description = item.Description;
                if (item.IsActive.GetValueOrDefault())
                {
                    items.Status = "Active";
                }
                else
                {
                    items.Status = "Not Active";
                }

                dataitem.Add(items);
            }
            return dataitem;
        }
        private List<DataItem> AssignCheklistDetail(List<ItemChecklistDetail> data)
        {
            List<DataItem> dataitem = new List<DataItem>();

            foreach (var item in data)
            {
                DataItem items = new DataItem();
                items.ChecklistDetailID = item.ChecklistDetail.ToString();
                items.WorkOrderDate = item.CreatedDate.ToString();
                items.Description = item.Description;
                if (item.IsActive.GetValueOrDefault())
                {
                    items.Status = "Active";
                }
                else
                {
                    items.Status = "Not Active";
                }

                dataitem.Add(items);
            }
            return dataitem;
        }

        public virtual ActionResult List()
        {
            return View();
        }
        public virtual ActionResult ItemDetailAddEdit(long id)
        {
            var data = ItemChecklistDetail.GetItemDetailByID(id);
            if (data != null)
            {
                ViewData.Model = data;
            }
            else
            {
                ViewData.Model = new ItemChecklistDetail();
            }
            SelectList sel = new SelectList();
            ViewBag.act = sel.GetActiveSelectList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult ItemDetailAddEdit(ItemChecklistDetail model)
        {
            string loginID = Session["UserId"].ToString();
            if (model.ChecklistDetail != 0)
            {
                var data = ItemChecklistDetail.GetItemDetailByID(model.ChecklistDetail);
                if (data != null)
                {
                    data.Description = model.Description;
                    data.UpdatedDate = DateTime.Now;
                    data.UpdatedBy = loginID;
                    data.UpdateSave<ItemChecklistDetail>();
                }
            }
            else
            {
                var newData = new ItemChecklistDetail();
                newData.Description = model.Description;
                newData.CreatedBy = loginID;
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = model.IsActive;
                newData.InsertSave<ItemChecklistDetail>();
            }
            return RedirectToAction(MVC.Risk.ItemChecklist.List());
        }
        public virtual ActionResult ChecklistHeaderAddEdit(string id)
        {
            var data = ChecklistHeader.GetChecklistHeaderById(id);
            if (data != null)
            {
                ViewData.Model = data;
            }
            else
            {
                ViewData.Model = new ChecklistHeader();
            }
            SelectList sel = new SelectList();
            ViewBag.stat = sel.GetActiveSelectList();
            ViewBag.DetailData = ItemChecklistDetail.GetAllItemCheclistDetail().ToList();
            ViewBag.AssignDetail = CurrentDataContext.CurrentContext.ChecklistHeaderDetails.Where(x => x.ItemChecklistHeader == id).ToList();
            ViewBag.group = sel.GetItemGroupCode();
            return View();
        }
        [HttpPost]
        public virtual ActionResult ChecklistHeaderAddEdit(ChecklistHeader model, int[] mutliselect_to)
        {
            string loginID = Session["UserId"].ToString();
            if (!string.IsNullOrEmpty(model.ChecklistHeaderID))
            {
                var current = ChecklistHeader.GetChecklistHeaderById(model.ChecklistHeaderID);

                if (current != null)
                {
                    current.Description = model.Description;
                    current.IsActive = model.IsActive;
                    current.UpdatedDate = DateTime.Now;
                    current.GroupItem = model.GroupItem;
                    current.UpdatedBy = loginID;
                    current.UpdateSave<ChecklistHeader>();
                }
                var data = ChecklistHeaderDetail.GetByChecklistHeader(model.ChecklistHeaderID);
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        item.Delete<ChecklistHeader>();
                    }
                }

                if (mutliselect_to != null)
                {
                    for (int i = 0; i < mutliselect_to.Count(); i++)
                    {
                        ChecklistHeaderDetail newData = new ChecklistHeaderDetail();
                        newData.ItemChecklistHeader = model.ChecklistHeaderID;
                        newData.ItemChecklistDetail = mutliselect_to[i];
                        newData.InsertSave<ChecklistHeaderDetail>();

                    }
                }
            }
            else
            {
                var newData = new ChecklistHeader();
                var count = ChecklistHeader.CountChkHeader();
                count = count + 1;
                newData.ChecklistHeaderID = "CHK/" + count.ToString().PadLeft(4, '0');
                newData.Description = model.Description;
                newData.CreatedBy = loginID;
                newData.GroupItem = model.GroupItem;
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = model.IsActive;
                newData.InsertSave<ChecklistHeader>();
                var data = ChecklistHeaderDetail.GetByChecklistHeader(newData.ChecklistHeaderID);
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        item.Delete<ChecklistHeader>();
                    }
                }

                if (mutliselect_to != null)
                {
                    for (int i = 0; i < mutliselect_to.Count(); i++)
                    {
                        ChecklistHeaderDetail newDatas = new ChecklistHeaderDetail();
                        newDatas.ItemChecklistHeader = newData.ChecklistHeaderID;
                        newDatas.ItemChecklistDetail = mutliselect_to[i];
                        newDatas.InsertSave<ChecklistHeaderDetail>();

                    }
                }
            }
            return RedirectToAction(MVC.Risk.ItemChecklist.CheckklistHeaderList());
        }
    }
}