﻿using PreventiveAssetManagement.dataRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset
{
    public class SelectList
    {
        public IEnumerable<SelectListItem> GetActiveSelectList()
        {
            var selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem
            {
                Value = "true",
                Text = "Active"
            });
            selectList.Add(new SelectListItem
            {
                Value = "false",
                Text = "InActive"
            });
            return selectList;
        }
        public IEnumerable<SelectListItem> GetRole()
        {
            var selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem
            {
                Value = "pic",
                Text = "PIC"
            });
            selectList.Add(new SelectListItem
            {
                Value = "manager",
                Text = "Manager"
            });
            selectList.Add(new SelectListItem
            {
                Value = "Leader",
                Text = "leader"
            });
            selectList.Add(new SelectListItem
            {
                Value = "it",
                Text = "IT"
            });
            selectList.Add(new SelectListItem
            {
                Value = "Risk",
                Text = "RISK"
            });
            return selectList;
        }
        public IEnumerable<SelectListItem> GetIntervalGroupSelectlist()
        {
            var selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem
            {
                Value = "Periodic",
                Text = "Periodic"
            });
            selectList.Add(new SelectListItem
            {
                Value = "Hour",
                Text = "Hour"
            });
            return selectList;
        }
        public IEnumerable<SelectListItem> GetIntervalTypeSeleclist()
        {
            var selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem
            {
                Value = "Week",
                Text = "Week"
            });
            selectList.Add(new SelectListItem
            {
                Value = "Month",
                Text = "Month"
            });
            selectList.Add(new SelectListItem
            {
                Value = "Year",
                Text = "Year"
            });
            return selectList;
        }
        public IEnumerable<SelectListItem> GetMasterIntervalDesc()
        {
            var selectlist = new List<SelectListItem>();
            var interval = Interval.GetAllActiveMasterInterval();
            foreach (var item in interval)
            {
                selectlist.Add(new SelectListItem
                {
                    Value = item.IntervalCode,
                    Text = item.Description

                });
            }
            return selectlist;
        }
        public IEnumerable<SelectListItem> GetActiveUserSelectList()
        {
            var selectList = new List<SelectListItem>();
            var user = MasterUser.GetAllUser().OrderByDescending(x => x.UserID).ToList();
            foreach (var item in user)
            {

                selectList.Add(new SelectListItem
                {
                    Value = item.UserID,
                    Text = item.Name
                });
            }
            return selectList;
        }
        public IEnumerable<SelectListItem> GetLocationSelectList()
        {
            var selectList = new List<SelectListItem>();
            var location = MasterLocation.GetAllActiveLocation().OrderByDescending(x => x.CreatedDate).ToList();
            foreach (var item in location)
            {
                selectList.Add(new SelectListItem
                {
                    Value = item.LocationCode,
                    Text = item.LocationCode
                });
            }
            return selectList;
        }
        public IEnumerable<SelectListItem> GetLocationAreaSelectList()
        {
            var selectList = new List<SelectListItem>();
            var location = MasterLocation.GetAllActiveLocation().OrderByDescending(x => x.CreatedDate).ToList();
            foreach (var item in location)
            {
                selectList.Add(new SelectListItem
                {
                    Value = item.LocationCode,
                    Text = item.LocationArea
                });
            }
            return selectList;
        }
        public IEnumerable<SelectListItem> GetUserGroupSelectList()
        {
            var selectList = new List<SelectListItem>();
            var usergroup = UserGroupHeader.GetAllGroupHeader().OrderByDescending(x => x.CreatedDate).ToList();
            foreach (var item in usergroup)
            {
                selectList.Add(new SelectListItem
                {
                    Value = item.UserGroupId,
                    Text = item.UserGroupId
                });
            }
            return selectList;
        }
        public List<string> GetIntervalInformation()
        {
            List<string> interval = new List<string>();
            interval.Add("500 Hour");
            interval.Add("1000 Hour");
            interval.Add("Weekly");
            interval.Add("Biweekly");
            interval.Add("Monthly");
            interval.Add("Bimonthly");
            interval.Add("Trimester");
            interval.Add("Semester");
            interval.Add("Yearly");
            return interval;
        }
        public List<SelectListItem> GetIKSelectList()
        {
            var selectlist = new List<SelectListItem>();
            var ik = MasterIK.GetAllIK().Where(x => x.IsActive == true);
            foreach (var item in ik)
            {
                selectlist.Add(new SelectListItem
                {
                    Value = item.IKCode,
                    Text = item.IKCode
                });
            }
            return selectlist;
        }
        public List<SelectListItem> GetAssetCategorySelectList()
        {
            var selectlist = new List<SelectListItem>();
            var assetcategory = MasterAssetCategory.GetAllMasterAssetCategory().Where(x => x.IsActive == true).ToList();
            foreach (var item in assetcategory)
            {
                selectlist.Add(new SelectListItem
                {
                    Value = item.AssetCategoryID,
                    Text = item.AssetCategoryID
                });
            }
            return selectlist;
        }
        public List<SelectListItem> GetAssetCategoryBydept(string dept)
        {
            var selectlist = new List<SelectListItem>();
            var assetcategory = MasterAssetCategory.GetAllMasterAssetCategory().Where(x => x.IsActive == true && x.Departement == dept).ToList();
            foreach (var item in assetcategory)
            {
                selectlist.Add(new SelectListItem
                {
                    Value = item.AssetCategoryID,
                    Text = item.AssetCategoryID
                });
            }
            return selectlist;
        }
        public List<SelectListItem> GetDepartementSelectlistItem()
        {
            var selectlist = new  List<SelectListItem>();
            var dept = MasterDepartement.GetAllDepartementList().OrderBy(x => x.DepartementName).ToList();
            foreach (var item in dept)
            {
                string deptID = item.DepartementID.ToString();
                selectlist.Add(new SelectListItem
                {
                    Value = deptID,
                    Text = item.DepartementName
                });
            }
            return selectlist;
        }
        public List<SelectListItem> GetDepartementNameSelectlistItem()
        {
            var selectlist = new List<SelectListItem>();
            var dept = MasterDepartement.GetAllDepartementList().OrderBy(x => x.DepartementName).ToList();
            foreach (var item in dept)
            {
                string deptID = item.DepartementID.ToString();
                selectlist.Add(new SelectListItem
                {
                    Value = item.DepartementName,
                    Text = item.DepartementDesc
                });
            }
            return selectlist;
        }
        public List<SelectListItem> GetAssetGroupSelectList()
        {
            var selectlist = new List<SelectListItem>();
            var assetcategory = MasterAssetCategory.GetAllMasterAssetCategory().Where(x => x.IsActive == true).GroupBy(x => x.AssetGroup).ToList();
            foreach (var item in assetcategory)
            {
                selectlist.Add(new SelectListItem
                {
                    Value = item.First().AssetGroup,
                    Text = item.First().AssetGroup
                });
            }
            return selectlist;
        } 
        public List<SelectListItem> GetUserIDNameSelectList()
        {
            var selectlist = new List<SelectListItem>();
            var role = HttpContext.Current.Session["Role"].ToString();
            var listUser = new List<MasterUser>();
            if (role.ToLower() == "manager" || role.ToLower() == "leader")
            {
                var grouplist = (List<UserGroupDetail>)HttpContext.Current.Session["GroupIDs"];
                foreach (var item in grouplist)
                {                                                                                                                                                                                                            
                    var workOrder = MasterUser.GetUserByGroup(item.UserGroupID).ToList();
                    listUser.AddRange(workOrder);
                }
                foreach (var user in listUser)
                {
                    selectlist.Add(new SelectListItem
                    {
                        Value = user.UserID,
                        Text = user.UserID + "-" + user.Name
                    });
                }
            }
            else
            {
                var grouplist = (UserGroupDetail)HttpContext.Current.Session["GroupID"];

                var userData = MasterUser.GetUserByGroup(grouplist.UserGroupID);
                foreach (var item in userData)
                {
                    selectlist.Add(new SelectListItem
                    {
                        Value = item.UserID + "-" + item.Name,
                        Text = item.UserID + "-" + item.Name
                    });
                }
            }


            return selectlist;
        }
        public List<SelectListItem> GetAssetTypeSelectList(string assetCategoryid)
        {
            var selectlist = new List<SelectListItem>();
            var assetype = MasterAssetType.GetAllAssetType().Where(x => x.IsActive == true && x.AssetCategoryID == assetCategoryid).OrderByDescending(x => x.CreatedDate).ToList();
            foreach (var item in assetype)
            {
                selectlist.Add(new SelectListItem
                {
                    Value = item.MasterAssetTypeID,
                    Text = item.MasterAssetTypeID
                });
            }
            return selectlist;
        }
        public List<SelectListItem> GetAssetTypeSelectListAll()
        {
            var selectlist = new List<SelectListItem>();
            var assetype = MasterAssetType.GetAllAssetType().Where(x => x.IsActive == true).OrderByDescending(x => x.CreatedDate).ToList();
            foreach (var item in assetype)
            {
                selectlist.Add(new SelectListItem
                {                                                                                                                                               
                    Value = item.MasterAssetTypeID,
                    Text = item.MasterAssetTypeID
                });
            }
            return selectlist;
        }
        public List<SelectListItem> GetDepartementSelectList()
        {
            var selectlist = new List<SelectListItem>();
            var departement = MasterDepartement.GetAllDepartementList().ToList();
            foreach (var item in departement)
            {
                selectlist.Add(new SelectListItem
                {
                    Value = item.DepartementName,
                    Text = item.DepartementName
                });
            }
            return selectlist;
        }
        public List<SelectListItem> GetItemGroupCode()
        {
            var selectlist = new List<SelectListItem>();
            var data = GroupItem.GetAllGroupItem().ToList();
            foreach (var item in data)
            {
                selectlist.Add(new SelectListItem
                {
                    Value = item.GroupItemID,
                    Text = item.Description
                });
            }
            return selectlist;
        }
    }

}