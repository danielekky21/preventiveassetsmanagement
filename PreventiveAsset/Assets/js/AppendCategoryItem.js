﻿$(document).ready(function () {
    var dateToday = new Date();
    $(".timepicker").timepicker(
       
    );
    $("#printPage").click(function () {
        $(".d-print-none").css("display", "none")

        window.print();
        $(".d-print-none").css("display", "block")

    })
    $(".select2").select2();
    $(".vd").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Vendor/autocompletevendor",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.VendorName, value: item.VendorName };
                    }))

                }
            })
        }
    })
    $("#FormChangePass").parsley();
    $("#formIKCat").parsley();
    $("#FormAddUser").parsley();
    $("#FormLocation").parsley();
    $("#formUserGroup").parsley();
    $("#formIK").parsley();
    $("#FormAssetCat").parsley();
    $("#AssetType").parsley();
    $("#IntervalForm").parsley();
    //$('.addNewIKItem').click(function () {

    $("#color").colorpicker();
    //})
    //$('.addNewIKCat').click(function () {

    //    var container = $('#ikCategoryContainer');
    //    container.append("<div class='form-group row'><div class='col-md-5 control-label'><label><b>Detail</b></label></div><div class='col-md-7'><a href='#' id='deleteIKCat' class='btn btn-sm btn-danger deleteIKCat'><i class='icon-minus'></i></a></div></div><div class='form-group row'><div class='col-md-5 control-label'><label for='name'>IK Category</label></div><div class='col-md-7'><input type='text' class='form-control' name='ikCategory' placeholder='IK Category' /></div></div><div class='col-md-12' id='ikItemContainer'><div class='form-group row'><div class='col-md-5 control-label'><label><b>Item</b></label></div><div class='col-md-7'><a href='#' id='addNewIKItem' class='btn btn-sm btn-success'><i class='icon-plus'></i></a><a href='#' id='deleteIKItem' class='btn btn-sm btn-danger'><i class='icon-minus'></i></a></div></div><div class='form-group row'><div class='col-md-5 control-label'><label for='name'>IK Item</label> </div><div class='col-md-7'><input type='text' class='form-control' name='ikCategory' placeholder='IK Item' /></div></div></div>")
    //})
    $("#addNewIKItem").click(function () {

        var container = $("#itemcontainer");
        var appended = "<div class='form-group row'><div class='col-md-5 control-label'><label for='name'>IK Item</label></div><div class='col-md-5'><input type='text' class='form-control' name='ikItem' placeholder='IK Item' required='' /></div><div class='col-md-2'><a href='#' class='btn btn-sm btn-danger deleterow'><i class='icon-minus'></i></a></div></div>";
        container.append(appended);
    })
    $("body").on("click", '.deleterow', function () {
        $(this).parent().parent().remove();
    })
    $(".datepicker").datepicker(
        {
            minDate: dateToday
        }
    );
    //$("body").on('click', ".datepicker", function () {
    //    var $this = $(this);
    //    //var dateToday = 
    //    $this.datepicker({
    //            minDate: new Date();
    //    }); // You should probably check whether datapicker is already attached before binding it. 
    //});
    $("#AssetCategoryID").change(function () {
        var val = $(this).val();
        $.ajax({
            url: "GetInstallmentDate/?id=" + val,
            contentType: "application/json",
            dataType: "json",
            success: function (result) {
                $("#installmentDate").text(result);
            }
        });
    })
    $("#addNewMaintenanceItem").click(function (e) {
        e.preventDefault();
        var container = $("#appendContainer");
        var trHead = $("#cloningelement");
        var cloninged = $("#cloningelement").clone();
        cloninged.removeAttr("id");
        cloninged.addClass("cloninged");
        //cloninged.find("[name=assettypeid]").find(":selected").remove();
        cloninged.insertAfter(trHead);
        var elem = cloninged.find(".datepicker");
        elem.removeClass("hasDatepicker");
        //$(".datepicker").datepicker("destroy");
        //var dateToday = new Date();
        elem.datepicker(
            {
                minDate: new Date()
            }
        );
    })
    $("body").on("click", '.addApproval', function (e) {
        e.preventDefault();
        var container = $("#approvalClone");
        var cloninged = container.clone();
        cloninged.removeAttr("id");
        cloninged.addClass("cloninged");
        cloninged.insertAfter(container);

    })
    $("body").on("click", '.deleteApproval', function (e) {
        e.preventDefault();
        var id = $(this).parent().parent().attr('id')
        if (id != "approvalClone") {
            $(this).parent().parent().remove();
        }

    })
    $("body").on("click", '.deleteMaintenanceItem', function (e) {
        e.preventDefault();
        var id = $(this).parent().parent().attr('id')
        if (id != "cloningelement") {
            $(this).parent().parent().remove();
        }

    })
    $("#calendarSearch").click(function () {
        $('#calendar').fullCalendar('destroy');
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next,today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            eventLimit: true,
            defaultView: 'month',
            //events: '/WorkOrder/GetWorkOderDate',
            //data: function () {
            //    return {
            //        categoryassetid: $("#CategoryAssetIdSearch").val(),
            //        workordersearch: $("#WorkOrderNoSearch").val()
            //    }
            //},
            eventSources: [

                // your event source
                {
                    url: '/WorkOrder/GetWorkOderDate',
                    type: 'POST',
                    data: {
                        categoryassetid: $("#CategoryAssetIdSearch").val(),
                        workordersearch: $("#WorkOrderNoSearch").val()
                    },
                    error: function () {
                        alert('there was an error while fetching events!');
                    },
                }

                // any other sources...

            ],
            eventClick: function (calEvent, jsEvent, view) {
                window.location.href = "/WorkOrder/EditWorkOrder/" + calEvent.id;
            }

        });

    })
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next,today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        eventLimit: true,
        defaultView: 'month',
        events: '/WorkOrder/GetWorkOderDate',
        data: function () {
            return {
                categoryassetid: $("#CategoryAssetIdSearch").val(),
                workordersearch: $("#WorkOrderNoSearch").val()
            }
        },
        eventClick: function (calEvent, jsEvent, view) {
            window.location.href = "/WorkOrder/EditWorkOrder/" + calEvent.id;
        }

    });

    $('#calendarToDo').fullCalendar({
        //defaultView: 'listWeek',
        //events: '/WorkOrder/GetWorkOderDate',
        //eventClick: function (calEvent, jsEvent, view) {
        //    window.location.href = "/WorkOrder/EditWorkOrder/" + calEvent.id;
        //}
        defaultView: 'listWeek',

        // customize the button names,
        // otherwise they'd all just say "list"
        views: {
            listWeek: { buttonText: 'list week' }
        },

        header: {
            left: '',
            center: 'title',
            right: 'listYear'
        },
        events: '/WorkOrder/GetWorkOderDate',
        eventClick: function (calEvent, jsEvent, view) {
            window.location.href = "/WorkOrder/EditWorkOrder/" + calEvent.id;
        }
    });
    $('#RiskTodo').fullCalendar({
        //defaultView: 'listWeek',
        //events: '/WorkOrder/GetWorkOderDate',
        //eventClick: function (calEvent, jsEvent, view) {
        //    window.location.href = "/WorkOrder/EditWorkOrder/" + calEvent.id;
        //}
        defaultView: 'listYear',

        // customize the button names,
        // otherwise they'd all just say "list"
        views: {
            listYear: { buttonText: 'list year' }
        },

        header: {
            left: '',
            center: 'title',
            right: 'listYear'
        },
        events: '/Risk/Todo/GetTodo',
        eventClick: function (calEvent, jsEvent, view) {
             window.location.href = "/Risk/RiskVisitForm/?id=" + calEvent.id;
        }
    });
    $("#AssetCategoryID").keyup(function () {
        var that = this,
            value = $(this).val();
        $.ajax({
            type: "GET",
            url: "/MasterAsset/CheckAssetCode",
            data: {
                'search_keyword': value
            },
            dataType: "text",
            success: function (res) {
                if (res == "true") {
                    alert("Please choose another asset category code");
                    $("#AssetCategoryID").val("");
                }
            }
        })
    })
    $("#UserGroupId").keyup(function () {
        var that = this,
            value = $(this).val();
        $.ajax({
            type: "GET",
            url: "/UserGroup/CheckUserGroupCode",
            data: {
                'search_keyword': value
            },
            dataType: "text",
            success: function (res) {
                if (res == "true") {
                    alert("Please choose another User Group code");
                    $("#UserGroupId").val("");
                }
            }
        })
    })
    $("#IKCode").keyup(function () {
        var that = this,
            value = $(this).val();
        $.ajax({
            type: "GET",
            url: "/InstruksiKerja/CheckIKCode",
            data: {
                'search_keyword': value
            },
            dataType: "text",
            success: function (res) {
                if (res == "true") {
                    alert("IK Category " + value + " already used, Please choose another name");
                    $("#IKCode").val("");
                }
            }
        })
    })
    $("#IKCategory1").keyup(function () {
        var that = this,
            value = $(this).val();
        $.ajax({
            type: "GET",
            url: "/InstruksiKerja/checkIKCate",
            data: {
                'search_keyword': value
            },
            dataType: "text",
            success: function (res) {
                if (res == "true") {
                    alert("IK Category " + value + " already used, Please choose another name");
                    $("#IKCategory1").val("");
                }
            }
        })
    })
    //validate work order
    $("button[name='btnName']").click(function (e) {
        e.preventDefault();
        var conf = confirm("Are You Sure?");
        if (conf) {
            var btn = $(this);
            var frm = $(this).parent().parent().parent();
            $("#btnNames").val(btn.val());
            var isFalse = 0;
            if (btn.val() == "save") {
                var nominal = $(".nominal");
                for (var i = 0; i < nominal.length; i++) {
                    if (nominal[i].value.match(/^[1-9]\d*(,\d+)?$/)) {
                    }
                    else
                    {
                        isFalse = isFalse + 1;
                    }
                }
                if (isFalse == 0) {
                    frm.submit();
                }
                else {
                    alert("Nominal must be a currency format!");
                }
            }
            else {
                var radioList = $("input[name*='cond']");
                var counting = 0;
                for (var i = 0; i < radioList.length; i++) {
                    if (radioList[i].checked) {
                        if (radioList[i].value == "Bad") {
                            counting = counting + 1;
                        }
                    }
                }
                if (counting > 0) {
                    alert("Please take a look asset condition!")
                }
                else {
                    frm.parsley().validate();
                    if (frm.parsley().isValid()) {
                        if (nominal[i].value.match(/^[1-9]\d*(,\d+)?$/)) {
                        }
                        else {
                            isFalse = isFalse + 1;
                        }
                        if (isFalse == 0) {
                            frm.submit();
                        }
                        else
                        {
                            alert("Nominal must be a currency format!");
                        }
                    }
                }
            }
        }
    })
    function isNumeric(obj) {
        var realStringObj = obj && obj.toString();
        return !jQuery.isArray(obj) && (realStringObj - parseFloat(realStringObj) + 1) >= 0;
    }
    $("#assetcategorycode").change(function () {
        var vals = $(this).val();
        $.ajax({
            type: "POST",
            url: "/MasterAsset/GetCascadeGroup?assetCategoryID=" + vals,
            contentType: "application/json",
            dataType: "json",
            success: function (response) {
                $("#assettype").empty();
                //$.each(response, function (index) {
                //    $("#assettype").append($("<option></option>").val(this[index]).html(this[index]));
                //});
                $('#assettype').append($('<option>', {
                    value: "",
                    text: "--select one--"
                }));
                for (var i = 0; i < response.text.length; i++) {
                    //$("#assettype").append($("<option></option>").val(this[i]).html(this[i]));

                    $('#assettype').append($('<option>', {
                        value: response.text[i],
                        text: response.text[i]
                    }));
                }
            }
        });
    })
    $.navigation.find('a').each(function () {

        var cUrl = String(window.location).split('?')[0];

        if (cUrl.substr(cUrl.length - 1) == '#') {
            cUrl = cUrl.slice(0, -1);
        }

        if ($($(this))[0].href == cUrl) {
            $(this).addClass('active');

            $(this).parents('ul').add(this).each(function () {
                $(this).parent().addClass('open');
            });
        }
    });

    // Dropdown Menu
    $.navigation.on('click', 'a', function (e) {

        if ($.ajaxLoad) {
            e.preventDefault();
        }

        if ($(this).hasClass('nav-dropdown-toggle')) {
            $(this).parent().toggleClass('open');
            resizeBroadcast();
        }

    });

    function resizeBroadcast() {

        var timesRun = 0;
        var interval = setInterval(function () {
            timesRun += 1;
            if (timesRun === 5) {
                clearInterval(interval);
            }
            window.dispatchEvent(new Event('resize'));
        }, 62.5);
    }

    /* ---------- Main Menu Open/Close, Min/Full ---------- */
    $('.sidebar-toggler').click(function () {
        $('body').toggleClass('sidebar-hidden');
        resizeBroadcast();
    });

    $('.sidebar-minimizer').click(function () {
        $('body').toggleClass('sidebar-minimized');
        resizeBroadcast();
    });

    $('.brand-minimizer').click(function () {
        $('body').toggleClass('brand-minimized');
    });

    $('.aside-menu-toggler').click(function () {
        $('body').toggleClass('aside-menu-hidden');
        resizeBroadcast();
    });

    $('.mobile-sidebar-toggler').click(function () {
        $('body').toggleClass('sidebar-mobile-show');
        resizeBroadcast();

    });

    $('.sidebar-close').click(function () {
        $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
    });

    /* ---------- Disable moving to top ---------- */
    $('a[href="#"][data-top!=true]').click(function (e) {
        e.preventDefault();
    });
    /* ---------- Apply to all vendor Work order -------- */
    $("#applyVendorall").click(function () {
        var vendorname = $("input[name='vendorname']")[0].value
        var workstartdate = $("input[name='workstartdate']")[0].value
        var startedby = $("select[name='startedby']")[0].value
        var workenddate = $("input[name='workenddate']")[0].value
        var completedby = $("select[name='completedby']")[0].value
        var picremarks = $("textarea[name='picremarks']")[0].value

        for (var i = 1; i < $("input[name='vendorname']").length; i++) {
            $("input[name='vendorname']")[i].value = vendorname;
            $("input[name='workstartdate']")[i].value = workstartdate;
            $("select[name='startedby']")[i].value = startedby;
            $("input[name='workenddate']")[i].value = workenddate;
            $("select[name='completedby']")[i].value = completedby;
            $("textarea[name='picremarks']")[i].value = picremarks;
        }
    })
    $("#RiskWorkOrderSearch").click(function (e) {
        e.preventDefault();
        $('#data-table-Risk').DataTable().destroy();
        $("#data-table-Risk").DataTable({
            "processing": true,
            "displayStart": 0,
            "serverSide": true,
            searching: false,
            "info": true,
            "stateSave": true,
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "ajax": {
                "url": "RiskWorkOrderList/GetWorkOrderGrid",
                "type": "GET",
                "data": { "Departement": $("#dpt").val(), "WorkOrderDateFrom": $("#datevaluefrom").val(), "WorkOrderDateTo": $("#datevaluefto").val(), "Category" : $("#ItemDesc").val() }
            },
            "columns": [
                { "data": "WorkOrderNo" },
                { "data": "Category" },
                { "data": "Departement" },
                { "data": "WorkOrderDate" },
                { "data": "Location" },
                { "data": "Status" },
                {
                    "targets": -1,
                    "render": function (data, type, full) {
                        return "<a href='#' data-id='" + full.WorkOrderNo + "' class='btn btn-sm btn-primary pick'><i class='icon-check'></i></a>";
                    }
                }
            ],
            "order": [[0, "asc"]]
        })

    })
    $(document).on('click', '.pick', function () {
        var dataid = $(this).data('id');
        $.ajax({
            type: "POST",
            url: "/Risk/RiskVisitForm/PickWorkOrder?id=" + dataid,
            contentType: "application/json",
            dataType: "json",
            success: function (response) {
                if (response == "success") {
                    alert("Work Order Moved to Reminder!");
                    $('#data-table-Risk').DataTable().destroy();
                    $("#data-table-Risk").DataTable({
                        "processing": true,
                        "displayStart": 0,
                        "serverSide": true,
                        searching: false,
                        "info": true,
                        "stateSave": true,
                        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
                        "ajax": {
                            "url": "RiskWorkOrderList/GetWorkOrderGrid",
                            "type": "GET",
                            "data": { "Departement": $("#dpt").val(), "WorkOrderDateFrom": $("#datevaluefrom").val(), "WorkOrderDateTo": $("#datevaluefto").val() }
                        },
                        "columns": [
                            { "data": "WorkOrderNo" },
                            { "data": "Category" },
                            { "data": "Departement" },
                            { "data": "WorkOrderDate" },
                            { "data": "Location" },
                            { "data": "Status" },
                            {
                                "targets": -1,
                                "render": function (data, type, full) {
                                    return "<a href='#' data-id='" + full.WorkOrderNo + "' class='btn btn-sm btn-primary pick'><i class='icon-check'></i></a>";
                                }
                            }
                        ],
                        "order": [[0, "asc"]]
                    })
                }
            }
        });
    })
    $(document).on('click', '#modelHistory',function (e) {
        e.preventDefault();
        var data = $(this).data();
        var assetid = data.assetid;
        $.ajax({
            type: "POST",
            url: "/MasterAsset/GetHistoryAssetLog?assetid=" + assetid,
            contentType: "application/json",
            dataType: "json",
            success: function (response) {
                var append = "";
                var container = $("#appendContainer");
                container.empty();
                for (var i = 0; i < response.length; i++) {
                    append = append + "<tr>"
                    append = append + "<td>" + response[i].AssetCategoryCode + "</td>"
                    append = append + "<td>" + response[i].AssetType + "</td>"
                    append = append + "<td>" + response[i].DateReplacement + "</td>"
                    append = append + "<td>" + response[i].Description + "</td>"
                    append = append + "<td>" + response[i].Remarks + "</td>"
                    append = append + "<td>" + response[i].ReplacementKe + "</td>"
                    append = append + "</tr>"
                }
                container.append(append);
            }
        })
        $('#ModalhistoryAsset').modal('toggle');
    })
})
