﻿$(document).ready(function () {
    $("#RickChkSearch").click(function () {
        $('#data-table-RiskChkHeader').DataTable().destroy();
        $('#data-table-RiskChkHeader').DataTable({
            "processing": true,
            "displayStart": 0,
            "serverSide": true,
            searching: false,
            "info": true,
            
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "ajax": {
                "url": "GetChecklistHeaderGrid",
                "type": "GET",
                "data": { "Description": $("#ItemDescChecklist").val() }
            },
            "columns": [
                { "data": "ChecklistHeaderID" },
                { "data": "Description" },
                { "data": "WorkOrderDate" },
                { "data": "Status" },
                {
                    "targets": -1,
                    "render": function (data, type, full) {
                        return "<a href='ChecklistHeaderAddEdit/?id=" + full.ChecklistHeaderID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                    }
                }
            ],
            "order": [[0, "asc"]]

        })

    })
    $('#data-table-RiskChkHeader').DataTable({
        "processing": true,
        "displayStart": 0,
        "serverSide": true,
        searching: false,
        "info": true,
        
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "GetChecklistHeaderGrid",
            "type": "GET",
            "data": { "Description": $("#ItemDescChecklist").val() }
        },
        "columns": [
            { "data": "ChecklistHeaderID" },
            { "data": "Description" },
            { "data": "WorkOrderDate" },
            { "data": "Status" },
            {
                "targets": -1,
                "render": function (data, type, full) {
                    return "<a href='ChecklistHeaderAddEdit/?id=" + full.ChecklistHeaderID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                }
            }
        ],
        "order": [[0, "asc"]]

    })
    $("#RiskItemSearch").click(function () {
        $('#data-table-RiskChkDetail').DataTable().destroy();
        $('#data-table-RiskChkDetail').DataTable({
            "processing": true,
            "displayStart": 0,
            "serverSide": true,
            searching: false,
            "info": true,
            
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "ajax": {
                "url": "GetChecklistDetailGrid",
                "type": "GET",
                "data": { "Description": $("#ItemDesc").val() }
            },
            "columns": [
                { "data": "ChecklistDetailID" },
                { "data": "Description" },
                { "data": "WorkOrderDate" },
                { "data": "Status" },
                {
                    "targets": -1,
                    "render": function (data, type, full) {
                        return "<a href='ItemDetailAddEdit/?id=" + full.ChecklistDetailID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                    }
                }
            ],
            "order": [[0, "asc"]]

        })
    })
    $('#data-table-RiskChkDetail').DataTable({
        "processing": true,
        "displayStart": 0,
        "serverSide": true,
        searching: false,
        "info": true,
        
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "GetChecklistDetailGrid",
            "type": "GET",
            "data": { "CategoryAssetID": $("#CategoryAssetId").val(), "WorkOrderNo": $("#WorkOrderNo").val() }
        },
        "columns": [
            { "data": "ChecklistDetailID" },
            { "data": "Description" },
            { "data": "WorkOrderDate" },
            { "data": "Status" },
            {
                "targets": -1,
                "render": function (data, type, full) {
                    return "<a href='ItemDetailAddEdit/?id=" + full.ChecklistDetailID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                }
            }
        ],
        "order": [[0, "asc"]]

    })
    $("#RiskWorkOrderSearch").click(function () {
        $('#data-table-GroupItemRisk').DataTable().destroy();
        $('#data-table-GroupItemRisk').DataTable({
            "processing": true,
            "displayStart": 0,
            "serverSide": true,
            searching: false,
            "info": true,
            
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "ajax": {
                "url": "GetGroupItemGrid",
                "type": "GET",
                "data": { "Description": $("#ItemDesc").val() }
            },
            "columns": [
                { "data": "GroupItemID" },
                { "data": "Description" },
                { "data": "WorkOrderDate" },
                { "data": "Status" },
                {
                    "targets": -1,
                    "render": function (data, type, full) {
                        return "<a href='AddEdit/?id=" + full.GroupItemID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                    }
                }
            ],
            "order": [[0, "asc"]]

        })
    })
    $('#data-table-GroupItemRisk').DataTable({
        "processing": true,
        "displayStart": 0,
        "serverSide": true,
        searching: false,
        "info": true,
        
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "GetGroupItemGrid",
            "type": "GET",
            "data": { "Description": $("#ItemDesc").val()}
        },
        "columns": [
            { "data": "GroupItemID" },
            { "data": "Description" },
            { "data": "WorkOrderDate" },
            { "data": "Status" },
            {
                "targets": -1,
                "render": function (data, type, full) {
                    return "<a href='AddEdit/?id=" + full.GroupItemID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                }
            }
        ],
        "order": [[0, "asc"]]

    })
    $('#data-table-masterinterval').DataTable();
    $('#datatable-workorder-search').click(function () {
        $('#data-table-workorder').DataTable().destroy();
        $('#data-table-workorder').dataTable({
            /* No ordering applied by DataTables during initialisation */
            "processing": true,
            "serverSide": true,
            searching: false,
            "info": true,
            
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "ajax": {
                "url": "GetWorkOrderGrid",
                "type": "GET",
                "data": { "CategoryAssetID": $("#CategoryAssetId").val(), "WorkOrderNo": $("#WorkOrderNo").val(), "description": $("#Description").val() }
            },
            "columns": [
                { "data": "AssetCategoryID" },
                { "data": "WorkOrderNo" },
                { "data": "WorkOrderDate" },
                { "data": "Description" },
                { "data": "Status" },
                {
                    "targets": -1,
                    "render": function (data, type, full) {
                        return "<a href='EditWorkOrder/?id=" + full.WorkOrderNo + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                    }
                }
            ],
            "order": [[0, "asc"]]
        })
    })
    $('#data-table-workorder').DataTable({
        /* No ordering applied by DataTables during initialisation */
        "processing": true,
        "displayStart": 0,
        "serverSide": true,
        searching: false,
        "info": true,
        
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "GetWorkOrderGrid",
            "type": "GET",
            "data": { "CategoryAssetID": $("#CategoryAssetId").val(), "WorkOrderNo": $("#WorkOrderNo").val() }
        },
        "columns": [
            { "data": "AssetCategoryID" },
            { "data": "WorkOrderNo" },
            { "data": "WorkOrderDate" },
            { "data": "Description" },
            { "data": "Status" },
            {
                "targets": -1,
                "render": function (data, type, full) {
                    return "<a href='EditWorkOrder/?id=" + full.WorkOrderNo + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                }
            }
        ],
        "order": [[0, "asc"]]
    })
    //$('#data-table-assetcategory').dataTable({
    //    "processing": true,
    //    "serverSide": true,
    //    searching: false,

    //    "info": true,
    //    
    //    "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
    //    "ajax": {
    //        "url": "MasterAsset/GetGridAssetCategory",
    //        "type": "GET",
    //        "data": { "assetcategoryid": $("#assetcategoryid").val(), "assetdescription": $("#assetdescription").val(), "assetstatus": $("#assetstatus").val(), "assetgroupcode": $("#assetgroupcode").val() },
    //    },
    //    "columns": [
    //        { "data": "AssetGroup" },
    //        { "data": "AssetCategoryID" },
    //        { "data": "AssetCategory" },
    //        { "data": "Description" },
    //        { "data": "Location" },
    //        { "data": "InstallmentDate" },
    //        //{ "data": "Lifetime" },
    //        { "data": "Status" },
    //        {
    //            "targets": -1,
    //            "render": function (data, type, full) {
    //                return "<a href='MasterAsset/AddEditAssetCategory/?id=" + full.AssetCategoryID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
    //            }
    //        }
    //    ],
    //    "order": [[0, "asc"]]

    //});

    $("#datatable-masterassettype-search").click(function () {
        $('#data-table-assettype').DataTable().destroy();
        $('#data-table-assettype').dataTable({
            "processing": true,
            "serverSide": true,
            searching: false,

            "info": true,
            
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "ajax": {
                "url": "MasterAssetType/GetGridAssetTypeSearch",
                "type": "GET",
                "data": { "assettypeid": $("#assettypeid").val(), "assettypedescription": $("#assettypedescription").val(), 'assetcategoryid': $("#assetcategoryid").val(), 'assettypestatus': $("#assettypestatus").val() },
            },
            "columns": [
                { "data": "AssetCategoryID" },
                { "data": "AssetTypeID" },
                { "data": "Description" },
                { "data": "InstallmentDate" },
                { "data": "Lifetime" },
                { "data": "Replacement" },
                { "data": "ReplacementDate" },
                {
                    "targets": -1,
                    "render": function (data, type, full) {
                        return "<a href='MasterAssetType/AssetTypeAddEdit/?id=" + full.AssetTypeID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                    }
                }
            ],
            "order": [[0, "asc"]]

        });

    })
    $("#datatable-masterasset-search").click(function () {
        $('#data-table-assetcategory').DataTable().destroy();
        $('#data-table-assetcategory').dataTable({
            "processing": true,
            "serverSide": true,
            searching: false,

            "info": true,
            
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "ajax": {
                "url": "MasterAsset/GetGridAssetCategory",
                "type": "GET",
                "data": { "assetcategoryid": $("#assetcategoryid").val(), "assetdescription": $("#assetdescription").val(), "assetstatus": $("#assetstatus").val(), "assetgroupcode": $("#assetgroupcode").val() },
            },
            "columns": [
                { "data": "AssetGroup" },
                { "data": "AssetCategoryID" },
                { "data": "AssetCategory" },
                { "data": "Description" },
                { "data": "Location" },
                { "data": "InstallmentDate" },
                //{ "data": "Lifetime" },
                { "data": "Status" },
                {
                    "targets": -1,
                    "render": function (data, type, full) {
                        return "<a href='MasterAsset/AddEditAssetCategory/?id=" + full.AssetCategoryID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a><a href='#' id='modelHistory' data-assetid='" + full.AssetCategoryID + "' class='btn btn-sm btn-success'>History</a>";
                    }
                }
            ],
            "order": [[0, "asc"]]

        });

    })
    $('#data-table-assettype').dataTable({
        "processing": true,
        "serverSide": true,
        searching: false,

        "info": true,
        
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "MasterAssetType/GetGridAssetType",
            "type": "GET"
        },
        "columns": [
            { "data": "AssetCategoryID" },
            { "data": "AssetTypeID" },
            { "data": "Description" },
            { "data": "InstallmentDate" },
            { "data": "Lifetime" },
            { "data": "Replacement" },
            { "data": "ReplacementDate" },
            {
                "targets": -1,
                "render": function (data, type, full) {
                    return "<a href='MasterAssetType/AssetTypeAddEdit/?id=" + full.AssetTypeID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                }
            }
        ],
        "order": [[0, "asc"]]

    });
    $('#data-table-assetcategory').dataTable({
        "processing": true,
        "serverSide": true,
        searching: false,

        "info": true,
        
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "MasterAsset/GetGridAssetCategory",
            "type": "GET"
        },
        "columns": [
            { "data": "AssetGroup" },
            { "data": "AssetCategoryID" },
            { "data": "AssetCategory" },
            { "data": "Description" },
            { "data": "Location" },
            { "data": "InstallmentDate" },
            { "data": "Status" },
            {
                "targets": -1,
                "render": function (data, type, full) {
                    return "<a href='MasterAsset/AddEditAssetCategory/?id=" + full.AssetCategoryID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a><a href='#' data-assetid='" + full.AssetCategoryID + "'  id='modelHistory' class='btn btn-sm btn-success'>History</a>";
                }
            }
        ],
        "order": [[0, "asc"]]

    });
    $("#data-table-ikcategory").DataTable();
    $('#data-table-masterik').dataTable({
        "processing": true,
        "serverSide": true,
        searching: false,

        "info": true,
        
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "InstruksiKerja/GetGridIK",
            "type": "GET"
        },
        "columns": [
            { "data": "IKCode" },
            { "data": "IKDescription" },
            { "data": "Status" },
            {
                "targets": -1,
                "render": function (data, type, full) {
                    return "<a href='InstruksiKerja/InstruksiKerjaAddEdit/?id=" + full.IKCode + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a><a href='InstruksiKerja/InstruksiKerjaCategoryList/?ikCode=" + full.IKCode + "' class='btn btn-sm btn-success'><i class='icon-plus'></i></a>";
                }
            }
        ],
        "order": [[0, "asc"]]

    });
    $('#datatable-masterik-search').click(function () {
        $("#data-table-masterik").DataTable().destroy();
        $('#data-table-masterik').dataTable({
            "processing": true,
            "serverSide": true,
            searching: false,

            "info": true,
            
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "ajax": {
                "url": "InstruksiKerja/GetGridIK",
                "type": "GET",
                "data": { "ikcode": $("#ikcode").val(), "ikdescription": $("#ikdescription").val() },
            },
            "columns": [
                { "data": "IKCode" },
                { "data": "IKDescription" },
                { "data": "Status" },
                {
                    "targets": -1,
                    "render": function (data, type, full) {
                        return "<a href='InstruksiKerja/InstruksiKerjaAddEdit/?id=" + full.IKCode + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a><a href='InstruksiKerja/InstruksiKerjaCategoryList/?ikCode=" + full.IKCode + "' class='btn btn-sm btn-success'><i class='icon-plus'></i></a>";
                    }
                }
            ],
            "order": [[0, "asc"]]

        });
    })
    $('#data-table-usergroup').dataTable({
        "processing": true,
        "serverSide": true,
        searching: false,

        "info": true,
        
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "UserGroup/GetGridUserGroup",
            "type": "GET"
        },
        "columns": [
            { "data": "UserGroupId" },
            { "data": "UserGroupName" },
            { "data": "UserGroupLeader" },
            { "data": "Status" },
            {
                "targets": -1,
                "render": function (data, type, full) {
                    return "<a href='UserGroup/UserGroupAddEdit/?id=" + full.UserGroupId + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                }
            }
        ],
        "order": [[0, "asc"]]

    });
    $("#datatable-usergroup-search").click(function () {
        $('#data-table-usergroup').DataTable().destroy();
        $('#data-table-usergroup').dataTable({
            "processing": true,
            "serverSide": true,
            searching: false,

            "info": true,
            
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "ajax": {
                "url": "UserGroup/GetGridUserGroup",
                "type": "GET",
                "data": { "groupid": $("#GroupID").val(), "leader": $("#Leader").val(), "status": $("#usergroupstatus").val() }
            },
            "columns": [
                { "data": "UserGroupId" },
                { "data": "UserGroupName" },
                { "data": "UserGroupLeader" },
                { "data": "Status" },
                {
                    "targets": -1,
                    "render": function (data, type, full) {
                        return "<a href='UserGroup/UserGroupAddEdit/?id=" + full.UserGroupId + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                    }
                }
            ],
            "order": [[0, "asc"]]

        });
    })
    $('#data-table-user').dataTable({
        "processing": true,
        "serverSide": true,
        searching: false,

        "info": true,
        
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "User/GetUserGrid",
            "type": "GET"
        },
        "columns": [
            { "data": "UserID" },
            { "data": "UserName" },
            { "data": "Status" },
            {
                "targets": -1,
                "render": function (data, type, full) {
                    return "<a href='User/AddEditUser/?id=" + full.UserID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a><a href='User/DeleteUser/?id=" + full.UserID + "' class='btn btn-sm btn-danger'><i class='icon-trash'></i></a>";
                    
                }
            }
        ],
        "order": [[0, "asc"]]

    });
    $("#datatable-name-search").click(function () {
        $('#data-table-user').DataTable().destroy();
        $('#data-table-user').dataTable({
            "processing": true,
            "serverSide": true,
            searching: false,

            "info": true,
            
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "ajax": {
                "url": "User/GetUserGridSearch",
                "type": "GET",
                "data": { "UserID": $("#UserID").val(), "UserName": $("#UserName").val() }
            },
            "columns": [
                { "data": "UserID" },
                { "data": "UserName" },
                { "data": "Status" },
                {
                    "targets": -1,
                    "render": function (data, type, full) {
                        return "<a href='User/AddEditUser/?id=" + full.UserID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a><a href='User/DeleteUser/?id=" + full.UserID + "' class='btn btn-sm btn-danger'><i class='icon-trash'></i></a>";
                    }
                }
            ],
            "order": [[0, "asc"]]

        });
    })
    $("#datatable-vendor-search").click(function () {
        $('#data-table-vendor').DataTable().destroy();
        $('#data-table-vendor').dataTable({
            "processing": true,
            "serverSide": true,
            searching: false,

            "info": true,
            
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "ajax": {
                "url": "Location/GetVendorGrid",
                "type": "GET",
                "data": { "VendorName": $("#VendorName").val(), "VendorDescription": $("#VendorDescription").val() }

            },
            "columns": [
                { "data": "VendorID" },
                { "data": "VendorName" },
                { "data": "VendorDescription" },
                {
                    "targets": -1,
                    "render": function (data, type, full) {
                        return "<a href='Vendor/AddEdit/?vendorid=" + full.VendorID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a><a href='Vendor/Delete/?vendorid=" + full.VendorID + "' class='btn btn-sm btn-danger'><i class='icon-trash'></i></a>";
                    }
                }
            ],
            "order": [[0, "asc"]]
        })
    });
    $('#data-table-vendor').dataTable({
        "processing": true,
        "serverSide": true,
        searching: false,

        "info": true,
        
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "Vendor/GetVendorGrid",
            "type": "GET"
        },
        "columns": [
            { "data": "VendorID" },
            { "data": "VendorName" },
            { "data": "VendorDescription" },
            {
                "targets": -1,
                "render": function (data, type, full) {
                    return "<a href='Vendor/AddEdit/?vendorid=" + full.VendorID + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a><a href='Vendor/Delete/?vendorid=" + full.VendorID + "' class='btn btn-sm btn-danger'><i class='icon-trash'></i></a>";
                }
            }
        ],
        "order": [[0, "asc"]]

    });
    $('#data-table-location').dataTable({
        "processing": true,
        "serverSide": true,
        searching: false,

        "info": true,
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "ajax": {
            "url": "Location/GetLocationGrid",
            "type": "GET"
        },
        "columns": [
            { "data": "LocationCode" },
            { "data": "SubPortoFolio" },
            { "data": "Floor" },
            { "data": "LocationArea" },
            { "data": "Description" },
            { "data": "Status" },
            {
                "targets": -1,
                "render": function (data, type, full) {
                    return "<a href='Location/AddEditLocation/?id=" + full.LocationCode + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                }
            }
        ],
        "order": [[0, "asc"]]

    });
    $("#datatable-location-search").click(function () {
        $('#data-table-location').DataTable().destroy();
        $('#data-table-location').dataTable({
            "processing": true,
            "serverSide": true,
            searching: false,

            "info": true,
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "ajax": {
                "url": "Location/GetLocationGridSearch",
                "type": "GET",
                "data": { "spc": $("#spc").val(), "flr": $("#flr").val() }

            },
            "columns": [
                { "data": "LocationCode" },
                { "data": "SubPortoFolio" },
                { "data": "Floor" },
                { "data": "LocationArea" },
                { "data": "Description" },
                { "data": "Status" },
                {
                    "targets": -1,
                    "render": function (data, type, full) {
                        return "<a href='Location/AddEditLocation/?id=" + full.LocationCode + "' class='btn btn-sm btn-primary'><i class='icon-note'></i></a>";
                    }
                }
            ],
            "order": [[0, "asc"]]
        })
    });

}); 