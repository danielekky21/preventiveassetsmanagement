// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace PreventiveAsset.Controllers
{
    public partial class WorkFlowController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public WorkFlowController() { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected WorkFlowController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult WorkFlowRiskDetail()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.WorkFlowRiskDetail);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult WorkFlowDetail()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.WorkFlowDetail);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult ApproveOrRejectWorkFlow()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ApproveOrRejectWorkFlow);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public WorkFlowController Actions { get { return MVC.WorkFlow; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "WorkFlow";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "WorkFlow";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string Index = "Index";
            public readonly string WorkFlowRiskDetail = "WorkFlowRiskDetail";
            public readonly string WorkFlowDetail = "WorkFlowDetail";
            public readonly string ApproveOrRejectWorkFlow = "ApproveOrRejectWorkFlow";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string Index = "Index";
            public const string WorkFlowRiskDetail = "WorkFlowRiskDetail";
            public const string WorkFlowDetail = "WorkFlowDetail";
            public const string ApproveOrRejectWorkFlow = "ApproveOrRejectWorkFlow";
        }


        static readonly ActionParamsClass_WorkFlowRiskDetail s_params_WorkFlowRiskDetail = new ActionParamsClass_WorkFlowRiskDetail();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_WorkFlowRiskDetail WorkFlowRiskDetailParams { get { return s_params_WorkFlowRiskDetail; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_WorkFlowRiskDetail
        {
            public readonly string workflowid = "workflowid";
            public readonly string workorderid = "workorderid";
        }
        static readonly ActionParamsClass_WorkFlowDetail s_params_WorkFlowDetail = new ActionParamsClass_WorkFlowDetail();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_WorkFlowDetail WorkFlowDetailParams { get { return s_params_WorkFlowDetail; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_WorkFlowDetail
        {
            public readonly string workflowid = "workflowid";
            public readonly string workorderid = "workorderid";
        }
        static readonly ActionParamsClass_ApproveOrRejectWorkFlow s_params_ApproveOrRejectWorkFlow = new ActionParamsClass_ApproveOrRejectWorkFlow();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_ApproveOrRejectWorkFlow ApproveOrRejectWorkFlowParams { get { return s_params_ApproveOrRejectWorkFlow; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_ApproveOrRejectWorkFlow
        {
            public readonly string workflowid = "workflowid";
            public readonly string command = "command";
            public readonly string workorderid = "workorderid";
            public readonly string remark = "remark";
            public readonly string IKCategory = "IKCategory";
            public readonly string vendornames = "vendornames";
            public readonly string condition = "condition";
            public readonly string IKItem = "IKItem";
            public readonly string workstartdates = "workstartdates";
            public readonly string workenddates = "workenddates";
            public readonly string picremark = "picremark";
            public readonly string assettypeid = "assettypeid";
            public readonly string maintenancetype = "maintenancetype";
            public readonly string maintenancejumlah = "maintenancejumlah";
            public readonly string maintenancesatuan = "maintenancesatuan";
            public readonly string maintenanceremarks = "maintenanceremarks";
            public readonly string ikremarks = "ikremarks";
            public readonly string IKCategoryfor = "IKCategoryfor";
            public readonly string completedby = "completedby";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string Index = "Index";
                public readonly string WorkFlowDetail = "WorkFlowDetail";
                public readonly string WorkFlowRiskDetail = "WorkFlowRiskDetail";
            }
            public readonly string Index = "~/Views/WorkFlow/Index.cshtml";
            public readonly string WorkFlowDetail = "~/Views/WorkFlow/WorkFlowDetail.cshtml";
            public readonly string WorkFlowRiskDetail = "~/Views/WorkFlow/WorkFlowRiskDetail.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_WorkFlowController : PreventiveAsset.Controllers.WorkFlowController
    {
        public T4MVC_WorkFlowController() : base(Dummy.Instance) { }

        [NonAction]
        partial void IndexOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult Index()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
            IndexOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void WorkFlowRiskDetailOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string workflowid, string workorderid);

        [NonAction]
        public override System.Web.Mvc.ActionResult WorkFlowRiskDetail(string workflowid, string workorderid)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.WorkFlowRiskDetail);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "workflowid", workflowid);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "workorderid", workorderid);
            WorkFlowRiskDetailOverride(callInfo, workflowid, workorderid);
            return callInfo;
        }

        [NonAction]
        partial void WorkFlowDetailOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string workflowid, string workorderid);

        [NonAction]
        public override System.Web.Mvc.ActionResult WorkFlowDetail(string workflowid, string workorderid)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.WorkFlowDetail);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "workflowid", workflowid);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "workorderid", workorderid);
            WorkFlowDetailOverride(callInfo, workflowid, workorderid);
            return callInfo;
        }

        [NonAction]
        partial void ApproveOrRejectWorkFlowOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string workflowid, string command, string workorderid, string remark, string[] IKCategory, string[] vendornames, string[] condition, string[] IKItem, string[] workstartdates, string[] workenddates, string[] picremark, string[] assettypeid, string[] maintenancetype, string[] maintenancejumlah, string[] maintenancesatuan, string[] maintenanceremarks, string[] ikremarks, string[] IKCategoryfor, string[] completedby);

        [NonAction]
        public override System.Web.Mvc.ActionResult ApproveOrRejectWorkFlow(string workflowid, string command, string workorderid, string remark, string[] IKCategory, string[] vendornames, string[] condition, string[] IKItem, string[] workstartdates, string[] workenddates, string[] picremark, string[] assettypeid, string[] maintenancetype, string[] maintenancejumlah, string[] maintenancesatuan, string[] maintenanceremarks, string[] ikremarks, string[] IKCategoryfor, string[] completedby)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ApproveOrRejectWorkFlow);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "workflowid", workflowid);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "command", command);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "workorderid", workorderid);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "remark", remark);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "IKCategory", IKCategory);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "vendornames", vendornames);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "condition", condition);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "IKItem", IKItem);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "workstartdates", workstartdates);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "workenddates", workenddates);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "picremark", picremark);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "assettypeid", assettypeid);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "maintenancetype", maintenancetype);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "maintenancejumlah", maintenancejumlah);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "maintenancesatuan", maintenancesatuan);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "maintenanceremarks", maintenanceremarks);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "ikremarks", ikremarks);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "IKCategoryfor", IKCategoryfor);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "completedby", completedby);
            ApproveOrRejectWorkFlowOverride(callInfo, workflowid, command, workorderid, remark, IKCategory, vendornames, condition, IKItem, workstartdates, workenddates, picremark, assettypeid, maintenancetype, maintenancejumlah, maintenancesatuan, maintenanceremarks, ikremarks, IKCategoryfor, completedby);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
