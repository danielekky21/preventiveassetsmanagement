﻿using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    [SessionTimeout]
    public partial class UserController : Controller
    {
        // GET: User
        public virtual ActionResult Index()
        {
           
            return View();
        }
        public virtual JsonResult GetUserGridSearch(int draw, int start, int length, string UserID, string UserName)
        {
            int sortColumn = -1;
            Session["searchUserID"] = UserID;
            Session["searchUserName"] = UserName;
            var Data = MasterUser.GetAllUser().ToList();
            //if (Session["searchUserID"] != null)
            //{
            //    Data = Data.Where(x => x.UserID.ToLower().Contains(Session["searchUserID"].ToString().ToLower())).ToList();
            //}
            //if (Session["searchUserName"] != null)
            //{
            //    Data = Data.Where(x => x.Name.ToLower().Contains(Session["searchUserName"].ToString().ToLower())).ToList();
            //}

            if (!string.IsNullOrEmpty(UserID))
            {
                Data = Data.Where(x => x.UserID.ToLower().Contains(UserID.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(UserName))
            {
                Data = Data.Where(x => x.Name.ToLower().Contains(UserName.ToLower())).ToList();
            }
            int countTotal = Data.Count;
            if (Data.Count > 10)
            {
                Data = Data.GetRange(start, Math.Min(length, Data.Count - start));
            }

            string sortDirection = "asc";
            if (length == -1)
            {
                length = MasterUser.GetAllUser().Count();
            }
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = countTotal;
            dataTableData.ListData = AssignUserData(Data);
            int recordsFiltered = countTotal;
            return Json(new
            {
                // this is what datatables wants sending back
                draw = dataTableData.draw,
                recordsTotal = 10,
                recordsFiltered = recordsFiltered,
                data = dataTableData.ListData
            }, JsonRequestBehavior.AllowGet);

        }
        public virtual JsonResult GetUserGrid(int draw, int start, int length, string UserID, string UserName)
        {
            int sortColumn = -1;

            var Data = MasterUser.GetAllUser().ToList();
            if (Session["searchUserID"] != null)
            {
                Data = Data.Where(x => x.UserID.ToLower().Contains(Session["searchUserID"].ToString().ToLower())).ToList();
            }
            if (Session["searchUserName"] != null)
            {
                Data = Data.Where(x => x.Name.ToLower().Contains(Session["searchUserName"].ToString().ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(UserID))
            {
                Data = Data.Where(x => x.UserID.ToLower().Contains(UserID.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(UserName))
            {
                Data = Data.Where(x => x.Name.ToLower().Contains(UserName.ToLower())).ToList();
            }
            int countTotal = Data.Count;
            if (Data.Count > 10)
            {
                Data = Data.GetRange(start, Math.Min(length, Data.Count - start));
            }
            string sortDirection = "asc";
            if (length == -1)
            {
                length = MasterUser.GetAllUser().Count();
            }
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = countTotal;
            dataTableData.ListData = AssignUserData(Data);
            int recordsFiltered = countTotal;
            return Json(new
            {
                // this is what datatables wants sending back
                draw = dataTableData.draw,
                recordsTotal = 10,
                recordsFiltered = recordsFiltered,
                data = dataTableData.ListData
            }, JsonRequestBehavior.AllowGet);

        }
        private List<DataItem> AssignUserData(List<MasterUser> data)
        {
            List<DataItem> dataitem = new List<DataItem>();

            foreach (var item in data)
            {
                DataItem items = new DataItem();
                items.UserID = item.UserID;
                items.UserName = item.Name;

                if (item.IsDeleted.GetValueOrDefault())
                {
                    items.Status = "In Active";
                }
                else
                {
                    items.Status = "Active";
                }
                dataitem.Add(items);
            }
            return dataitem;
        }
        public virtual ActionResult AddEditUser(string id)
        {
            SelectList selectlist = new SelectList();
            var role = selectlist.GetRole();
            ViewBag.dept = selectlist.GetDepartementSelectList();
            if (!string.IsNullOrEmpty(id))
            {
                var currentData = MasterUser.GetUserByID(id);
                if (currentData != null)
                {
                    currentData.Password = PreventiveAssetManagement.commons.Security.DataEncription.Decrypt(currentData.Password);
                    ViewData.Model = currentData;
                }
            }
            else
            {
                ViewData.Model = new MasterUser();
            }
            ViewBag.role = role;
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEditUser(MasterUser model)
        {
            string loginID = Session["UserId"].ToString();
            if (!string.IsNullOrEmpty(model.UserID))
            {
                var currentData = MasterUser.GetUserByID(model.UserID);
                if (currentData != null)
                {
                    currentData.Password = PreventiveAssetManagement.commons.Security.DataEncription.Encrypt(model.Password);
                    currentData.Department = model.Department;
                    currentData.Role = model.Role;
                    currentData.Email = model.Email;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.UpdatedBy = loginID;
                    currentData.UpdateSave<MasterUser>();
                }
                else
                {
                    MasterUser newData = new MasterUser();
                    newData = model;
                    newData.Password = PreventiveAssetManagement.commons.Security.DataEncription.Encrypt(model.Password);
                    newData.CreatedBy = loginID;
                    newData.IsDeleted = false;
                    newData.CreatedDate = DateTime.Now;
                    newData.InsertSave<MasterUser>();
                }
            }
           
            return RedirectToAction(MVC.User.Index());
        }
        public virtual ActionResult DeleteUser(string id)
        {
            var currentData = MasterUser.GetUserByID(id);
            if (currentData != null)
            {
                currentData.IsDeleted = true;
                currentData.UpdateSave<MasterUser>();
            }
            return RedirectToAction(MVC.User.Index());
        }
        public virtual ActionResult LogOut()
        {
            Session.Abandon();
            return RedirectToAction(MVC.Login.Index());
        }

    }
}