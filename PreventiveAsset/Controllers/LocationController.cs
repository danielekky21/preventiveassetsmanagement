﻿using PreventiveAssetManagement.Models;
using PreventiveAssetManagement.dataRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    [SessionTimeout]
    public partial class LocationController : Controller
    {
        // GET: Location
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual JsonResult GetLocationGridSearch(int draw, int start, int length, string spc, string flr)
        {
            try
            {
                int sortColumn = -1;
                var Data = MasterLocation.GetAllActiveLocation().ToList();
                Session["searchSpc"] = spc;
                Session["searchFlr"] = flr;
                if (!string.IsNullOrEmpty(spc))
                {

                    Data = Data.Where(x => x.Subportofolio.ToLower().Contains(spc.ToLower())).ToList();
                }
                if (!string.IsNullOrEmpty(flr))
                {

                    Data = Data.Where(x => x.Floor.ToLower().Contains(flr.ToLower())).ToList();
                }

                int countTotal = Data.Count;
                if (Data.Count > start)
                {
                    Data = Data.GetRange(start, Math.Min(length, Data.Count - start));
                }
                else
                {
                    start = 1;
                    Data = Data.GetRange(start, Math.Min(length, Data.Count - start));

                }



                string sortDirection = "asc";
                if (length == -1)
                {
                    length = countTotal;
                }
                DataTableData dataTableData = new DataTableData();
                dataTableData.draw = draw;
                dataTableData.recordsTotal = countTotal;
                dataTableData.ListData = AssignLocationData(Data);

                int recordsFiltered = countTotal;
                return Json(new
                {
                    // this is what datatables wants sending back
                    draw = dataTableData.draw,
                    recordsTotal = 10,
                    recordsFiltered = recordsFiltered,
                    data = dataTableData.ListData
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {

                throw;
            }
        }
        public virtual JsonResult GetLocationGrid(int draw, int start, int length, string spc, string flr)
        {
            try
            {
                int sortColumn = -1;
                var Data = MasterLocation.GetAllActiveLocation().ToList();


                if (Session["searchSpc"] != null)
                {
                    Data = Data.Where(x => x.Subportofolio.ToLower().Contains(Session["searchSpc"].ToString().ToLower())).ToList();
                }
                if (Session["searchFlr"] != null)
                {
                    Data = Data.Where(x => x.Floor.ToLower().Contains(Session["searchFlr"].ToString().ToLower())).ToList();
                }
                if (!string.IsNullOrEmpty(spc))
                {
                    Data = Data.Where(x => x.Subportofolio.ToLower().Contains(spc.ToLower())).ToList();
                }
                if (!string.IsNullOrEmpty(flr))
                {
                    Data = Data.Where(x => x.Floor.ToLower().Contains(flr.ToLower())).ToList();
                }

                int countTotal = Data.Count;
                if (Data.Count > start)
                {
                    Data = Data.GetRange(start, Math.Min(length, Data.Count - start));
                }


                string sortDirection = "asc";
                if (length == -1)
                {
                    length = countTotal;
                }
                DataTableData dataTableData = new DataTableData();
                dataTableData.draw = draw;
                dataTableData.recordsTotal = countTotal;
                dataTableData.ListData = AssignLocationData(Data);

                int recordsFiltered = countTotal;
                return Json(new
                {
                    // this is what datatables wants sending back
                    draw = dataTableData.draw,
                    recordsTotal = 10,
                    recordsFiltered = recordsFiltered,
                    data = dataTableData.ListData
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {

                throw;
            }

        }
        private List<DataItem> AssignLocationData(List<MasterLocation> data)
        {
            List<DataItem> dataitem = new List<DataItem>();

            foreach (var item in data)
            {
                DataItem items = new DataItem();
                items.LocationCode = item.LocationCode;
                items.Floor = item.Floor;
                items.LocationArea = item.LocationArea;
                items.Description = item.Description;
                items.SubPortoFolio = item.Subportofolio;
                if (item.IsActive.GetValueOrDefault())
                {
                    items.Status = "Active";
                }
                else
                {
                    items.Status = "In Active";
                }
                dataitem.Add(items);
            }
            return dataitem;
        }
        public virtual ActionResult AddEditLocation(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var currentData = MasterLocation.GetLocationByID(id);
                if (currentData != null)
                {
                    ViewData.Model = currentData;
                }
            }
            else
            {
                ViewData.Model = new MasterLocation();
            }
            SelectList select = new SelectList();
            ViewBag.activestate = select.GetActiveSelectList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEditLocation(MasterLocation model)
        {
            string loginID = Session["UserId"].ToString();
            var current = MasterLocation.GetLocationByID(model.LocationCode);
            if (current != null)
            {
                current.Subportofolio = model.Subportofolio;
                current.Floor = model.Floor;
                current.LocationArea = model.LocationArea;
                current.Description = model.Description;
                current.IsActive = model.IsActive;
                current.UpdatedBy = loginID;
                current.UpdatedDate = DateTime.Now;
                current.UpdateSave<MasterLocation>();
            }
            else
            {
                model.LocationCode = model.Subportofolio + "-" + model.Floor + "-" + model.LocationArea;
                model.CreatedBy = loginID;
                model.CreatedDate = DateTime.Now;
                model.InsertSave<MasterLocation>();
            }
            return RedirectToAction(MVC.Location.Index());
        }
        public virtual ActionResult BulkInsert()
        {
            for (int i = 0; i < 50; i++)
            {
                MasterLocation model = new MasterLocation();
                string loginID = Session["UserId"].ToString();
                model.Floor = "P02";
                model.LocationArea = "WRH";
                model.Subportofolio = "SPC";
                model.LocationCode = model.Subportofolio + "-" + model.Floor + "-" + model.LocationArea + "-" + i;
                model.CreatedBy = loginID;
                model.CreatedDate = DateTime.Now;
                model.InsertSave<MasterLocation>();
            }
            return RedirectToAction(MVC.Location.Index());
        }
    }
}