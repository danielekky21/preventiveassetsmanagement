﻿using PreventiveAssetManagement.commons;
using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    public partial class LoginController : Controller
    {
        // GET: Login
        public virtual ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult Index(LoginModel model)
        {
            if (!ValidateLogOn(model.Username, model.Password))
            {
                return View();
            }
            else
            {
                string pwd = PreventiveAssetManagement.commons.Security.DataEncription.Encrypt(model.Password);
                MasterUser _User = MasterUser.GetUserByUserNameAndPassword(model.Username, pwd);
                if (_User != null)
                {
                    CreateAuthenticationCookie(_User);
                    Session["UserId"] = _User.UserID;
                    Session["User"] = _User;
                    Session["GroupID"] = UserGroupHeader.GetGroupIDbyUserID(_User.UserID);
                    Session["GroupIDs"] = UserGroupHeader.GetAllGroupIDbyUserID(_User.UserID).ToList();
                    Session["WfTotal"] = WorkFlow.CountWorkFlow(_User.UserID);
                    Session["Role"] = _User.Role;
                    Session["Dept"] = _User.Department;
                    //var defaultMenu = data.MsUserMenu.GetDefaultPage(_User.UserID);
                    //if (defaultMenu.MenuLink != null)
                    //{
                    //    return RedirectToAction(defaultMenu.MenuAction, defaultMenu.MenuLink, new { id = defaultMenu.MenuId });
                    //}
                    //else
                    //{
                    //    return RedirectToAction(MVC.Dashboard.Index());
                    //}
                    if (_User.Role == "Risk")
                    {
                        return RedirectToAction(MVC.Risk.Todo.Index());
                    }
                    else
                    {
                        return RedirectToAction(MVC.Dashboard.Index());
                    }

                }
                else
                {
                    ViewBag.Error = "Username atau password salah";
                }
            }
            return View();
        }
        private bool ValidateLogOn(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName))
            {
                ViewBag.ErrorUsername = "Username Cannot Be Empty";
            }

            if (String.IsNullOrEmpty(password))
            {
                @ViewBag.ErrorPassword = "Password Cannot Be Empty";
            }

            return ModelState.IsValid;
        }
        private void CreateAuthenticationCookie(MasterUser _User)
        {
            HttpCookie myCookie = new HttpCookie("PlazaindonesiaManagementCookie");
            if (!SiteSetting.Domain.Contains("localhost"))
            {
                myCookie.Domain = "." + SiteSetting.Domain + ".com";
            }

            myCookie["user"] = _User.UserID.ToString();
            myCookie.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Add(myCookie);
        }
    }
}