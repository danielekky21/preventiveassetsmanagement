﻿using PreventiveAssetManagement.dataRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    [SessionTimeout]
    public partial class IntervalController : Controller
    {
        // GET: Interval
        public virtual ActionResult Index()
        {
            ViewData.Model = Interval.GetAllActiveMasterInterval().OrderByDescending(x => x.CreatedDate).ToList();
            return View();
        }
        public virtual ActionResult AddEditInterval(int id)
        {
            if (id != 0)
            {
                var currentData = Interval.GetMasterIntervalByID(id);
                if (currentData != null)
                {
                    ViewData.Model = currentData;
                }

            }
            else
            {
                MasterInterval interval = new MasterInterval();
                interval.IsEnabled = true;
                ViewData.Model = interval;
            }
            SelectList seleclist = new SelectList();
            ViewBag.intervalgroups = seleclist.GetIntervalGroupSelectlist();
            ViewBag.intervaltypes = seleclist.GetIntervalTypeSeleclist();
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEditInterval(MasterInterval model)
        {
            if (model.IntervalID != 0)
            {
                var currentData = Interval.GetMasterIntervalByID(model.IntervalID);
                if (currentData != null)
                {
                    currentData.IntervalType = model.IntervalType;
                    currentData.IntervalValue = model.IntervalValue;
                    currentData.IntervalGroup = model.IntervalGroup;
                    currentData.IntervalCode = model.IntervalCode;
                    currentData.Description = model.Description;
                    currentData.IsEnabled = model.IsEnabled;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.UpdadatedBy = Session["UserId"].ToString();
                    currentData.UpdateSave<MasterInterval>();
                }

            }
            else
            {
                var newData = new MasterInterval();
                newData = model;
                newData.CreatedBy = Session["UserId"].ToString();
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = true;
                newData.InsertSave<MasterInterval>();
            }

            return RedirectToAction(MVC.Interval.Index());

        }

    }
}