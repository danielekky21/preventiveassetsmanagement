﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using ClosedXML;
using ClosedXML.Excel;
using System.Data;
using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.dataRepo.Managed;

namespace PreventiveAsset.Controllers
{
    [SessionTimeout]
    public partial class ReportController : Controller
    {
        // GET: Report
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult AssetTypeAgingReport()
        {
            SelectList selectlist = new SelectList();
            ViewBag.Group = selectlist.GetAssetGroupSelectList();
            string dept = Session["Dept"].ToString();
            ViewBag.Category = selectlist.GetAssetCategoryBydept(dept);
            return View();
        }
        [HttpPost]
        public virtual ActionResult AssetTypeAgingReport(DateTime? from, DateTime? to, string assetgroup, string assetcategory,string GeneralCode)
        {
            string folderPath = "C:\\Excel\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                var dept = Session["Dept"].ToString();
                var data = MasterAssetType.GetAllAssetType().Where(x => x.IsActive == true && x.MasterAssetCategory.Departement == dept);
                if (!String.IsNullOrEmpty(assetgroup))
                {
                    data = data.Where(x => x.MasterAssetCategory.AssetGroup == assetgroup);
                }
                if (!String.IsNullOrEmpty(assetcategory))
                {
                    data = data.Where(x => x.MasterAssetCategory.AssetCategory == assetcategory);
                }
                if (!String.IsNullOrEmpty(GeneralCode))
                {
                    data = data.Where(x => x.MasterAssetCategory.AssetCategoryID.ToLower().Contains(GeneralCode.ToLower()));
                }
                if (from != null)
                {
                    data = data.Where(x => x.CreatedDate >= from);
                }
                if (to != null)
                {
                    data = data.Where(x => x.CreatedDate <= to);
                }
                var ss = wb.Worksheets.Add("Data");
                List<AssetTypeAgingReport> reportModel = new List<AssetTypeAgingReport>();

                foreach (var item in data.OrderBy(x => x.AssetCategoryID))
                {

                    var date = DateTime.Now;
                    var age = DateTime.Now.Year;
                    var maintenanceDate = MasterAssetType.GetMaintenanceDateByCode(item.MasterAssetTypeID);
                    if (maintenanceDate != null)
                    {
                        //
                        if (maintenanceDate.maintenanceDate != null)
                        {
                            age = age - maintenanceDate.maintenanceDate.Value.Year;
                        }
                        else
                        {
                            age = age - item.InstallmentDate.Value.Year;
                        }
                    }
                    else
                    {
                        age = age - item.InstallmentDate.Value.Year;
                    }
                    AssetTypeAgingReport report = new AssetTypeAgingReport();
                    report.AssetCategoryCode = item.AssetCategoryID;
                    report.AssetTypeCode = item.MasterAssetTypeID;
                    report.Description = item.Description;
                    report.InstallmentDate = item.InstallmentDate.ToString();
                    report.Lifetime = item.Lifetime.ToString();
                    report.ReplacementKe = item.Replacement.ToString();
                    report.ReplacementDate = item.ReplacementDate.ToString();
                    report.Age = age.ToString();


                    reportModel.Add(report);
                }
              
                ss.Cell("D1").Value = "Asset Type Aging Report";
                ss.Cell("D2").Value = "PT. PLAZA INDONESIA REALTY, TBK";
                ss.Cell("D3").Value = "Periode Date: From:" + from + "To : " + to;
                ss.Cell("D4").Value = "Asset Group: " + assetgroup;
                ss.Cell("D5").Value = "Asset Category: " + assetcategory;

                if (reportModel.Count() == 0)
                {
                    var tableWithStrings = ss.Cell("A11").Value = "Empty Data";
                }
                else
                {
                    var tableWithStrings = ss.Cell("A11").InsertTable(reportModel);
                }
                var ws = wb.Worksheet("Data");
                var path = Server.MapPath(@"~/Assets/img/Logo PI-Lama-01.png");
                var image = ws.AddPicture(path);

                image.MoveTo(ws.Cell(1, 1).Address);

                // optional: resize picture
                //wb.SaveAs(folderPath + "DataGridViewExport.xlsx");
                string myName = Server.UrlEncode("Asset Type Aging Report" + "_" +
                DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx");
                MemoryStream stream = GetStream(wb);// The method is defined below
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition",
                "attachment; filename=" + myName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            return RedirectToAction(MVC.Report.AssetTypeAgingReport());
        }
        public virtual ActionResult MainAssetAgingReport()
        {
            SelectList selectlist = new SelectList();
            ViewBag.Group = selectlist.GetAssetGroupSelectList();
            string dept = Session["Dept"].ToString();
            ViewBag.Category = selectlist.GetAssetCategoryBydept(dept);
            return View();
        }
        [HttpPost]
        public virtual ActionResult MainAssetAgingReport(DateTime? from,DateTime? to,string assetgroup,string assetcategory,string GeneralCode)
        {
            string folderPath = "C:\\Excel\\";
            string dept = Session["Dept"].ToString();
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
     
                var data = MasterAssetCategory.GetAllActiveMasterAssetCategory().Where(x => x.Departement == dept);
                if (!String.IsNullOrEmpty(assetgroup))
                {
                    data = data.Where(x => x.AssetGroup == assetgroup);
                }
                if (!String.IsNullOrEmpty(assetcategory))
                {
                    data = data.Where(x => x.AssetCategory == assetcategory);
                }
                if (!String.IsNullOrEmpty(GeneralCode))
                {
                    data = data.Where(x => x.AssetCategory.ToLower().Contains(GeneralCode.ToLower()));
                }
                if (from != null)
                {
                    data = data.Where(x => x.CreatedDate >= from);
                }
                if (to != null)
                {
                    data = data.Where(x => x.CreatedDate <= to);
                }
                var ss = wb.Worksheets.Add("Data");
                List<MainAssetAgingReportModel> reportModel = new List<MainAssetAgingReportModel>();
           
                foreach (var item in data)
                {
                    
                    var date = DateTime.Now;
                    var age = DateTime.Now.Year - item.InstalmentDate.Value.Year;
                    MainAssetAgingReportModel report = new MainAssetAgingReportModel();
                    report.AssetCategoryCode = item.AssetCategoryID;
                    report.AssetGroup = item.AssetGroup;
                    report.AssetCategory = item.AssetCategory;
                    report.Description = item.Description;
                    report.InstallmentDate = item.InstalmentDate.ToString();
                    report.Lifetime = item.Lifetime.ToString();
                    report.Age = age.ToString();
                  

                    reportModel.Add(report);
                }
                ss.Cell("D1").Value = "Main Asset Aging Report";
                ss.Cell("D2").Value = "PT. PLAZA INDONESIA REALTY, TBK";
                ss.Cell("D3").Value = "Periode Date: From:" + from + "To : " + to;
                ss.Cell("D4").Value = "Asset Group: " + assetgroup;
                ss.Cell("D5").Value = "Asset Group: " + assetgroup;
                ss.Cell("D6").Value = "Asset Category: " + assetcategory;
                if (reportModel.Count() == 0)
                {
                    var tableWithStrings = ss.Cell("A11").Value = "Empty Data";
                }
                else
                {
                    var tableWithStrings = ss.Cell("A11").InsertTable(reportModel);
                }
                var ws = wb.Worksheet("Data");
                var path = Server.MapPath(@"~/Assets/img/Logo PI-Lama-01.png");
                var image = ws.AddPicture(path);

                image.MoveTo(ws.Cell(1, 1).Address);

                // optional: resize picture
                //wb.SaveAs(folderPath + "DataGridViewExport.xlsx");
                string myName = Server.UrlEncode("Main Asset Aging Report" + "_" +
                DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx");
                MemoryStream stream = GetStream(wb);// The method is defined below
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition",
                "attachment; filename=" + myName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            return RedirectToAction(MVC.Report.MainAssetAgingReport());
        }
        public virtual ActionResult RepairMaintenanceAssetReport()
        {
            SelectList selectlist = new SelectList();
            string dept = Session["Dept"].ToString();
            ViewBag.Category = selectlist.GetAssetCategoryBydept(dept);
            return View();
        }
        [HttpPost]
        public virtual ActionResult RepairMaintenanceAssetReport(DateTime? from,DateTime? to,string maintenanceType,string assetcategorytype,string assetcategorycode)
        {
            string folderPath = "C:\\Excel\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                string dept = Session["Dept"].ToString();
                var data = new List<WorkOrderManualMaintenance>();
                var grouplist = (List<UserGroupDetail>)Session["GroupIDs"];
                foreach (var item in grouplist)
                {
                    var workOrder = WorkOrderManual.GetAllMaintenances().Where(x => x.WorkOrderManual.MasterAssetCategory.UserGroup == item.UserGroupID && x.WorkOrderManual.Status == "Approved" && x.WorkOrderManual.MasterAssetCategory.Departement == dept).ToList();
                    data.AddRange(workOrder);
                }
                if (!String.IsNullOrEmpty(maintenanceType))
                {
                    data = data.Where(x => x.maintenanceType == maintenanceType).ToList();
                }
                if (!String.IsNullOrEmpty(assetcategorycode))
                {
                    data = data.Where(x => x.WorkOrderManual.MasterAssetCategory.AssetCategoryID == assetcategorycode).ToList();
                }
                if (!String.IsNullOrEmpty(assetcategorytype))
                {
                    data = data.Where(x => x.AssetTypeID == assetcategorytype).ToList();
                }
                if (from != null)
                {
                    data = data.Where(x => x.WorkOrderManual.WorkOrderDate >= from).ToList();
                }
                if (to != null)
                {
                    data = data.Where(x => x.WorkOrderManual.WorkOrderDate <= to).ToList();
                }
                var ss = wb.Worksheets.Add("Data");
                List<RepairMaintenanceAssetReport> reportModel = new List<RepairMaintenanceAssetReport>();
                RepairMaintenanceAssetSummary woSum = new RepairMaintenanceAssetSummary();
                woSum.Replace = data.Where(x => x.maintenanceType == "Replace").Count().ToString();
                woSum.Service = data.Where(x => x.maintenanceType == "Service").Count().ToString();
                foreach (var item in data)
                {
                    var AssetCategoryCode = CurrentDataContext.CurrentContext.MasterAssetTypes.FirstOrDefault(x => x.MasterAssetTypeID == item.AssetTypeID);
                    var date = DateTime.Now;
                 
                    RepairMaintenanceAssetReport report = new RepairMaintenanceAssetReport();
                    report.AssetCategoryCode = AssetCategoryCode.AssetCategoryID;
                    report.AssetTypeCode = item.AssetTypeID;
                    report.AssetTypeDesc = AssetCategoryCode.Description;
                    report.WorkOrderDate = item.WorkOrderManual.WorkOrderDate.ToString();
                    report.WoNo = item.WorkOrderManualID;
                    report.MaintenanceType = item.maintenanceType;
                    report.Jumlah = item.Jumlah;
                    report.satuan = item.Satuan;
                    report.Remarks = item.Remarks;
                    reportModel.Add(report);
                }
                ss.Cell("D1").Value = "Repair Maintenance Asset Report";
                ss.Cell("D2").Value = "PT. PLAZA INDONESIA REALTY, TBK";
                ss.Cell("D3").Value = "Maintenance Type: " + maintenanceType;
                ss.Cell("D4").Value = "Asset Category Code: " + assetcategorycode;
                ss.Cell("D5").Value = "Asset Type Code: " + assetcategorytype;
                ss.Cell("D6").Value = "Work order Date from : " + from + " to : " + to;
                ss.Cell("A7").Value = "Summary";
                ss.Cell("A8").Value = "Replace";
                ss.Cell("B8").Value = woSum.Replace;
                ss.Cell("A9").Value = "Service";
                ss.Cell("B9").Value = woSum.Service;
                if (reportModel.Count() == 0)
                {
                    var tableWithStrings = ss.Cell("A11").Value = "Empty Data";
                }
                else
                {
                    var tableWithStrings = ss.Cell("A11").InsertTable(reportModel);
                }
                var ws = wb.Worksheet("Data");
                var path = Server.MapPath(@"~/Assets/img/Logo PI-Lama-01.png");
                var image = ws.AddPicture(path);

                image.MoveTo(ws.Cell(1, 1).Address);

                // optional: resize picture
                //wb.SaveAs(folderPath + "DataGridViewExport.xlsx");
                string myName = Server.UrlEncode("Maintenance Asset Report" + "_" +
                DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx");
                MemoryStream stream = GetStream(wb);// The method is defined below
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition",
                "attachment; filename=" + myName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            return RedirectToAction(MVC.Report.RepairMaintenanceAssetReport());
        }
        public virtual ActionResult WorkOrderOFTReport()
        {
            SelectList selectlist = new SelectList();
            string dept = Session["Dept"].ToString();
            ViewBag.Category = selectlist.GetAssetCategoryBydept(dept);
            ViewBag.Interval = selectlist.GetMasterIntervalDesc();
            return View();
        }
        [HttpPost]
        public virtual ActionResult WorkOrderOFTReport(DateTime? from, DateTime? to, string assetcategorycode, string interval,string GeneralCode)
        {
            string folderPath = "C:\\Excel\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //XLWorkbook wb = new XLWorkbook();
            //
            using (XLWorkbook wb = new XLWorkbook())
            {
                var dept = Session["Dept"].ToString();
                var data = WorkOrderManual.GetAllWorkOrder().Where(x => x.MasterAssetCategory.Departement == dept);
                if (!String.IsNullOrEmpty(interval))
                {

                    data = data.Join(CurrentDataContext.CurrentContext.Intervals,
                           (wo => wo.CategoryAssetID),
                           (it => it.AssetCategoryID),
                           ((wo, it) => new { WorkOrderManual = wo, Interval = it })).Where(x => x.WorkOrderManual.CategoryAssetID == x.Interval.AssetCategoryID && x.Interval.Interval1 == interval).Select(x => x.WorkOrderManual);
                }
                if (!String.IsNullOrEmpty(assetcategorycode))
                {
                    data = data.Where(x => x.CategoryAssetID == assetcategorycode);
                }
                if (!String.IsNullOrEmpty(GeneralCode))
                {
                    data = data.Where(x => x.MasterAssetCategory.AssetCategoryID.ToLower().Contains(GeneralCode.ToLower()));
                }
                if (from != null)
                {
                    data = data.Where(x => x.WorkOrderDate >= from);
                }
                if (to != null)
                {
                    data = data.Where(x => x.WorkOrderDate <= to);
                }
                var ss = wb.Worksheets.Add("Data");
                List<WorkOrderOFTReportModel> reportModel = new List<WorkOrderOFTReportModel>();
                WorkOrderReportSummary woSum = new WorkOrderReportSummary();
                woSum.TotalWO = data.Count().ToString();
                woSum.Open = data.Where(x => x.Status == "Open").Count().ToString();
                woSum.Submitted = data.Where(x => x.Status == "Submit").Count().ToString();
                woSum.Approved = data.Where(x => x.Status == "Approved").Count().ToString();
                woSum.Rejected = data.Where(x => x.Status == "Reject").Count().ToString();
                foreach (var item in data.OrderBy(x => x.WorkOrderDate).ToList())
                {
                    var Approval = ApprovalGroup.GetAllApprovalGroupByGroupID(item.MasterAssetCategory.UserGroup);
                    var combineApproval = string.Empty;
                    foreach (var app in Approval)
                    {
                        var username = MasterUser.GetUserByID(app.ApprovalId);
                        if (combineApproval == string.Empty)
                        {
                            combineApproval = username.Name;
                        }
                        else
                        {
                            combineApproval = combineApproval + "," + username.Name;
                        }

                    }
                    var date = DateTime.Now;
                    var intervalID = item.MasterAssetCategory.Intervals.FirstOrDefault(x => x.AssetCategoryID == item.MasterAssetCategory.AssetCategoryID).Interval1;
                    var conditionVendor = item.IKConditionVendors;
                    var intervalDesc = Interval.GetMasterIntervalByCode(intervalID);
                    var submittedBy = WorkFlow.GetWorkFlowByWorkOrderID(item.WorkOderManualID);
                    WorkOrderOFTReportModel report = new WorkOrderOFTReportModel();
                    report.WorkOrderDate = item.WorkOrderDate.ToString();
                    report.WONo = item.WorkOderManualID;
                    if (conditionVendor.Count > 0)
                    {
                        if (conditionVendor.ToList().FirstOrDefault().WorkStartDate != null)
                        {
                            report.WorkStartDate = conditionVendor.ToList().FirstOrDefault().WorkStartDate.ToString();
                        }
                        if (conditionVendor.ToList().FirstOrDefault().WorkEndDate != null)
                        {
                            report.WorkEndDate = conditionVendor.ToList().FirstOrDefault().WorkEndDate.ToString();
                        }
                    }
                    else
                    {
                        report.WorkStartDate = string.Empty;
                        report.WorkEndDate = string.Empty;
                    }
                    report.AssetCategoryCode = item.MasterAssetCategory.AssetCategoryID;
                    report.AssetcategoryDesc = item.MasterAssetCategory.Description;
                    report.AssetLocationCode = item.MasterAssetCategory.Location;
                    report.IntervalDescription = intervalDesc.Description;

                    report.Leader = combineApproval;
                    report.Status = item.Status;
                    if (submittedBy != null)
                    {
                        if (submittedBy.CreatedBy != null)
                        {
                            var name = MasterUser.GetUserByID(submittedBy.CreatedBy);
                            report.SubmittedBy = name.Name;
                        }
                        else
                        {
                            report.SubmittedBy = "";
                        }
                        if (submittedBy.CreatedDate != null)
                        {
                            report.SubmittedDate = submittedBy.CreatedDate.ToString();
                        }
                        else
                        {
                            report.SubmittedDate = "";
                        }
                        if (submittedBy.ApprovedTime != null)
                        {
                            report.ApprovedDate = submittedBy.ApprovedTime.ToString();
                        }
                        else
                        {
                            report.ApprovedDate = "";
                        }
                        if (report.ApprovedBy != null)
                        {
                            var approve = MasterUser.GetUserByID(report.ApprovedBy);
                            report.ApprovedBy = approve.Name;
                        }
                        else
                        {
                            report.ApprovedBy = "";
                        }
                        if (submittedBy.Remark != null)
                        {
                            report.Memo = submittedBy.Remark;
                        }
                        else
                        {
                            report.Memo = "";
                        }
                        if (submittedBy.UpdatedBy != null)
                        {
                            var approved = MasterUser.GetUserByID(submittedBy.UpdatedBy);
                            report.ApprovedBy = approved.Name;
                        }
                        else
                        {
                            report.ApprovedBy = "";
                        }
                    }


                    reportModel.Add(report);
                }

                ss.Cell("D1").Value = "Work Order Report";
                ss.Cell("D2").Value = "PT. PLAZA INDONESIA REALTY, TBK";
                ss.Cell("D3").Value = "Asset Category Code: " + assetcategorycode;
                ss.Cell("D4").Value = "Asset Type Code: ";
                ss.Cell("D5").Value = "Work Order Date from : " + from + " to : " + to;
                ss.Cell("A7").Value = "Summary";
                ss.Cell("A8").Value = "Total WO";
                ss.Cell("B8").Value = woSum.TotalWO;
                ss.Cell("A9").Value = "Open";
                ss.Cell("B9").Value = woSum.Open;
                ss.Cell("A10").Value = "Submitted";
                ss.Cell("B10").Value = woSum.Submitted;
                ss.Cell("A11").Value = "Approved";
                ss.Cell("B11").Value = woSum.Approved;
                ss.Cell("A12").Value = "Reject";
                ss.Cell("B12").Value = woSum.Rejected;
                if (reportModel.Count() == 0)
                {
                    var tableWithStrings = ss.Cell("A14").Value = "Empty Data";
                }
                else
                {
                    var tableWithStrings = ss.Cell("A14").InsertTable(reportModel);
                }
                var ws = wb.Worksheet("Data");
                var path = Server.MapPath(@"~/Assets/img/Logo PI-Lama-01.png");
                var image = ws.AddPicture(path);

                image.MoveTo(ws.Cell(1, 1).Address);

                // optional: resize picture
                //wb.SaveAs(folderPath + "DataGridViewExport.xlsx");
                string myName = Server.UrlEncode("WorkOrder" + "_" +
                DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx");
                MemoryStream stream = GetStream(wb);// The method is defined below
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition",
                "attachment; filename=" + myName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            return RedirectToAction(MVC.Report.WorkOrderReport());
        }
        public virtual ActionResult WorkOrderReport()
        {
            SelectList selectlist = new SelectList();
            string dept = Session["Dept"].ToString();
            ViewBag.Category = selectlist.GetAssetCategoryBydept(dept);
            ViewBag.Interval = selectlist.GetMasterIntervalDesc();
            return View();
        }
        [HttpPost]
        public virtual ActionResult WorkOrderReport(DateTime? from, DateTime? to, string assetcategorycode, string interval,string GeneralCode)
        {
            string folderPath = "C:\\Excel\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //XLWorkbook wb = new XLWorkbook();
            //
            using (XLWorkbook wb = new XLWorkbook())
            {
                var dept = Session["Dept"].ToString();
                var data = WorkOrderManual.GetAllWorkOrder().Where(x => x.MasterAssetCategory.Departement == dept);
                if (!String.IsNullOrEmpty(interval))
                {

                    data = data.Join(CurrentDataContext.CurrentContext.Intervals,
                           (wo => wo.CategoryAssetID),
                           (it => it.AssetCategoryID),
                           ((wo, it) => new { WorkOrderManual = wo, Interval = it })).Where(x => x.WorkOrderManual.CategoryAssetID == x.Interval.AssetCategoryID && x.Interval.Interval1 == interval).Select(x => x.WorkOrderManual);
                }
                if (!String.IsNullOrEmpty(assetcategorycode))
                {
                    data = data.Where(x => x.CategoryAssetID == assetcategorycode);
                }
                if (!string.IsNullOrEmpty(GeneralCode))
                {
                    data = data.Where(x => x.CategoryAssetID.ToLower().Contains(GeneralCode.ToLower()));
                }
                if (from != null)
                {
                    data = data.Where(x => x.WorkOrderDate >= from);
                }
                if (to != null)
                {
                    data = data.Where(x => x.WorkOrderDate <= to);
                }
                var ss = wb.Worksheets.Add("Data");
                List<WorkOrderReportModel> reportModel = new List<WorkOrderReportModel>();
                WorkOrderReportSummary woSum = new WorkOrderReportSummary();
                woSum.TotalWO = data.Count().ToString();
                woSum.Open = data.Where(x => x.Status == "Open").Count().ToString();
                woSum.Submitted = data.Where(x => x.Status == "Submit").Count().ToString();
                woSum.Approved = data.Where(x => x.Status == "Approved").Count().ToString();
                woSum.Rejected = data.Where(x => x.Status == "Reject").Count().ToString();
                foreach (var item in data.OrderBy(x => x.WorkOrderDate).ToList())
                {
                    var Approval = ApprovalGroup.GetAllApprovalGroupByGroupID(item.MasterAssetCategory.UserGroup);
                    var combineApproval = string.Empty;
                    foreach (var app in Approval)
                    {
                        var username = MasterUser.GetUserByID(app.ApprovalId);
                        if (combineApproval == string.Empty)
                        {
                            combineApproval = username.Name;
                        }
                        else
                        {
                            combineApproval = combineApproval + "," + username.Name;
                        }
                      
                    }
                    var date = DateTime.Now;
                    var intervalID = item.MasterAssetCategory.Intervals.FirstOrDefault(x => x.AssetCategoryID == item.MasterAssetCategory.AssetCategoryID).Interval1;
                    var intervalDesc = Interval.GetMasterIntervalByCode(intervalID);
                    var submittedBy = WorkFlow.GetWorkFlowByWorkOrderID(item.WorkOderManualID);
                    WorkOrderReportModel report = new WorkOrderReportModel();
                    report.WorkOrderDate = item.WorkOrderDate.ToString();
                    report.WONo = item.WorkOderManualID;
                    report.AssetCategoryCode = item.MasterAssetCategory.AssetCategoryID;
                    report.AssetcategoryDesc = item.MasterAssetCategory.Description;
                    report.AssetLocationCode = item.MasterAssetCategory.Location;
                    report.IntervalDescription = intervalDesc.Description;
                   
                    report.Leader = combineApproval;
                    report.Status = item.Status;
                    if (submittedBy != null)
                    {
                        if (submittedBy.CreatedBy != null)
                        {
                            var name = MasterUser.GetUserByID(submittedBy.CreatedBy);
                            report.SubmittedBy = name.Name;
                        }
                        else
                        {
                            report.SubmittedBy = "";
                        }
                        if (submittedBy.CreatedDate != null)
                        {
                            report.SubmittedDate = submittedBy.CreatedDate.ToString();
                        }
                        else
                        {
                            report.SubmittedDate = "";
                        }
                        if (submittedBy.ApprovedTime != null)
                        {
                            report.ApprovedDate = submittedBy.ApprovedTime.ToString();
                        }
                        else
                        {
                            report.ApprovedDate = "";
                        }
                        if (report.ApprovedBy != null)
                        {
                            var approve = MasterUser.GetUserByID(report.ApprovedBy);
                            report.ApprovedBy = approve.Name;
                        }
                        else
                        {
                            report.ApprovedBy = "";
                        }
                        if (submittedBy.Remark != null)
                        {
                            report.Memo = submittedBy.Remark;
                        }
                        else
                        {
                            report.Memo = "";
                        }
                        if (submittedBy.UpdatedBy != null)
                        {
                            var approved = MasterUser.GetUserByID(submittedBy.UpdatedBy);
                            report.ApprovedBy = approved.Name;
                        }
                        else
                        {
                            report.ApprovedBy = "";
                        }
                    }
                   

                    reportModel.Add(report);
                }

                ss.Cell("D1").Value = "Work Order Report";
                ss.Cell("D2").Value = "PT. PLAZA INDONESIA REALTY, TBK";
                ss.Cell("D3").Value = "Asset Category Code: " + assetcategorycode;
                ss.Cell("D4").Value = "Asset Type Code: ";
                ss.Cell("D5").Value = "Installment Date from : " + from + " to : " + to;
                ss.Cell("A7").Value = "Summary";
                ss.Cell("A8").Value = "Total WO";
                ss.Cell("B8").Value = woSum.TotalWO;
                ss.Cell("A9").Value = "Open";
                ss.Cell("B9").Value = woSum.Open;
                ss.Cell("A10").Value = "Submitted";
                ss.Cell("B10").Value = woSum.Submitted;
                ss.Cell("A11").Value = "Approved";
                ss.Cell("B11").Value = woSum.Approved;
                ss.Cell("A12").Value = "Reject";
                ss.Cell("B12").Value = woSum.Rejected;
                if (reportModel.Count() == 0)
                {
                    var tableWithStrings = ss.Cell("A14").Value = "Empty Data";
                }
                else
                {
                    var tableWithStrings = ss.Cell("A14").InsertTable(reportModel);
                }
                var ws = wb.Worksheet("Data");
                var path = Server.MapPath(@"~/Assets/img/Logo PI-Lama-01.png");
                var image = ws.AddPicture(path);

                image.MoveTo(ws.Cell(1, 1).Address);

                // optional: resize picture
                //wb.SaveAs(folderPath + "DataGridViewExport.xlsx");
                string myName = Server.UrlEncode("WorkOrder" + "_" +
                DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx");
                MemoryStream stream = GetStream(wb);// The method is defined below
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition",
                "attachment; filename=" + myName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            return RedirectToAction(MVC.Report.WorkOrderReport());
        }
        public virtual ActionResult AssetTypeReport()
        {
            SelectList selectlist = new SelectList();

            string dept = Session["Dept"].ToString();
            ViewBag.Category = selectlist.GetAssetCategoryBydept(dept);

            return View();
        }
        [HttpPost]
        public virtual ActionResult AssetTypeReport(DateTime? from, DateTime? to,DateTime? replacementdate,string assetcategorycode,string assettypecode)
        {
            string folderPath = "C:\\Excel\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //XLWorkbook wb = new XLWorkbook();
            //
            using (XLWorkbook wb = new XLWorkbook())
            {
                var dept = Session["Dept"].ToString();
                var data = MasterAssetType.GetAllAssetType().Where(x => x.MasterAssetCategory.Departement == dept);
                if (!String.IsNullOrEmpty(assetcategorycode))
                {
                    data = data.Where(x => x.MasterAssetCategory.AssetCategoryID == assetcategorycode);
                }
                if (!String.IsNullOrEmpty(assettypecode))
                {
                    data = data.Where(x => x.MasterAssetTypeID == assettypecode);
                }
                if (replacementdate != null)
                {
                    data = data.Where(x => x.ReplacementDate == replacementdate);
                }
                if (from != null)
                {
                    data = data.Where(x => x.InstallmentDate >= from);
                }
                if (to != null)
                {
                    data = data.Where(x => x.InstallmentDate <= to);
                }
                var ss = wb.Worksheets.Add("Data");
                List<AssetTypeReportModel> reportModel = new List<AssetTypeReportModel>();
                foreach (var item in data.OrderBy(x => x.AssetCategoryID))
                {
                    var date = DateTime.Now;
                    AssetTypeReportModel report = new AssetTypeReportModel();
                    var wo = WorkOrderManual.GetWorkOrderByAsset(item.MasterAssetCategory.AssetGroup).Where(x => x.WorkOrderDate <= date && x.CategoryAssetID == item.AssetCategoryID).ToList();
                    var lastWO = WorkOrderManual.GetWorkOrderByAsset(item.MasterAssetCategory.AssetGroup).Where(x => x.WorkOrderDate <= date && x.CategoryAssetID == item.AssetCategoryID).OrderBy(x => x.WorkOrderDate).Take(1).FirstOrDefault();
                    var nextWO = WorkOrderManual.GetWorkOrderByAsset(item.MasterAssetCategory.AssetGroup).Where(x => x.WorkOrderDate > date && x.CategoryAssetID == item.AssetCategoryID).OrderBy(x => x.WorkOrderDate).Take(1).FirstOrDefault();
                    var interval = MasterAssetCategory.GetAllIntervalByAssetCategoryID(item.MasterAssetCategory.AssetCategoryID);
                    report.AssetCategoryCode = item.AssetCategoryID;
                    report.AssetTypeCode = item.MasterAssetTypeID;
                    report.Description = item.Description;
                    report.InstallmentDate = item.InstallmentDate.ToString();
                    report.Lifetime = item.Lifetime;
                    report.ReplacementKe = item.Replacement.ToString();
                    report.ReplacementDate = item.ReplacementDate.ToString();
                    if (lastWO != null)
                    {
                        report.LastWONo = lastWO.WorkOderManualID;
                    }
                    else
                    {
                        report.LastWONo = "";
                    }
                    if (interval != null)
                    {
                        if (interval.FirstOrDefault(x => x.MaintenanceDate <= DateTime.Now) != null)
                        {
                            report.LastMaintenanceDate = interval.FirstOrDefault(x => x.MaintenanceDate <= DateTime.Now).MaintenanceDate.ToString();
                        }
                        else
                        {
                            report.LastMaintenanceDate = "";
                        }
                        if (interval.FirstOrDefault(x => x.MaintenanceDate > DateTime.Now) != null)
                        {
                            report.NextMaintenanceDate = interval.FirstOrDefault(x => x.MaintenanceDate > DateTime.Now).MaintenanceDate.ToString();
                        }
                        else
                        {
                            report.NextMaintenanceDate = "";
                        }
                    }
                    else
                    {
                        report.LastMaintenanceDate = "";
                        report.NextMaintenanceDate = "";
                    }
                    if (nextWO != null)
                    {
                        report.NextWONo = nextWO.WorkOderManualID;
                    }
                    else
                    {
                        report.NextWONo = "";
                    }



                    reportModel.Add(report);
                }
                ss.Cell("D1").Value = "Asset Type Report";
                ss.Cell("D2").Value = "PT. PLAZA INDONESIA REALTY, TBK";
                ss.Cell("D3").Value = "Asset Category Code: " + assetcategorycode;
                ss.Cell("D4").Value = "Asset Type Code: " + assettypecode;
                ss.Cell("D5").Value = "Installment Date from : " + from + " to : " + to;
                if (reportModel.Count() == 0)
                {
                    var tableWithStrings = ss.Cell("A11").Value = "Empty Data";
                 }
                else
                {
                    var tableWithStrings = ss.Cell("A11").InsertTable(reportModel);
                }
                var ws = wb.Worksheet("Data");
                var path = Server.MapPath(@"~/Assets/img/Logo PI-Lama-01.png");
                var image = ws.AddPicture(path);

                image.MoveTo(ws.Cell(1, 1).Address);

                // optional: resize picture
                //wb.SaveAs(folderPath + "DataGridViewExport.xlsx");
                string myName = Server.UrlEncode("AssetType" + "_" +
                DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx");
                MemoryStream stream = GetStream(wb);// The method is defined below
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition",
                "attachment; filename=" + myName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            return RedirectToAction(MVC.Report.AssetTypeReport());
        }
        public virtual ActionResult MainAssetReport()
        {
            SelectList selectlist = new SelectList();
            ViewBag.Group = selectlist.GetAssetGroupSelectList();
            string dept = Session["Dept"].ToString();
            ViewBag.Category = selectlist.GetAssetCategoryBydept(dept);
            return View();
        }
        [HttpPost]
        public virtual ActionResult MainAssetReport(DateTime? from, DateTime? to,string assetgroup,string assetcategory,string GeneralCode)
        {
            //Exporting to Excel
            string folderPath = "C:\\Excel\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //XLWorkbook wb = new XLWorkbook();
            //
            using (XLWorkbook wb = new XLWorkbook())
            {
                var dept = Session["Dept"].ToString();
                var data = MasterAssetCategory.GetAllMasterAssetCategory().Where(x => x.Departement == dept);
                if (!String.IsNullOrEmpty(assetgroup))
                {
                    data = data.Where(x => x.AssetGroup == assetgroup);
                }
                if (!String.IsNullOrEmpty(assetcategory))
                {
                    data = data.Where(x => x.AssetCategoryID == assetcategory);
                }
                if (!String.IsNullOrEmpty(GeneralCode))
                {
                    data = data.Where(x => x.AssetCategoryID.ToLower().Contains(GeneralCode.ToLower()));
                }
                if (from != null)
                {
                    data = data.Where(x => x.InstalmentDate >= from);
                }
                if (to != null)
                {
                    data = data.Where(x => x.InstalmentDate <= to);
                }
                var ss = wb.Worksheets.Add("Data");
                List<MainAssetReportModel> reportModel = new List<MainAssetReportModel>();
                foreach (var item in data)
                {
                    DateTime date = DateTime.Now;
                    MainAssetReportModel report = new MainAssetReportModel();
                    var lastWO = WorkOrderManual.GetWorkOrderByAsset(item.AssetGroup).Where(x => x.WorkOrderDate <= date && x.CategoryAssetID == item.AssetCategoryID).OrderByDescending(x => x.WorkOrderDate).Take(1).FirstOrDefault();

                    var nextWO = WorkOrderManual.GetWorkOrderByAsset(item.AssetGroup).Where(x => x.WorkOrderDate > date && x.CategoryAssetID == item.AssetCategoryID).OrderBy(x => x.WorkOrderDate).Take(1).FirstOrDefault();
                    var interval = MasterAssetCategory.GetAllIntervalByAssetCategoryID(item.AssetCategoryID);
                    report.AssetGroup = item.AssetGroup;
                    report.AssetCategoryCode = item.AssetCategoryID;
                    report.AssetCategory = item.AssetCategory;
                    report.Description = item.Description;
                    report.AssetLocationCode = item.Location;
                    report.InstallmentDate = item.InstalmentDate.ToString();
                    report.Lifetime = item.Lifetime.ToString();
                    if (lastWO != null)
                    {
                        report.LastWONo = lastWO.WorkOderManualID;
                    }
                    else
                    {
                        report.LastWONo = "";
                    }
                    if (interval != null)
                    {
                        if (interval.FirstOrDefault(x => x.MaintenanceDate <= date) != null)
                        {
                            //interval.Where(x => x.AssetCategoryID == item.AssetCategoryID).Last().MaintenanceDate.ToString();
                            //interval.FirstOrDefault(x => x.MaintenanceDate <= date).MaintenanceDate.ToString();
                            //report.LastMaintenanceDate
                        }
                        else
                        {
                            report.LastMaintenanceDate = "";
                        }
                        //if (interval.Where(x => x.MaintenanceDate > date).FirstOrDefault() != null)
                        //{
                        //    report.NextMaintenanceDate = interval.FirstOrDefault(x => x.MaintenanceDate > date).MaintenanceDate.ToString();
                        //}
                        //else
                        //{
                        //    report.NextMaintenanceDate = "";
                        //}
                    }
                    else
                    {
                        report.LastMaintenanceDate = "";
                        report.NextMaintenanceDate = "";
                    }
                    if (nextWO != null)
                    {
                        report.NextWONo = nextWO.WorkOderManualID;
                        if (nextWO.WorkOrderManualMaintenances != null)
                        {
                            report.NextMaintenanceDate = nextWO.WorkOrderDate.ToString();
                        }
                        else
                        {
                            report.NextMaintenanceDate = "";
                        }
                    }
                    else
                    {
                        report.NextWONo = "";
                    }
                    if (item.UserGroupHeader != null)
                    {
                        report.Leader = item.UserGroupHeader.UserGroupLeader;
                    }
                    else
                    {
                        report.Leader = "";
                    }


                    reportModel.Add(report);
                }
                ss.Cell("D1").Value = "Main Asset Report";
                ss.Cell("D2").Value = "PT. PLAZA INDONESIA REALTY, TBK";
                ss.Cell("D3").Value = "Asset Group: " + assetgroup;
                ss.Cell("D4").Value = "General Code: " + GeneralCode;
                ss.Cell("D5").Value = "Asset Category: " + assetcategory;
                ss.Cell("D6").Value = "Installment Date from : " + from + " to : " + to;
                if (reportModel.Count() == 0)
                {
                    var tableWithStrings = ss.Cell("A11").Value = "Empty Data";
                }
                else
                {
                    var tableWithStrings = ss.Cell("A11").InsertTable(reportModel);
                }
                var ws = wb.Worksheet("Data");
                var path = Server.MapPath(@"~/Assets/img/Logo PI-Lama-01.png");
                var image = ws.AddPicture(path);

                image.MoveTo(ws.Cell(1, 1).Address);

                // optional: resize picture
                //wb.SaveAs(folderPath + "DataGridViewExport.xlsx");
                string myName = Server.UrlEncode("MainAassetReport" + "_" +
                DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx");
                MemoryStream stream = GetStream(wb);// The method is defined below
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition",
                "attachment; filename=" + myName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            return RedirectToAction(MVC.Report.MainAssetReport());
        }
        void AddImage(XLWorkbook wb, string sheetName, int col, int row)
        {
          
            wb.Save();
        }
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
    }
}