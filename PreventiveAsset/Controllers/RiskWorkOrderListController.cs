﻿using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    public partial class RiskWorkOrderListController : Controller
    {
        // GET: RiskWorkOrderList
        public virtual ActionResult Index()
        {
            SelectList sel = new SelectList();
            ViewBag.dept = sel.GetDepartementNameSelectlistItem();
            return View();
        }
        public virtual JsonResult GetWorkOrderGrid(int draw, int start, int length, string Departement, DateTime? WorkOrderDateFrom , DateTime? WorkOrderDateTo,string Category)
        {
            int sortColumn = -1;
            var role = Session["Role"].ToString();
            var year = DateTime.Now.Year;
            var model = WorkOrderManual.GetAllRiskWorkOrder().Where(x => x.CreatedDate.Value.Year == year).OrderByDescending(x => x.CreatedDate);
            //var Data = model.OrderBy(x => x.sorting).ThenBy(x => x.WorkOrderDate);
            if (!string.IsNullOrEmpty(Departement))
            {
                model = model.Where(x => x.MasterAssetCategory.Departement == Departement).OrderByDescending(x => x.WorkOrderDate);
            }
            if (WorkOrderDateFrom != null)
            {
                model = model.Where(x => x.WorkOrderDate >= WorkOrderDateFrom).OrderByDescending(x => x.WorkOrderDate);
            }
            if (WorkOrderDateTo != null)
            {
                model = model.Where(x => x.WorkOrderDate <= WorkOrderDateTo).OrderByDescending(x => x.WorkOrderDate);
            }
            if (!string.IsNullOrEmpty(Category))
            {
                model = model.Where(x => x.MasterAssetCategory.AssetCategory == Category).OrderByDescending(x => x.WorkOrderDate); ;
            }
            int countTotal = model.Count();
            var data = model.ToList();
            if (model.Count() > 10)
            {
                data = data.GetRange(start, Math.Min(length, data.Count - start));
            }
            string sortDirection = "asc";
            if (length == -1)
            {
                length = countTotal;
            }
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = countTotal;
            dataTableData.ListData = AssignWorkOder(data);

            int recordsFiltered = countTotal;
            return Json(new
            {
                // this is what datatables wants sending back
                draw = dataTableData.draw,
                recordsTotal = 10,
                recordsFiltered = recordsFiltered,
                data = dataTableData.ListData
            }, JsonRequestBehavior.AllowGet);

        }
        private List<DataItem> AssignWorkOder(List<WorkOrderManual> data)
        {
            List<DataItem> dataitem = new List<DataItem>();

            foreach (var item in data)
            {
                DataItem items = new DataItem();
                items.WorkOrderNo = item.WorkOderManualID;
                items.Category = item.MasterAssetCategory.AssetCategory;
                items.Departement = item.MasterAssetCategory.Departement;
                items.WorkOrderDate = item.WorkOrderDate.GetValueOrDefault().ToString();
                items.Location = item.MasterAssetCategory.Location;
                items.Status = item.Status;

                dataitem.Add(items);
            }
            return dataitem;
        }
        public virtual ActionResult PickWorkOrder(string id)
        {
            return View();
        }
    }
}