﻿using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    [SessionTimeout]
    public partial class MasterAssetController : Controller
    {
        // GET: MasterAsset
        public virtual ActionResult Index()
        {
            SelectList select = new SelectList();
            ViewBag.active = select.GetActiveSelectList();
            return View();
        }
        public virtual JsonResult GetGridAssetCategory(int draw, int start, int length, string assetcategoryid, string assetdescription,string assetdepartement,string assetgroupcode,string assetstatus)
        {
            int sortColumn = -1;

            var DataInitial = MasterAssetCategory.GetAllMasterAssetCategory();
            string dept = Session["Dept"].ToString();
            var Data = DataInitial.Where(x => x.Departement == dept).ToList();
            if (!string.IsNullOrEmpty(assetgroupcode))
            {
                Data = Data.Where(x => x.AssetGroup.ToLower().Contains(assetgroupcode.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(assetcategoryid))
            {
                Data = Data.Where(x => x.AssetCategoryID.ToLower().Contains(assetcategoryid.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(assetdescription))
            {
                Data = Data.Where(x => x.Description.ToLower().Contains(assetdescription.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(assetstatus))
            {
                bool stat = bool.Parse(assetstatus);
                Data = Data.Where(x => x.IsActive == stat).ToList();
            }
            int countTotal = Data.Count;
            if (Data.Count > 10)
            {
                Data = Data.GetRange(start, Math.Min(length, Data.Count - start));
            }
            

            string sortDirection = "asc";
            if (length == -1)
            {
                length = countTotal;
            }
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = countTotal;
            dataTableData.ListData = AssignMasterAssetData(Data);

            int recordsFiltered = countTotal;
            return Json(new
            {
                // this is what datatables wants sending back
                draw = dataTableData.draw,
                recordsTotal = 10,
                recordsFiltered = recordsFiltered,
                data = dataTableData.ListData
            }, JsonRequestBehavior.AllowGet);


        }
        public virtual JsonResult GetCascadeGroup(string assetCategoryID)
        {
            var result = MasterAssetType.GetAllAssetTypeByCat(assetCategoryID).ToList();
            return Json(new {
                text = result.Select(x => x.MasterAssetTypeID)
            }, JsonRequestBehavior.AllowGet);
        }
        private List<DataItem> AssignMasterAssetData(List<MasterAssetCategory> data)
        {
            List<DataItem> dataitem = new List<DataItem>();

            foreach (var item in data)
            {
                DataItem items = new DataItem();
                items.AssetCategoryID = item.AssetCategoryID;
                items.AssetGroup = item.AssetGroup;
                items.AssetCategory = item.AssetCategory;
                items.Description = item.Description;
                items.Location = item.Location;
                items.InstallmentDate = item.InstalmentDate.ToString();
                item.Lifetime = item.Lifetime;
                if (item.IsActive.GetValueOrDefault())
                {
                    items.Status = "Active";
                }
                else
                {
                    items.Status = "In Active";
                }
                dataitem.Add(items);
            }
            return dataitem;
        }
        public virtual ActionResult AddEditAssetCategory(string id)
        {
            ViewBag.Interval = new List<Interval>();
            if (!string.IsNullOrEmpty(id))
            {
                var currentData = MasterAssetCategory.GetMasterAssetByID(id);
                ViewBag.Interval = MasterAssetCategory.GetAllIntervalByAssetCategoryID(id).ToList();
                ViewData.Model = currentData;
            }
            else
            {
                ViewData.Model = new MasterAssetCategory();
            }
            
            SelectList seleclist = new SelectList();
            ViewBag.selectLocation = seleclist.GetLocationSelectList();
            ViewBag.listInterval = Interval.GetAllEnableInterval().ToList();
            ViewBag.usergroup = seleclist.GetUserGroupSelectList();
            ViewBag.IKSeleclist = seleclist.GetIKSelectList();
            ViewBag.status = seleclist.GetActiveSelectList();
            ViewBag.departementList = seleclist.GetDepartementSelectList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEditAssetCategory(MasterAssetCategory model,string[] interval,string[] maintenanceperiod, string[] ik)
        {
            string loginID = Session["UserId"].ToString();
            if (!string.IsNullOrEmpty(model.AssetCategoryID))
            {
                var currentData = MasterAssetCategory.GetMasterAssetByID(model.AssetCategoryID);
                if (currentData != null)
                {
                    //update
                    currentData.AssetGroup = model.AssetGroup;
                    currentData.Departement = model.Departement;
                    currentData.AssetCategory = model.AssetCategory;
                    currentData.Description = model.Description;
                    currentData.Location = model.Location;
                    currentData.InstalmentDate = model.InstalmentDate;
                    currentData.Lifetime = model.Lifetime;
                    currentData.IsActive = model.IsActive;
                    currentData.UserGroup = model.UserGroupHeader.UserGroupId;
                    currentData.UpdateSave<MasterAssetCategory>();
                    //clear interval data
                    var intervalCurrent = MasterAssetCategory.GetAllIntervalByAssetCategoryID(model.AssetCategoryID).ToList();
                    foreach (var item in intervalCurrent)
                    {
                        item.Delete<Interval>();
                    }
                    //insert new fresh data
                    var maintenanceperiodFiltered = maintenanceperiod.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    var ikFiltered = ik.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    if (interval != null)
                    {
                        for (int i = 0; i < interval.Length; i++)
                        {
                            Interval intervalData = new Interval();
                            intervalData.Interval1 = interval[i];
                            intervalData.MaintenanceDate = DateTime.Parse(maintenanceperiodFiltered[i]);
                            intervalData.IKCode = ikFiltered[i];
                            intervalData.AssetCategoryID = model.AssetCategoryID;
                            intervalData.CreatedDate = DateTime.Now;
                            intervalData.CreatedBy = loginID;
                            intervalData.InsertSave<Interval>();
                        }
                    }
                    

                }
                else
                {

                    //insert
                    MasterAssetCategory newAsset = new MasterAssetCategory();
                    newAsset.AssetCategoryID = model.AssetCategoryID;
                    newAsset.AssetGroup = model.AssetGroup;
                    newAsset.Departement = model.Departement;
                    newAsset.AssetCategory = model.AssetCategory;
                    newAsset.Description = model.Description;
                    newAsset.Location = model.Location;
                    newAsset.InstalmentDate = model.InstalmentDate;
                    newAsset.Lifetime = model.Lifetime;
                    newAsset.IsActive = model.IsActive;
                    newAsset.UserGroup = model.UserGroup;
                    newAsset.InsertSave<MasterAssetCategory>();
                    var maintenanceperiodFiltered = maintenanceperiod.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    var ikFiltered = ik.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    for (int i = 0; i < interval.Length; i++)
                    {
                        Interval intervalData = new Interval();
                        intervalData.Interval1 = interval[i];
                        intervalData.MaintenanceDate = DateTime.Parse(maintenanceperiodFiltered[i]);
                        intervalData.IKCode = ikFiltered[i];
                        intervalData.AssetCategoryID = model.AssetCategoryID;
                        intervalData.CreatedDate = DateTime.Now;
                        intervalData.CreatedBy = loginID;
                        intervalData.InsertSave<Interval>();
                    }
                }
            }
            return RedirectToAction(MVC.MasterAsset.Index());
        }
        public virtual JsonResult CheckAssetCode(string search_keyword)
        {
            bool result = MasterAssetCategory.CheckCatCode(search_keyword.ToLower());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult GetHistoryAssetLog(string assetid)
        {
            var data = MaintenanceLog.GetHistoryByAssetID(assetid).ToList();
            return Json(data,JsonRequestBehavior.AllowGet);
        }
    }
}