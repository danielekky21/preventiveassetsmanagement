﻿using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    [SessionTimeout]
    public partial class InstruksiKerjaController : Controller
    {
        // GET: InstruksiKerja
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual JsonResult GetGridIK(int draw, int start, int length, string ikcode, string ikdescription)
        {
            int sortColumn = -1;
            string dept = Session["Dept"].ToString();
            var DataInitial = MasterIK.GetAllIK();
            var Data = DataInitial.Where(x => x.Departement == dept).ToList();
            int countTotal = Data.Count;
            if (!string.IsNullOrEmpty(ikdescription))
            {
                Data = Data.Where(x => x.IKDescription.ToLower().Contains(ikdescription.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(ikcode))
            {
                Data = Data.Where(x => x.IKCode.ToLower().Contains(ikcode.ToLower())).ToList();
            }
            Data = Data.GetRange(start, Math.Min(length, Data.Count - start));

            string sortDirection = "asc";
            if (length == -1)
            {
                length = countTotal;
            }
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = countTotal;
            dataTableData.ListData = AssignIKData(Data);

            int recordsFiltered = countTotal;
            return Json(new
            {
                // this is what datatables wants sending back
                draw = dataTableData.draw,
                recordsTotal = 10,
                recordsFiltered = recordsFiltered,
                data = dataTableData.ListData
            }, JsonRequestBehavior.AllowGet);


        }
        private List<DataItem> AssignIKData(List<MasterIK> data)
        {
            List<DataItem> dataitem = new List<DataItem>();

            foreach (var item in data)
            {
                DataItem items = new DataItem();
                items.IKCode = item.IKCode;
                items.IKDescription = item.IKDescription;
                if (item.IsActive.GetValueOrDefault())
                {
                    items.Status = "Active";
                }
                else
                {
                    items.Status = "In Active";
                }
                dataitem.Add(items);
            }
            return dataitem;
        }
        public virtual ActionResult InstruksiKerjaAddEdit(string id)
        {

            if (!string.IsNullOrEmpty(id))
            {
                var currentData = MasterIK.GetIKByID(id);
                if (currentData != null)
                {
                    ViewData.Model = currentData;
                }

            }
            else
            {
                ViewData.Model = new MasterIK();
            }
            SelectList selectList = new SelectList();
            ViewBag.Status = selectList.GetActiveSelectList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult InstruksiKerjaAddEdit(MasterIK model)
        {
            string loginID = Session["UserId"].ToString();
            if (model.IKCode != null)
            {
                var currentData = MasterIK.GetIKByID(model.IKCode);
                if (currentData != null)
                {
                    currentData.IKDescription = model.IKDescription;
                    currentData.IsActive = model.IsActive;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.UpdatedBy = loginID;
                    currentData.Departement = model.Departement;
                    currentData.UpdateSave<MasterIK>();
                }
                else
                {
                    //new data
                    MasterIK newData = new MasterIK();
                    newData.IKCode = model.IKCode;
                    newData.IKDescription = model.IKDescription;
                    newData.Departement = model.Departement;
                    newData.IsActive = model.IsActive;
                    newData.CreatedBy = loginID;
                    newData.CreatedDate = DateTime.Now;
                    newData.InsertSave<MasterIK>();
                }
            }
            return RedirectToAction(MVC.InstruksiKerja.Index());
        }
        public virtual ActionResult InstruksiKerjaCategoryList(string ikCode)
        {


            var category = MasterIK.GetCategoryByIKCode(ikCode).OrderBy(x => x.CreatedDate).ToList();
            ViewData.Model = category;
            ViewBag.ikcode = ikCode;

            return View();
        }
        public virtual ActionResult IKCategoryAddEdit(string IKCategory,string ikCode)
        {
            ViewBag.IKCode = ikCode;
            ViewBag.items = new List<IKItem>();
            if (!string.IsNullOrEmpty(IKCategory))
            {
                var currentData = MasterIK.GetCategoryByCategoryID(IKCategory);
                if (currentData != null)
                {
                    ViewData.Model = currentData;
                    ViewBag.items = MasterIK.GetAllIKItemByCategory(IKCategory).ToList();
                }
            }
            else
            {
                ViewData.Model = new IKCategory();
            }
            SelectList selectList = new SelectList();
            ViewBag.Status = selectList.GetActiveSelectList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult IKCategoryAddEdit(IKCategory model,string[] ikItem)
        {
            string loginID = Session["UserId"].ToString();
            if (!string.IsNullOrEmpty(model.IKCategory1))
            {
                //edit
                var currenData = MasterIK.GetCategoryByCategoryID(model.IKCategory1);
                if (currenData != null)
                {
                    currenData.IsActive = model.IsActive;
                    currenData.UpdateSave<IKCategory>();
                    //delete current item
                    var currentItem = MasterIK.GetAllIKItemByCategory(model.IKCategory1).ToList();
                    foreach (var item in currentItem)
                    {
                        item.Delete<IKItem>();
                    }
                    
                    //insert new fresh data
                    for (int i = 0; i < ikItem.Count(); i++)
                    {
                        var newData = new IKItem();
                        newData.Item = ikItem[i];
                        newData.IKCategory = model.IKCategory1;
                        newData.CreatedDate = DateTime.Now;
                        newData.CreatedBy = loginID;
                        newData.InsertSave<IKItem>();
                    }
                }
                else
                {
                    //new category
                    var category = new IKCategory();
                    category.IKCategory1 = model.IKCategory1;
                    category.MasterIK = model.MasterIK;
                    category.IsActive = model.IsActive;
                    category.InsertSave<IKCategory>();
                    for (int i = 0; i < ikItem.Count(); i++)
                    {
                        var newData = new IKItem();
                        newData.Item = ikItem[i];
                        newData.IKCategory = model.IKCategory1;
                        newData.CreatedDate = DateTime.Now;
                        newData.CreatedBy = loginID;
                        newData.InsertSave<IKItem>();
                    }
                }
            }
            return RedirectToAction(MVC.InstruksiKerja.InstruksiKerjaCategoryList(model.MasterIK));
        }
        public virtual JsonResult CheckIKCode(string search_keyword)
        {
            bool result = MasterIK.CheckIKcode(search_keyword.ToLower());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult checkIKCate(string search_keyword)
        {
            bool result = MasterIK.CheckIKCate(search_keyword.ToLower());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}