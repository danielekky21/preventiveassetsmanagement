﻿using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    [SessionTimeout]
    public partial class MasterAssetTypeController : Controller
    {
        // GET: MasterAssetType
        public virtual ActionResult Index()
        {
            SelectList select = new SelectList();
            ViewBag.active = select.GetActiveSelectList();
            return View();
        }
        public virtual JsonResult GetGridAssetTypeSearch(int draw, int start, int length, string assettypeid, string assettypedescription, string assetcategoryid, string assettypestatus)
        {
            int sortColumn = -1;
            string dept = Session["Dept"].ToString();
            var DataInitial = MasterAssetType.GetAllAssetType();
            var Data = DataInitial.Where(x => x.MasterAssetCategory.Departement == dept).ToList();
            Session["searchAssetTypeID"] = assettypeid;
            Session["searchAssetTypeDescription"] = assettypedescription;
            Session["searchAssetCategoryID"] = assetcategoryid;
            Session["searchAssetTypeStatus"] = assettypestatus;
            if (!string.IsNullOrEmpty(assettypeid))
            {
                Data = Data.Where(x => x.MasterAssetTypeID.ToLower().Contains(assettypeid.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(assettypedescription))
            {
                Data = Data.Where(x => x.Description.ToLower().Contains(assettypedescription.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(assetcategoryid))
            {
                Data = Data.Where(x => x.AssetCategoryID.ToLower().Contains(assetcategoryid.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(assettypestatus))
            {
                bool stat = bool.Parse(assettypestatus);
                Data = Data.Where(x => x.IsActive == stat).ToList();
            }
            int countTotal = Data.Count;
            if (Data.Count > 10)
            {
                Data = Data.GetRange(start, Math.Min(length, Data.Count - start));
            }


            string sortDirection = "asc";
            if (length == -1)
            {
                length = countTotal;
            }
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = countTotal;
            dataTableData.ListData = AssignMasterAssetTypeData(Data);

            int recordsFiltered = countTotal;
            return Json(new
            {
                // this is what datatables wants sending back
                draw = dataTableData.draw,
                recordsTotal = 10,
                recordsFiltered = recordsFiltered,
                data = dataTableData.ListData
            }, JsonRequestBehavior.AllowGet);


        }
        public virtual JsonResult GetGridAssetType(int draw, int start, int length, string assettypeid, string assettypedescription, string assetcategoryid , string assettypestatus)
        {
            int sortColumn = -1;
            string dept = Session["Dept"].ToString();
            var DataInitial = MasterAssetType.GetAllAssetType();
            var Data = DataInitial.Where(x => x.MasterAssetCategory.Departement == dept).ToList();
            if (Session["searchAssetTypeID"] != null)
            {
                Data = Data.Where(x => x.MasterAssetTypeID.ToLower().Contains(Session["searchAssetTypeID"].ToString().ToLower())).ToList();
            }
            if (Session["searchAssetTypeDescription"] != null)
            {
                Data = Data.Where(x => x.MasterAssetTypeID.ToLower().Contains(Session["searchAssetTypeDescription"].ToString().ToLower())).ToList();
            }
            if (Session["searchAssetCategoryID"] != null)
            {
                Data = Data.Where(x => x.AssetCategoryID.ToLower().Contains(Session["searchAssetCategoryID"].ToString().ToLower())).ToList();
            }
            if (Session["searchAssetTypeStatus"] != null)
            {
                if (!string.IsNullOrEmpty(Session["searchAssetTypeStatus"].ToString()))
                {
                    bool stat = bool.Parse(Session["searchAssetTypeStatus"].ToString());
                    Data = Data.Where(x => x.IsActive == stat).ToList();
                }
            }
            if (!string.IsNullOrEmpty(assettypeid))
            {
                Data = Data.Where(x => x.MasterAssetTypeID.ToLower().Contains(assettypeid.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(assettypedescription))
            {
                Data = Data.Where(x => x.Description.ToLower().Contains(assettypedescription.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(assetcategoryid))
            {
                Data = Data.Where(x => x.AssetCategoryID.ToLower().Contains(assetcategoryid.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(assettypestatus))
            {
                bool stat = bool.Parse(assettypestatus);
                Data = Data.Where(x => x.IsActive == stat).ToList();
            }
            int countTotal = Data.Count;
            if (Data.Count > 10)
            {
                Data = Data.GetRange(start, Math.Min(length, Data.Count - start));
            }
            

            string sortDirection = "asc";
            if (length == -1)
            {
                length = countTotal;
            }
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = countTotal;
            dataTableData.ListData = AssignMasterAssetTypeData(Data);

            int recordsFiltered = countTotal;
            return Json(new
            {
                // this is what datatables wants sending back
                draw = dataTableData.draw,
                recordsTotal = 10,
                recordsFiltered = recordsFiltered,
                data = dataTableData.ListData
            }, JsonRequestBehavior.AllowGet);


        }
        private List<DataItem> AssignMasterAssetTypeData(List<MasterAssetType> data)
        {
            List<DataItem> dataitem = new List<DataItem>();

            foreach (var item in data)
            {
                DataItem items = new DataItem();
                items.AssetTypeID = item.MasterAssetTypeID;
                items.AssetCategoryID = item.AssetCategoryID;
                items.Description = item.Description;
                items.InstallmentDate = item.InstallmentDate.ToString();
                items.Lifetime = item.Lifetime;
                items.Replacement = item.Replacement.ToString();
                items.ReplacementDate = item.ReplacementDate.ToString();
                if (item.IsActive.GetValueOrDefault())
                {
                    items.Status = "Active";
                }
                else
                {
                    items.Status = "In Active";
                }
                dataitem.Add(items);
            }
            return dataitem;
        }
        public virtual ActionResult AssetTypeAddEdit(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var currentData = MasterAssetType.GetAllAssetTypeByID(id);
                if (currentData != null)
                {
                    ViewData.Model = currentData;
                }

            }
            else
            {
                MasterAssetType newData = new MasterAssetType();
                ViewData.Model = newData;
            }
            SelectList select = new SelectList();
            ViewBag.active = select.GetActiveSelectList();
            ViewBag.assetGroup = select.GetAssetCategorySelectList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult AssetTypeAddEdit(MasterAssetType model)
        {
            string loginID = Session["UserId"].ToString();
            if (!string.IsNullOrEmpty(model.MasterAssetTypeID))
            {
               
                var currentData = MasterAssetType.GetAllAssetTypeByID(model.MasterAssetTypeID);
                if (currentData != null)
                {
                    //update
                    currentData.Description = model.Description;
                    currentData.Lifetime = model.Lifetime;
                    currentData.IsActive = model.IsActive;
                    currentData.UpdatedBy = loginID;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.UpdateSave<MasterAssetType>();

                }
                else
                {
                    //insert
                    MasterAssetType newAsset = new MasterAssetType();
                    newAsset.MasterAssetTypeID = model.MasterAssetTypeID;
                    newAsset.Description = model.Description;
                    newAsset.AssetCategoryID = model.AssetCategoryID;
                    newAsset.InstallmentDate = CurrentDataContext.CurrentContext.MasterAssetCategories.FirstOrDefault(x => x.AssetCategoryID == model.AssetCategoryID).InstalmentDate;
                    newAsset.Lifetime = model.Lifetime;
                    newAsset.IsActive = model.IsActive;
                    newAsset.CreatedBy = loginID;
                    newAsset.CreatedDate = DateTime.Now;
                    newAsset.InsertSave<MasterAssetType>();
                }
            }
            return RedirectToAction(MVC.MasterAssetType.Index());
        }
        public virtual JsonResult GetInstallmentDate(string id)
        {
            string installmentDate = CurrentDataContext.CurrentContext.MasterAssetCategories.FirstOrDefault(x => x.AssetCategoryID == id).InstalmentDate.ToString();
            return Json(installmentDate, JsonRequestBehavior.AllowGet);
        }
    }
}