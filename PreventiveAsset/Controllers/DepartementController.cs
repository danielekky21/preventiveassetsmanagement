﻿using PreventiveAssetManagement.dataRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    public partial class DepartementController : Controller
    {
        // GET: Departement
        public virtual ActionResult Index()
        {
            ViewData.Model = MasterDepartement.GetAllDepartementList().ToList();
            return View();
        }
        public virtual ActionResult AddEditDepartement(int id)
        {
            if (id != 0)
            {
                var deptData = MasterDepartement.GetDepartementByID(id);
                if (deptData != null)
                {
                    ViewData.Model = deptData;
                }
                else
                {
                    ViewData.Model = new MasterDepartement();
                }
            }
            else
            {
                ViewData.Model = new MasterDepartement();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEditDepartement(MasterDepartement model)
        {
            string loginID = Session["UserId"].ToString();
            if (model.DepartementID != 0)
            {
                var deptData = MasterDepartement.GetDepartementByID(model.DepartementID);
                if (deptData != null)
                {
                    deptData.DepartementName = model.DepartementName;
                    deptData.DepartementDesc = model.DepartementDesc;
                    deptData.color = model.color;
                    deptData.UpdatedBy = loginID;
                    deptData.UpdatedDate = DateTime.Now;
                    deptData.UpdateSave<MasterDepartement>();
                }

            }
            else
            {
                var newData = new MasterDepartement();
                newData.DepartementName = model.DepartementName;
                newData.DepartementDesc = model.DepartementDesc;
                newData.color = model.color;
                newData.CreatedBy = loginID;
                newData.CreatedDate = DateTime.Now;
                newData.IsDeleted = false;
                newData.InsertSave<MasterDepartement>();
            }
            return RedirectToAction(MVC.Departement.Index());
        }
        public virtual ActionResult DeleteDept(int id)
        {
            if (id != 0)
            {
                var deptData = MasterDepartement.GetDepartementByID(id);
                if (deptData != null)
                {
                    deptData.IsDeleted = true;
                    deptData.UpdateSave<MasterDepartement>();
                }
            }
            return RedirectToAction(MVC.Departement.Index());
        }
    }
}