﻿using PreventiveAssetManagement.dataRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    [SessionTimeout]
    public partial class WorkFlowController : Controller
    {
        // GET: WorkFlow
        public virtual ActionResult Index()
        {
            ViewData.Model = WorkFlow.GetWorkFlowSubmitByApproval(Session["UserId"].ToString()).OrderByDescending(x => x.CreatedDate).ToList();
            return View();
        }
        public virtual ActionResult WorkFlowRiskDetail(string workflowid, string workorderid)
        {
             var data = RiskVisit.GetRiskVisitByID(workorderid);
            if (data != null)
            {
                ViewBag.checklist = RiskVisit.GetRiskVisitChecklistByVisitID(data.VisitID).ToList();
                ViewData.Model = data;
            }
            else
            {
                RiskVisit newData = new RiskVisit();
                ViewBag.checklist = new List<RiskVisitChecklist>();
                newData.WorkOrderID = workorderid;
                ViewData.Model = newData;
            }
            SelectList sel = new SelectList();
            ViewBag.WOR = WorkOrderManual.GetWorkOderByID(data.WorkOrderID);
            ViewBag.loc = sel.GetLocationAreaSelectList();
            ViewBag.dpt = sel.GetDepartementSelectlistItem();
            ViewBag.workflowid = workflowid;
            ViewBag.itemgroup = sel.GetItemGroupCode();
            return View();
        }
        public virtual ActionResult WorkFlowDetail(string workflowid, string workorderid)
        {
            var WorkoderData = WorkOrderManual.GetWorkOderByID(workorderid);
            ViewData.Model = WorkoderData;
            ViewBag.category = MasterIK.GetCategoryByIKCode(WorkoderData.IKCode).ToList();
            ViewBag.condition = WorkOrderManual.GetIKConditionByWOID(workorderid).ToList();
            ViewBag.vendor = WorkOrderManual.GetVendorByWOID(workorderid).ToList();
            ViewBag.maintenance = WorkOrderManual.GetAllMaintenance(workorderid).ToList();
            SelectList selectlist = new SelectList();
            ViewBag.assettypeselectlist = selectlist.GetAssetTypeSelectList(WorkoderData.CategoryAssetID);
            ViewBag.workflowid = workflowid;
            var name = selectlist.GetUserIDNameSelectList();
            //foreach (var item in vendor)
            //{
            //    foreach (var items in name)
            //    {
            //        if (item.CompletedBy == items.Text)
            //        {
            //            items.Selected = true;
            //        }
            //    }
            //}
            ViewBag.userName = name;
            return View();
        }
        [HttpPost]
        public virtual ActionResult ApproveOrRejectWorkFlow(string workflowid, string command, string workorderid,string remark,string[] IKCategory, string[] vendornames, string[] condition, string[] IKItem, string[] workstartdates, string[] workenddates, string[] picremark, string[] assettypeid, string[] maintenancetype, string[] maintenancejumlah, string[] maintenancesatuan, string[] maintenanceremarks, string[] ikremarks, string[] IKCategoryfor , string[] completedby)
        {
            if (command == "approve")
            {
                

                var wf = WorkFlow.GetWorkFlowByWorkFlowID(int.Parse(workflowid));
               
                wf.WorkFlowStatus = "A";
                wf.ApprovedTime = DateTime.Now;
                wf.Remark = remark;
                wf.UpdatedBy = Session["UserId"].ToString();
                wf.UpdatedDate = DateTime.Now;
                wf.UpdateSave<WorkFlow>();
                var GetAllOtherWorkflow = WorkFlow.GetAllOtherWorkFlow(wf.WorkFlowWorkOrderID).ToList();
                foreach (var item in GetAllOtherWorkflow)
                {
                    item.WorkFlowStatus = "A";
                    item.ApprovedTime = DateTime.Now;
                    item.Remark = remark;
                    item.UpdatedBy = Session["UserId"].ToString();
                    item.UpdatedDate = DateTime.Now;
                    item.UpdateSave<WorkFlow>();
                }
                //do after
                if (workorderid.Contains("HIRAC"))
                {
                    var workorder = RiskVisit.GetRiskVisitByID(workorderid);
                    workorder.ApprovalStatus = "CLOSED";
                    workorder.UpdateSave<RiskVisit>();
                }
                else
                {
                    var workorder = WorkOrderManual.GetWorkOderByID(workorderid);
                    workorder.Status = "Approved";
                    workorder.sorting = 4;

                    workorder.UpdateSave<WorkOrderManual>();
                    var replacement = WorkOrderManual.GetMaintenanceByWOID(workorderid).ToList();
                    foreach (var item in replacement)
                    {
                        var assettype = MasterAssetType.GetAllAssetTypeByID(item.AssetTypeID);
                        if (assettype != null)
                        {
                            if (item.maintenanceType.ToLower() == "replace")
                            {
                                if (assettype.Replacement == null)
                                {
                                    assettype.Replacement = 1;

                                }
                                else
                                {
                                    assettype.Replacement = assettype.Replacement + 1;

                                }
                                assettype.ReplacementDate = item.maintenanceDate;
                            }
                            assettype.UpdateSave<MasterAssetType>();
                            //if (item.maintenanceType.ToLower() == "replace")
                            //{
                            //    //if (item.Nominal > 5000000)
                            //    //{
                            //    //    var log = MaintenanceLog.GetByassetIDFirst(item.WorkOrderManual.CategoryAssetID);
                            //    //    if (log != null)
                            //    //    {
                            //    //        log.ReplacementKe = log.ReplacementKe + 1;
                            //    //        log.DateReplacement = item.maintenanceDate.ToString();
                            //    //        log.UpdateSave<MaintenanceLog>();
                            //    //    }
                            //    //    else
                            //    //    {
                            //    //        MaintenanceLog newLog = new MaintenanceLog();
                            //    //        newLog.DateReplacement = item.maintenanceDate.ToString();
                            //    //        newLog.AssetCategoryCode = item.WorkOrderManual.CategoryAssetID;
                            //    //        newLog.AssetType = item.WorkOrderManual.MasterAssetCategory.MasterAssetTypes.FirstOrDefault().MasterAssetTypeID;
                            //    //        newLog.ReplacementKe = 1;
                            //    //        newLog.Remarks = item.Remarks;
                            //    //        newLog.Description = item.WorkOrderManual.MasterAssetCategory.MasterAssetTypes.FirstOrDefault().Description;
                            //    //        newLog.InsertSave<MaintenanceLog>();
                            //    //    }
                            //    //}
                            //}
                        }

                    }
                    var vendorCurrent = WorkOrderManual.GetAllVendor(workorderid).ToList();
                    if (vendorCurrent.Count() > 0)
                    {
                        foreach (var items in vendorCurrent)
                        {
                            items.Delete<IKConditionVendor>();
                        }
                    }
                    for (int a = 0; a < IKCategory.Length; a++)
                    {
                        IKConditionVendor vendor = new IKConditionVendor();
                        vendor.VendorName = vendornames[a];
                        vendor.StartedBy = Session["UserId"].ToString();
                        if (!string.IsNullOrEmpty(workstartdates[a]))
                        {
                            vendor.WorkStartDate = DateTime.Parse(workstartdates[a]);

                        }
                        if (!string.IsNullOrEmpty(workenddates[a]))
                        {
                            vendor.WorkEndDate = DateTime.Parse(workenddates[a]);
                        }
                        vendor.CompletedBy = completedby[a];
                        vendor.PICRemarks = picremark[a];
                        vendor.WorkOrderManual = workorderid;
                        vendor.IKCategory = IKCategory[a];
                        vendor.InsertSave<IKConditionVendor>();
                    }
                }

            }
            if (command == "reject")
            {
                var wf = WorkFlow.GetWorkFlowByWorkFlowID(int.Parse(workflowid));
                wf.WorkFlowStatus = "R";
                wf.ApprovedTime = DateTime.Now;
                wf.UpdatedBy = Session["UserId"].ToString();
                wf.UpdatedDate = DateTime.Now;
                wf.Remark = remark;
                wf.UpdateSave<WorkFlow>();
                var GetAllOtherWorkflow = WorkFlow.GetAllOtherWorkFlow(wf.WorkFlowWorkOrderID).ToList();
                foreach (var item in GetAllOtherWorkflow)
                {
                    item.WorkFlowStatus = "A";
                    item.ApprovedTime = DateTime.Now;
                    item.Remark = remark;
                    item.UpdatedBy = Session["UserId"].ToString();
                    item.UpdatedDate = DateTime.Now;
                    item.UpdateSave<WorkFlow>();
                }
                //do after
                if (workorderid.Contains("HIRAC"))
                {
                    var workorder = RiskVisit.GetRiskVisitByID(workorderid);
                    workorder.ApprovalStatus = "REJECT";
                    workorder.UpdateSave<RiskVisit>();
                }
                else
                {
                    var workorder = WorkOrderManual.GetWorkOderByID(workorderid);
                    workorder.Status = "Reject";
                    workorder.sorting = 1;
                    workorder.UpdateSave<WorkOrderManual>();
                }
            }
            Session["WfTotal"] = WorkFlow.CountWorkFlow(Session["UserId"].ToString());
            return RedirectToAction(MVC.WorkFlow.Index());
        }
    }
}