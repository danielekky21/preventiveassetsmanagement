﻿using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    [SessionTimeout]
    public partial class VendorController : Controller
    {
        // GET: Vendor
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual JsonResult GetVendorGrid(int draw, int start, int length, string UserID, string VendorName)
        {
            var Data = MasterVendor.GetAllActiveVendor().ToList();

            if (!string.IsNullOrEmpty(VendorName))
            {
                Data = Data.Where(x => x.VendorName.ToLower().Contains(VendorName.ToLower())).ToList();
            }
            string sortDirection = "asc";
            if (length == -1)
            {
                length = MasterVendor.GetAllActiveVendor().Count();
            }
            int countTotal = Data.Count;
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = countTotal;
            dataTableData.ListData = AssignVendorData(Data);
            int recordsFiltered = countTotal;
            return Json(new
            {
                // this is what datatables wants sending back
                draw = dataTableData.draw,
                recordsTotal = 10,
                recordsFiltered = recordsFiltered,
                data = dataTableData.ListData
            }, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult autocompletevendor(string prefix)
        {
            var vendorname = MasterVendor.VendorList(prefix).ToList();
            return Json(vendorname, JsonRequestBehavior.AllowGet);
        }
        private List<DataItem> AssignVendorData(List<MasterVendor> data)
        {
            List<DataItem> dataitem = new List<DataItem>();

            foreach (var item in data)
            {
                DataItem items = new DataItem();
                items.VendorID = item.VendorID.ToString();
                items.VendorName = item.VendorName;
                items.VendorDescription = item.VendorDescription;

                dataitem.Add(items);
            }
            return dataitem;
        }
        public virtual ActionResult AddEdit(int vendorid)
        {
            if (vendorid != 0)
            {
                var model = MasterVendor.GetVendorByID(vendorid);
                if (model != null)
                {
                    ViewData.Model = model;
                }
            }
            else
            {
                ViewData.Model = new MasterVendor();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(MasterVendor model)
        {
            string loginID = Session["UserId"].ToString();
            if (model.VendorID != 0)
            {
                var data = MasterVendor.GetVendorByID(model.VendorID);
                if (data != null)
                {
                    data.VendorName = model.VendorName;
                    data.VendorDescription = model.VendorDescription;
                    data.UpdatedBy = loginID;
                    data.UpdatedDate = DateTime.Now;
                    data.UpdateSave<MasterVendor>();
                }

            }
            else
            {
                var currentData = new MasterVendor();
                currentData.VendorName = model.VendorName;
                currentData.VendorDescription = model.VendorDescription;
                currentData.CreatedBy = loginID;
                currentData.IsActive = true;
                currentData.CreatedDate = DateTime.Now;
                currentData.InsertSave<MasterVendor>();
            }
            return RedirectToAction(MVC.Vendor.Index());
        }
        public virtual ActionResult Delete(int vendorid)
        {
            string loginID = Session["UserId"].ToString();
            if (vendorid != 0)
            {
                var model = MasterVendor.GetVendorByID(vendorid);
                if (model != null)
                {
                    model.IsActive = false;
                    model.UpdatedDate = DateTime.Now;
                    model.UpdatedBy = loginID;
                    model.UpdateSave<MasterVendor>();
                }
            }
            return RedirectToAction(MVC.Vendor.Index());
        }
    }
}