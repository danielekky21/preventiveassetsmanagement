﻿using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    [SessionTimeout]
    public partial class WorkOrderController : Controller
    {
        // GET: WorkOrder
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult WorkOrder()
        {
            SelectList selectlist = new SelectList();
            ViewBag.ik = selectlist.GetIKSelectList();
            ViewBag.asset = selectlist.GetAssetCategorySelectList();

            var role = Session["Role"].ToString();
            var model = new List<WorkOrderManual>();
            //if (role.ToLower() == "manager" || role.ToLower() == "leader")
            //{
                var grouplist = (List<UserGroupDetail>)Session["GroupIDs"];
                foreach (var item in grouplist)
                {
                    var workOrder = WorkOrderManual.GetWorkOrderByGroup(item.UserGroupID).ToList();
                    model.AddRange(workOrder);
                }
            //}
            //else
            //{
            //    var grouplist = (UserGroupDetail)Session["GroupID"];
            //    model = WorkOrderManual.GetWorkOrderByGroup(grouplist.UserGroupID).ToList();
            //}
            ViewData.Model = model.OrderBy(x => x.sorting).ThenBy(x => x.WorkOrderDate).ToList();
            return View();

        }
        public virtual JsonResult GetWorkOrderGrid(int draw, int start, int length,string CategoryAssetID,string WorkOrderNo,string description)
        {
            int sortColumn = -1;
            var role = Session["Role"].ToString();
            var model = new List<WorkOrderManual>();
            if (role.ToLower() == "manager" || role.ToLower() == "leader")
            {
                var grouplist = (List<UserGroupDetail>)Session["GroupIDs"];
                foreach (var item in grouplist)
                {
                    var workOrder = WorkOrderManual.GetWorkOrderByGroup(item.UserGroupID).ToList();
                    model.AddRange(workOrder);
                }
            }
            else
            {
                var grouplist = (UserGroupDetail)Session["GroupID"];
                model = WorkOrderManual.GetWorkOrderByGroup(grouplist.UserGroupID).ToList();
            }
            var Data = model.OrderBy(x => x.sorting).ThenBy(x => x.WorkOrderDate).ToList();
            if (!string.IsNullOrEmpty(CategoryAssetID))
            {
                Data = Data.Where(x => x.CategoryAssetID.ToLower().Contains(CategoryAssetID.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(WorkOrderNo))
            {
                Data = Data.Where(x => x.WorkOderManualID.ToLower().Contains(WorkOrderNo.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(description))
            {
                Data = Data.Where(x => x.CategoryDescription.ToLower().Contains(description.ToLower())).ToList();
            }
            int countTotal = Data.Count;
            if (Data.Count > 10)
            {
                Data = Data.GetRange(start, Math.Min(length, Data.Count - start));
            }
            string sortDirection = "asc";
            if (length == -1)
            {
                length = countTotal;
            }
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = countTotal;
            dataTableData.ListData = AssignWorkOder(Data);

            int recordsFiltered = countTotal;
            return Json(new
            {
                // this is what datatables wants sending back
                draw = dataTableData.draw,
                recordsTotal = 10,
                recordsFiltered = recordsFiltered,
                data = dataTableData.ListData
            }, JsonRequestBehavior.AllowGet);

        }
        private List<DataItem> AssignWorkOder(List<WorkOrderManual> data)
        {
            List<DataItem> dataitem = new List<DataItem>();

            foreach (var item in data)
            {
                DataItem items = new DataItem();
                items.WorkOrderNo = item.WorkOderManualID;
                items.AssetCategoryID = item.CategoryAssetID;
                items.WorkOrderDate = item.WorkOrderDate.GetValueOrDefault().ToString();
                items.Description = item.CategoryDescription;
                items.Status = item.Status;

                dataitem.Add(items);
            }
            return dataitem;
        }
        public virtual ActionResult Calendar()
        {
            return View();
        }
        public virtual JsonResult GetWorkOrderRisk(string DepartementID, DateTime? WorkOrderDateFrom, DateTime? WorkOrderDateTo)
        {
            var workorder = new List<WorkOrderManual>();
            var role = Session["Role"].ToString();
            workorder = WorkOrderManual.GetAllWorkOrder().ToList();
            //if (role.ToLower() == "manager" || role.ToLower() == "leader")
            //{
            if (!string.IsNullOrEmpty(DepartementID))
            {
                workorder = workorder.Where(x => x.MasterAssetCategory.Departement == DepartementID).ToList();
            }
            if (WorkOrderDateFrom != null)
            {
                workorder = workorder.Where(x => x.WorkOrderDate >= WorkOrderDateFrom).ToList();
            }
            if (WorkOrderDateTo != null)
            {
                workorder = workorder.Where(x => x.WorkOrderDate <= WorkOrderDateTo).ToList();
            }
            var eventList = workorder.Where(x => x.Status != "Submit" && x.Status != "Approved").Select(x => new CalendarModel
            {

                id = x.WorkOderManualID,
                title = x.WorkOderManualID,
                start = x.WorkOrderDate.GetValueOrDefault().ToString(),
                desc = x.CategoryDescription,
                end = x.WorkOrderDate.GetValueOrDefault().ToString(),
                color = "#2980b9",
                allDay = false,
                dept = x.MasterAssetCategory.Departement
            }).ToArray();
            return Json(eventList, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult GetWorkOderDate(string categoryassetid,string workordersearch)
        {
            var workorder = new List<WorkOrderManual>();
            var role = Session["Role"].ToString();
            //workorder = WorkOrderManual.GetWorkOrderByGroup(grouplist.UserGroupID).ToList();
            //if (role.ToLower() == "manager" || role.ToLower() == "leader")
            //{
                var grouplist = (List<UserGroupDetail>)Session["GroupIDs"];
                foreach (var item in grouplist)
                {
                   var workOrder = WorkOrderManual.GetWorkOrderByGroup(item.UserGroupID).ToList();
                    workorder.AddRange(workOrder);
                }
            //}
            //else
            //{
            //    var grouplist = (UserGroupDetail)Session["GroupID"];
            //    workorder = WorkOrderManual.GetWorkOrderByGroup(grouplist.UserGroupID).ToList();
            //}
            if (!string.IsNullOrEmpty(categoryassetid))
            {
                workorder = workorder.Where(x => x.CategoryAssetID.ToLower().Contains(categoryassetid.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(workordersearch))
            {
                workorder = workorder.Where(x => x.WorkOderManualID.ToLower().Contains(workordersearch.ToLower())).ToList();
            }
            var eventList = workorder.Where(x => x.Status != "Submit" && x.Status != "Approved").Select(x => new CalendarModel
            {

                id = x.WorkOderManualID,
                title = x.WorkOderManualID,
                start = x.WorkOrderDate.GetValueOrDefault().ToString(),
                desc = x.CategoryDescription,
                end = x.WorkOrderDate.GetValueOrDefault().ToString(),
                color = "#2980b9",
                allDay = false,
                dept = x.MasterAssetCategory.Departement
            }).ToArray();
            //foreach (var item in eventList)
            //{
            //    var color = MasterDepartement.GetDepartmenetColorByName(item.dept);
            //    if (!string.IsNullOrEmpty(color))
            //    {
            //        item.color = color;
            //    }
            //}
            
            for (int i = 0; i < eventList.Count(); i++)
            {
                var color = MasterDepartement.GetDepartmenetColorByName(eventList[i].dept);
                if (!string.IsNullOrEmpty(color))
                {
                    eventList[i].color = color;
                }
            }
            return Json(eventList, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult GeneratePeriodicWorkOrder(string categoryAssetID, string IKCode)
        {
            string dept = Session["Dept"].ToString();
            //var allAsset = MasterAssetCategory.GetAllActiveMasterAssetCategory().Where(x => x.Departement == dept).ToList();
            var allAsset = MasterAssetCategory.GetCategoryByAsset("PEP");

            //MasterAssetCategory assetCategory = MasterAssetCategory.GetMasterAssetByID(categoryAssetID);
            SelectList selectlist = new SelectList();
            ViewBag.assettypeselectlist = selectlist.GetAssetTypeSelectList(categoryAssetID);
            ViewBag.ikCategory = MasterIK.GetCategoryByIKCode(IKCode).ToList();
            //Get Periodic Time from assetCategory
            foreach (var assetCategory in allAsset)
            {
                var periodicTime = MasterAssetCategory.GetAllIntervalByAssetCategoryID(assetCategory.AssetCategoryID).ToList();
                foreach (var item in periodicTime)
                {
                    if (item.IsGenerated != true)
                    {
                        var intervalData = Interval.GetMasterIntervalByCode(item.Interval1);
                        if (intervalData != null)
                        {

                            WorkOrderManual workorder = new WorkOrderManual();
                            if (item.MaintenanceDate >= DateTime.Now)
                            {
                                string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();
                                workorder.WorkOderManualID = id;
                                workorder.CategoryAssetID = assetCategory.AssetCategoryID;
                                workorder.AssetLocationCode = assetCategory.Location;
                                workorder.Status = "Open";
                                workorder.sorting = 2;
                                workorder.StartedBy = Session["UserId"].ToString();
                                workorder.IKCode = item.IKCode;
                                workorder.CategoryDescription = assetCategory.Description;
                                workorder.AssetLocationCode = assetCategory.Location;
                                workorder.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                                workorder.IKDescription = item.MasterIK.IKDescription;
                                workorder.WorkOderManualID = id;
                                workorder.WorkOrderDate = item.MaintenanceDate;
                                workorder.isactive = true;
                                workorder.CreatedDate = DateTime.Now;
                                workorder.InsertSave<WorkOrderManual>();
                                item.IsGenerated = true;
                                item.UpdateSave<MasterInterval>();
                            }
                            if (intervalData.IntervalType != null)
                            {
                                if (intervalData.IntervalType.ToLower() == "month")
                                {
                                    var time = item.MaintenanceDate.GetValueOrDefault();

                                    ValidateIntervalMonth(time, intervalData.IntervalValue.GetValueOrDefault(), assetCategory.AssetCategoryID, item.IKCode);
                                }
                                if (intervalData.IntervalType.ToLower() == "week")
                                {
                                    var time = item.MaintenanceDate.GetValueOrDefault();

                                    ValidateIntervalWeek(time, intervalData.IntervalValue.GetValueOrDefault(), assetCategory.AssetCategoryID, item.IKCode);
                                }
                                if (intervalData.IntervalType.ToLower() == "year")
                                {

                                }
                                item.IsGenerated = true;
                                item.UpdateSave<MasterInterval>();
                            }


                        }
                    }
                    //if (item.Interval1.ToLower().Contains("500 hour"))
                    //{
                    //    WorkOrderManual workorder = new WorkOrderManual();

                    //    string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();
                    //    workorder.WorkOderManualID = id;
                    //    workorder.CategoryAssetID = assetCategory.AssetCategoryID;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.Status = "Open";
                    //    workorder.StartedBy = Session["UserId"].ToString();
                    //    workorder.IKCode = item.IKCode;
                    //    workorder.CategoryDescription = assetCategory.Description;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                    //    workorder.IKDescription = item.MasterIK.IKDescription;
                    //    workorder.WorkOderManualID = id;
                    //    workorder.WorkOrderDate = item.MaintenanceDate;
                    //    workorder.isactive = true;
                    //    workorder.CreatedDate = DateTime.Now;
                    //    workorder.InsertSave<WorkOrderManual>();
                    //}
                    //if (item.Interval1.ToLower().Contains("1000 hour"))
                    //{
                    //    WorkOrderManual workorder = new WorkOrderManual();
                    //    string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();
                    //    workorder.WorkOderManualID = id;
                    //    workorder.CategoryAssetID = assetCategory.AssetCategoryID;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.Status = "Open";
                    //    workorder.StartedBy = Session["UserId"].ToString();
                    //    workorder.IKCode = item.IKCode;
                    //    workorder.CategoryDescription = assetCategory.Description;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                    //    workorder.IKDescription = item.MasterIK.IKDescription;
                    //    workorder.WorkOderManualID = id;
                    //    workorder.WorkOrderDate = item.MaintenanceDate;
                    //    workorder.isactive = true;
                    //    workorder.CreatedDate = DateTime.Now;
                    //    workorder.InsertSave<WorkOrderManual>();
                    //}

                    //if (item.Interval1.ToLower() == "trimester")
                    //{
                    //    WorkOrderManual workorder = new WorkOrderManual();
                    //    var time = item.MaintenanceDate.GetValueOrDefault();
                    //    string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();
                    //    workorder.WorkOderManualID = id;
                    //    workorder.CategoryAssetID = assetCategory.AssetCategoryID;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.Status = "Open";
                    //    workorder.StartedBy = Session["UserId"].ToString();
                    //    workorder.IKCode = item.IKCode;
                    //    workorder.CategoryDescription = assetCategory.Description;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                    //    workorder.IKDescription = item.MasterIK.IKDescription;
                    //    workorder.WorkOderManualID = id;
                    //    workorder.WorkOrderDate = item.MaintenanceDate;
                    //    workorder.isactive = true;
                    //    workorder.CreatedDate = DateTime.Now;
                    //    workorder.InsertSave<WorkOrderManual>();
                    //    ValidateIntervalMonth(time, 3, assetCategory.AssetCategoryID, item.IKCode);
                    //}
                    //if (item.Interval1.ToLower() == "weekly")
                    //{
                    //    WorkOrderManual workorder = new WorkOrderManual();
                    //    var time = item.MaintenanceDate.GetValueOrDefault();
                    //    string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();
                    //    workorder.WorkOderManualID = id;
                    //    workorder.CategoryAssetID = assetCategory.AssetCategoryID;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.Status = "Open";
                    //    workorder.StartedBy = Session["UserId"].ToString();
                    //    workorder.IKCode = item.IKCode;
                    //    workorder.CategoryDescription = assetCategory.Description;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                    //    workorder.IKDescription = item.MasterIK.IKDescription;
                    //    workorder.WorkOderManualID = id;
                    //    workorder.WorkOrderDate = item.MaintenanceDate.GetValueOrDefault();
                    //    workorder.isactive = true;
                    //    workorder.CreatedDate = DateTime.Now;
                    //    workorder.InsertSave<WorkOrderManual>();
                    //    ValidateIntervalWeek(time, 7, assetCategory.AssetCategoryID, item.IKCode);
                    //}
                    //if (item.Interval1.ToLower() == "biweekly")
                    //{
                    //    WorkOrderManual workorder = new WorkOrderManual();
                    //    var time = item.MaintenanceDate.GetValueOrDefault();
                    //    string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();
                    //    workorder.WorkOderManualID = id;
                    //    workorder.CategoryAssetID = assetCategory.AssetCategoryID;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.Status = "Open";
                    //    workorder.StartedBy = Session["UserId"].ToString();
                    //    workorder.IKCode = item.IKCode;
                    //    workorder.CategoryDescription = assetCategory.Description;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                    //    workorder.IKDescription = item.MasterIK.IKDescription;
                    //    workorder.WorkOderManualID = id;
                    //    workorder.WorkOrderDate = item.MaintenanceDate.GetValueOrDefault();
                    //    workorder.isactive = true;
                    //    workorder.CreatedDate = DateTime.Now;
                    //    workorder.InsertSave<WorkOrderManual>();
                    //    ValidateIntervalWeek(time, 14, assetCategory.AssetCategoryID, item.IKCode);
                    //}
                    //if (item.Interval1.ToLower() == "monthly")
                    //{
                    //    WorkOrderManual workorder = new WorkOrderManual();
                    //    var time = item.MaintenanceDate.GetValueOrDefault();
                    //    string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();
                    //    workorder.WorkOderManualID = id;
                    //    workorder.CategoryAssetID = assetCategory.AssetCategoryID;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.Status = "Open";
                    //    workorder.StartedBy = Session["UserId"].ToString();
                    //    workorder.IKCode = item.IKCode;
                    //    workorder.CategoryDescription = assetCategory.Description;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                    //    workorder.IKDescription = item.MasterIK.IKDescription;
                    //    workorder.WorkOderManualID = id;
                    //    workorder.WorkOrderDate = item.MaintenanceDate;
                    //    workorder.isactive = true;
                    //    workorder.CreatedDate = DateTime.Now;
                    //    workorder.InsertSave<WorkOrderManual>();
                    //    ValidateIntervalMonth(time, 1, assetCategory.AssetCategoryID, item.IKCode);
                    //}
                    //if (item.Interval1.ToLower() == "bimonthly")
                    //{
                    //    WorkOrderManual workorder = new WorkOrderManual();
                    //    var time = item.MaintenanceDate.GetValueOrDefault();
                    //    string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();
                    //    workorder.WorkOderManualID = id;
                    //    workorder.CategoryAssetID = assetCategory.AssetCategoryID;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.Status = "Open";
                    //    workorder.StartedBy = Session["UserId"].ToString();
                    //    workorder.IKCode = item.IKCode;
                    //    workorder.CategoryDescription = assetCategory.Description;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                    //    workorder.IKDescription = item.MasterIK.IKDescription;
                    //    workorder.WorkOderManualID = id;
                    //    workorder.WorkOrderDate = item.MaintenanceDate;
                    //    workorder.isactive = true;
                    //    workorder.CreatedDate = DateTime.Now;
                    //    workorder.InsertSave<WorkOrderManual>();
                    //    ValidateIntervalMonth(time, 2, assetCategory.AssetCategoryID, item.IKCode);
                    //}
                    //if (item.Interval1.ToLower() == "semester")
                    //{
                    //    WorkOrderManual workorder = new WorkOrderManual();
                    //    var time = item.MaintenanceDate.GetValueOrDefault();
                    //    string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();
                    //    workorder.WorkOderManualID = id;
                    //    workorder.CategoryAssetID = assetCategory.AssetCategoryID;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.Status = "Open";
                    //    workorder.StartedBy = Session["UserId"].ToString();
                    //    workorder.IKCode = item.IKCode;
                    //    workorder.CategoryDescription = assetCategory.Description;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                    //    workorder.IKDescription = item.MasterIK.IKDescription;
                    //    workorder.WorkOderManualID = id;
                    //    workorder.WorkOrderDate = item.MaintenanceDate;
                    //    workorder.isactive = true;
                    //    workorder.CreatedDate = DateTime.Now;
                    //    workorder.InsertSave<WorkOrderManual>();
                    //    ValidateIntervalMonth(time, 6, assetCategory.AssetCategoryID, item.IKCode);
                    //}
                    //if (item.Interval1.ToLower().Contains("year"))
                    //{

                    //    WorkOrderManual workorder = new WorkOrderManual();
                    //    string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();
                    //    workorder.WorkOderManualID = id;
                    //    workorder.CategoryAssetID = assetCategory.AssetCategoryID;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.Status = "Open";
                    //    workorder.StartedBy = Session["UserId"].ToString();
                    //    workorder.IKCode = item.IKCode;
                    //    workorder.CategoryDescription = assetCategory.Description;
                    //    workorder.AssetLocationCode = assetCategory.Location;
                    //    workorder.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                    //    workorder.IKDescription = item.MasterIK.IKDescription;
                    //    workorder.WorkOderManualID = id;
                    //    workorder.WorkOrderDate = item.MaintenanceDate.GetValueOrDefault();
                    //    workorder.isactive = true;
                    //    workorder.CreatedDate = DateTime.Now;
                    //    workorder.InsertSave<WorkOrderManual>();
                    //}
                }
            }

            return RedirectToAction(MVC.WorkOrder.WorkOrder());
        }
        public void ValidateIntervalMonth(DateTime time, int month, string categoryAssetID, string IKCode)
        {
            DateTime endTime = new DateTime(DateTime.Now.Year, 12, 31);
            time = time.AddMonths(month);
            if (time < endTime)
            {


                if (time >= DateTime.Now)

                {

                    WorkOrderManual wo = new WorkOrderManual();
                    MasterAssetCategory assetCategory = MasterAssetCategory.GetMasterAssetByID(categoryAssetID);
                    SelectList selectlist = new SelectList();
                    MasterIK ik = MasterIK.GetIKByID(IKCode);
                    string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();

                    wo.CategoryAssetID = assetCategory.AssetCategoryID;
                    wo.AssetLocationCode = assetCategory.Location;
                    wo.Status = "Open";
                    wo.StartedBy = Session["UserId"].ToString();
                    wo.sorting = 3;
                    wo.IKCode = ik.IKCode;
                    wo.CategoryDescription = assetCategory.Description;
                    wo.AssetLocationCode = assetCategory.Location;
                    //wo.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                    wo.IKDescription = ik.IKDescription;
                    wo.WorkOderManualID = id;
                    wo.isactive = true;
                    wo.WorkOrderDate = time;
                    wo.CreatedDate = DateTime.Now;
                    wo.InsertSave<WorkOrderManual>();


                }
                ValidateIntervalMonth(time, month, categoryAssetID, IKCode);
            }
        }
        public void ValidateIntervalWeek(DateTime time, int days, string categoryAssetID, string IKCode)
        {
            DateTime endTime = new DateTime(DateTime.Now.Year, 12, 31);
            time = time.AddDays(days);
            if (time < endTime)
            {



                if (time >= DateTime.Now)
                {

                    WorkOrderManual wo = new WorkOrderManual();
                    MasterAssetCategory assetCategory = MasterAssetCategory.GetMasterAssetByID(categoryAssetID);
                    SelectList selectlist = new SelectList();
                    MasterIK ik = MasterIK.GetIKByID(IKCode);
                    string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();

                    wo.CategoryAssetID = assetCategory.AssetCategoryID;
                    wo.AssetLocationCode = assetCategory.Location;
                    wo.Status = "Open";
                    wo.sorting = 3;
                    wo.StartedBy = Session["UserId"].ToString();
                    wo.IKCode = ik.IKCode;
                    wo.CategoryDescription = assetCategory.Description;
                    wo.AssetLocationCode = assetCategory.Location;
                    //wo.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                    wo.IKDescription = ik.IKDescription;
                    wo.WorkOderManualID = id;
                    wo.isactive = true;
                    wo.WorkOrderDate = time;
                    wo.CreatedDate = DateTime.Now;
                    wo.InsertSave<WorkOrderManual>();


                }
                ValidateIntervalWeek(time, days, categoryAssetID, IKCode);
            }
        }
        public void ValidateInterval(DateTime time, int hour, string categoryAssetID, string IKCode)
        {
            DateTime endTime = new DateTime(DateTime.Now.Year, 12, 31);
            time = time.AddHours(hour);
            if (time < endTime)
            {

                WorkOrderManual wo = new WorkOrderManual();
                MasterAssetCategory assetCategory = MasterAssetCategory.GetMasterAssetByID(categoryAssetID);
                SelectList selectlist = new SelectList();
                MasterIK ik = MasterIK.GetIKByID(IKCode);
                string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();

                wo.CategoryAssetID = assetCategory.AssetCategoryID;
                wo.AssetLocationCode = assetCategory.Location;
                wo.Status = "Open";
                wo.sorting = 3;
                wo.StartedBy = Session["UserId"].ToString();
                wo.IKCode = ik.IKCode;
                wo.CategoryDescription = assetCategory.Description;
                wo.AssetLocationCode = assetCategory.Location;
                //wo.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
                wo.IKDescription = ik.IKDescription;
                wo.WorkOderManualID = id;
                wo.isactive = true;
                wo.WorkStartDate = time;
                wo.WorkOrderDate = DateTime.Now;
                wo.InsertSave<WorkOrderManual>();
                ValidateInterval(time, hour, categoryAssetID, IKCode);

            }

        }
        public virtual ActionResult GenerateManual(string categoryAssetID, string IKCode)
        {
            MasterAssetCategory assetCategory = MasterAssetCategory.GetMasterAssetByID(categoryAssetID);
            SelectList selectlist = new SelectList();
            MasterIK ik = MasterIK.GetIKByID(IKCode);
            string id = assetCategory.Departement + "-" + assetCategory.AssetCategory + "-" + DateTime.Now.ToString("yyyy") + "-" + DateTime.Now.ToString("MM") + "-" + WorkOrderManual.countWorkOrderzeroformat();
            WorkOrderManual workorder = new WorkOrderManual();
            workorder.WorkOderManualID = id;
            workorder.WorkOrderDate = DateTime.Now;
            workorder.CategoryAssetID = assetCategory.AssetCategoryID;
            workorder.AssetLocationCode = assetCategory.Location;
            workorder.Status = "Open";
            workorder.StartedBy = Session["UserId"].ToString();
            workorder.IKCode = ik.IKCode;
            workorder.CategoryDescription = assetCategory.Description;
            workorder.AssetLocationCode = assetCategory.Location;
            workorder.WorkOrderLeader = assetCategory.UserGroupHeader.UserGroupLeader;
            workorder.IKDescription = ik.IKDescription;
            workorder.InsertSave<WorkOrderManual>();
            ViewBag.assettypeselectlist = selectlist.GetAssetTypeSelectList(categoryAssetID);
            ViewData.Model = workorder;
            ViewBag.ikCategory = MasterIK.GetCategoryByIKCode(IKCode).ToList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult GenerateManual(WorkOrderManual model, string[] completedby, string[] startedby, string[] IKCategory, string[] vendorname, string[] condition, string[] IKItem, string[] workstartdate, string[] workenddate, string[] picremarks, string[] assettypeid, string[] maintenancetype, string[] maintenancejumlah, string[] maintenancesatuan, string[] maintenanceremarks, DateTime?[] maintenancedate, string[] ikremarks, string[] IKCategoryfor, string btnNames, FormCollection frm, string[] grn,string[] nominal)
        {
            //insert condition
            var ikConditionCurrent = WorkOrderManual.GetAllWorkOrderCondition(model.WorkOderManualID).ToList();
            if (ikConditionCurrent != null)
            {
                foreach (var item in ikConditionCurrent)
                {
                    item.Delete<IKCondition>();
                }
            }

            for (int i = 0; i < IKItem.Length; i++)
            {
                var cond = frm["cond" + IKItem[i]];

                //if (i < condition.Length)
                //{
                //    IKCondition conditions = new IKCondition();
                //    conditions.IKitemID = int.Parse(IKItem[i]);
                //    conditions.Condition = condition[i];
                //    conditions.WorkOrderManualID = model.WorkOderManualID;
                //    conditions.Remarks = ikremarks[i];
                //    conditions.IKCategory = IKCategoryfor[i];
                //    conditions.InsertSave<IKCondition>();
                //}
                if (cond != null)
                {
                    IKCondition conditions = new IKCondition();
                    conditions.IKitemID = int.Parse(IKItem[i]);
                    conditions.Condition = cond;
                    conditions.WorkOrderManualID = model.WorkOderManualID;
                    conditions.Remarks = ikremarks[i];
                    conditions.IKCategory = IKCategoryfor[i];
                    conditions.InsertSave<IKCondition>();
                }
            }
            //Delete current vendor
            var vendorCurrent = WorkOrderManual.GetAllVendor(model.WorkOderManualID).ToList();
            if (vendorCurrent.Count() > 0)
            {
                foreach (var items in vendorCurrent)
                {
                    items.Delete<IKConditionVendor>();
                }
            }

            //insert vendor
            for (int a = 0; a < IKCategory.Length; a++)
            {
                IKConditionVendor vendor = new IKConditionVendor();
                vendor.VendorName = vendorname[a];
                vendor.StartedBy = startedby[a];
                //if (completedby != null)
                //{
                    vendor.CompletedBy = completedby[a];
                //}

                if (!string.IsNullOrEmpty(workstartdate[a]))
                {
                    vendor.WorkStartDate = DateTime.Parse(workstartdate[a]);

                }
                if (!string.IsNullOrEmpty(workenddate[a]))
                {
                    vendor.WorkEndDate = DateTime.Parse(workenddate[a]);
                }

                vendor.PICRemarks = picremarks[a];
                vendor.WorkOrderManual = model.WorkOderManualID;
                vendor.IKCategory = IKCategory[a];
                vendor.InsertSave<IKConditionVendor>();
            }
            //delete current maintenance
            var maintenanceCurrent = WorkOrderManual.GetAllMaintenance(model.WorkOderManualID).ToList();
            foreach (var current in maintenanceCurrent)
            {
                current.Delete<WorkOrderManualMaintenance>();
            }

            for (int b = 0; b < assettypeid.Length; b++)
            {
                WorkOrderManualMaintenance maintenance = new WorkOrderManualMaintenance();
                maintenance.AssetTypeID = assettypeid[b];
                maintenance.maintenanceType = maintenancetype[b];
                maintenance.Jumlah = maintenancejumlah[b];
                maintenance.Satuan = maintenancesatuan[b];
                maintenance.Remarks = maintenanceremarks[b];
                maintenance.maintenanceDate = maintenancedate[b];
                maintenance.GRN = grn[b];
                maintenance.Nominal = decimal.Parse(nominal[b]);
                maintenance.WorkOrderManualID = model.WorkOderManualID;
                maintenance.InsertSave<WorkOrderManualMaintenance>();
            }
            if (btnNames == "save")
            {
                var WorkOrder = WorkOrderManual.GetWorkOderByID(model.WorkOderManualID);
                if (WorkOrder != null)
                {
                    WorkOrder.Status = "Save";
                    WorkOrder.sorting = 2;
                    WorkOrder.CompleteBy = Session["UserId"].ToString();
                    WorkOrder.UpdatedBy = Session["UserId"].ToString();
                    WorkOrder.UpdatedDate = DateTime.Now;
                    WorkOrder.UpdateSave<WorkOrderManual>();
                }

            }
            if (btnNames == "submit")
            {
                var WorkOrder = WorkOrderManual.GetWorkOderByID(model.WorkOderManualID);
                //insert workflow
                if (WorkOrder != null)
                {
                    WorkOrder.Status = "Submit";
                    WorkOrder.sorting = 4;
                    WorkOrder.CompleteBy = Session["UserId"].ToString();
                    WorkOrder.UpdatedBy = Session["UserId"].ToString();
                    WorkOrder.UpdatedDate = DateTime.Now;
                    WorkOrder.UpdateSave<WorkOrderManual>();


                    var usrGroup = MasterAssetCategory.GetUserGroupByCategoryID(model.CategoryAssetID);
                    var approvalGroup = ApprovalGroup.GetAllApprovalGroupByGroupID(usrGroup).ToList();

                    foreach (var items in approvalGroup)
                    {
                        WorkFlow wf = new WorkFlow();
                        wf.WorkFlowApproval = Session["UserId"].ToString();
                        wf.WorkFlowStatus = "S";
                        wf.WorkFlowApproval = items.ApprovalId;
                        wf.WorkFlowWorkOrderID = model.WorkOderManualID;
                        wf.CreatedBy = Session["UserId"].ToString();
                        wf.CreatedDate = DateTime.Now;
                        wf.InsertSave<WorkFlow>();
                    }

                    var vendor = WorkOrderManual.GetVendorByWOID(model.WorkOderManualID).ToList();
                    if (vendor != null)
                    {
                        foreach (var item in vendor)
                        {
                            item.WorkEndDate = item.WorkEndDate;
                            //item.CompletedBy = Session["UserId"].ToString();
                            item.UpdateSave<IKConditionVendor>();
                        }
                    }

                }

                return RedirectToAction(MVC.WorkOrder.WorkOrder());
            }
            return RedirectToAction(MVC.WorkOrder.WorkOrder());
        }
        public virtual ActionResult EditWorkOrder(string id)
        {
            var WorkoderData = WorkOrderManual.GetWorkOderByID(id);
            ViewData.Model = WorkoderData;
            ViewBag.category = MasterIK.GetCategoryByIKCode(WorkoderData.IKCode).ToList();
            ViewBag.condition = WorkOrderManual.GetIKConditionByWOID(id).ToList();
            var usrGroup = MasterAssetCategory.GetUserGroupByCategoryID(WorkoderData.CategoryAssetID);
            var approvalGroup = ApprovalGroup.GetAllApprovalGroupByGroupID(usrGroup).ToList();
            string approver = string.Empty;
            foreach (var item in approvalGroup)
            {
                var approverName = MasterUser.GetUserByID(item.ApprovalId);
                //populate leader
                if (!string.IsNullOrEmpty(approver))
                {
                    approver = approver + "," + approverName.Name;
                }
                else
                {
                    approver = approver + approverName.Name;
                }
            }
            ViewBag.approver = approver;
            var vendor = WorkOrderManual.GetVendorByWOID(id).ToList();
            ViewBag.vendor = vendor;
            ViewBag.maintenance = WorkOrderManual.GetAllMaintenance(id).ToList();
            SelectList selectlist = new SelectList();
            ViewBag.assettypeselectlist = selectlist.GetAssetTypeSelectList(WorkoderData.CategoryAssetID);
            var name = selectlist.GetUserIDNameSelectList();
            //foreach (var item in vendor)
            //{
            //    foreach (var items in name)
            //    {
            //        if (item.CompletedBy == items.Text)
            //        {
            //            items.Selected = true;
            //        }
            //    }
            //}
            ViewBag.userName = name;
            return View();
        }
    }
}