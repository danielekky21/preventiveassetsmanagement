﻿using PreventiveAssetManagement.dataRepo;
using PreventiveAssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    [SessionTimeout]
    public partial class UserGroupController : Controller
    {
        // GET: UserGroup
        public virtual ActionResult Index()
        {
            SelectList select = new SelectList();
            ViewBag.active = select.GetActiveSelectList();
            return View();
        }
        public virtual JsonResult GetGridUserGroup(int draw, int start, int length, string groupid, string leader,string status)
        {
            int sortColumn = -1;

            var Data = UserGroupHeader.GetAllGroupHeader().ToList();
          
            if (!string.IsNullOrEmpty(groupid))
            {
                Data = Data.Where(x => x.UserGroupId.ToLower().Contains(groupid.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(leader))
            {
                Data = Data.Where(x => x.UserGroupName.ToLower().Contains(leader.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(status))
            {
                bool stat = bool.Parse(status);
                Data = Data.Where(x => x.IsActive == stat).ToList();
            }
            int countTotal = Data.Count;
            if (Data.Count > 10)
            {
                Data = Data.GetRange(start, Math.Min(length, Data.Count - start));
            }

            string sortDirection = "asc";
            if (length == -1)
            {
                length = countTotal;
            }
            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = countTotal;
            dataTableData.ListData = AssignUserGroupData(Data);

            int recordsFiltered = countTotal;
            return Json(new
            {
                // this is what datatables wants sending back
                draw = dataTableData.draw,
                recordsTotal = 10,
                recordsFiltered = recordsFiltered,
                data = dataTableData.ListData
            }, JsonRequestBehavior.AllowGet);


        }
        private List<DataItem> AssignUserGroupData(List<UserGroupHeader> data)
        {
            List<DataItem> dataitem = new List<DataItem>();

            foreach (var item in data)
            {
                DataItem items = new DataItem();
                items.UserGroupId = item.UserGroupId;
                items.UserGroupName = item.UserGroupName;
                items.UserGroupLeader = item.UserGroupLeader;
                if (item.IsActive.GetValueOrDefault())
                {
                    items.Status = "Active";
                }
                else
                {
                    items.Status = "In Active";
                }
                dataitem.Add(items);
            }
            return dataitem;
        }
        public virtual ActionResult UserGroupAddEdit(string id)
        {
            SelectList selectList = new SelectList();
            ViewBag.userSelectlist = selectList.GetActiveUserSelectList();
            ViewBag.CurrentGroupUser = new List<UserGroupDetail>();
            var userData = MasterUser.GetAllUser().ToList();
            if (!string.IsNullOrEmpty(id))
            {
                var headerid = UserGroupHeader.GetGroupHeaderByID(id);
                if (headerid != null)
                {
                    var UserDetaildata = UserGroupHeader.GetUserGroupDetailByHeaderID(headerid.UserGroupId).ToList();
                    foreach (var item in UserDetaildata)
                    {
                        var sameUser = userData.FirstOrDefault(x => x.UserID == item.UserID);
                        if (sameUser != null)
                        {
                            userData.Remove(sameUser);
                        }
                    }
                    ViewData.Model = headerid;
                    ViewBag.CurrentGroupUser = UserGroupHeader.GetUserGroupDetailByHeaderID(id).ToList();
                    ViewBag.ApprovalGroup = ApprovalGroup.GetAllApprovalGroupByGroupID(id).ToList();
                }
            }
            else
            {
                ViewBag.ApprovalGroup = new List<ApprovalGroup>();
                ViewData.Model = new UserGroupHeader();
            }
            ViewBag.Userdata = userData;
            
            ViewBag.Status = selectList.GetActiveSelectList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult UserGroupAddEdit(UserGroupHeader model, string[] mutliselect_to ,string[] approvalGroup)
        {
            string loginID = Session["UserId"].ToString();
            if (!string.IsNullOrEmpty(model.UserGroupId))
            {
                var currentData = UserGroupHeader.GetGroupHeaderByID(model.UserGroupId);
                if (currentData != null)
                {
                    //new data
                    currentData.UserGroupLeader = model.UserGroupLeader;
                    currentData.UserGroupName = model.UserGroupName;
                    currentData.IsActive = model.IsActive;
                    currentData.UserGroupLeader = model.UserGroupLeader;
                    currentData.UpdateSave<UserGroupHeader>();
                    var currentDetailData = UserGroupHeader.GetUserGroupDetailByHeaderID(currentData.UserGroupId).ToList();
                    
                    //remove existing user data in user group detail
                    foreach (var item in currentDetailData)
                    {
                        item.Delete<UserGroupDetail>();
                    }
                    //insert new fresh data
                    if (mutliselect_to != null)
                    {
                        for (int i = 0; i < mutliselect_to.Count(); i++)
                        {
                            UserGroupDetail usergroupdetail = new UserGroupDetail();
                            usergroupdetail.UserGroupID = currentData.UserGroupId;
                            usergroupdetail.UserID = mutliselect_to[i];
                            usergroupdetail.CreatedDate = DateTime.Now;
                            usergroupdetail.CreratedBy = loginID;
                            usergroupdetail.InsertSave<UserGroupDetail>();
                        }
                    }
                    
                    var currentApprovalData = ApprovalGroup.GetAllApprovalGroupByGroupID(model.UserGroupId).ToList();
                    //remove approval group item
                    foreach (var item in currentApprovalData)
                    {
                        item.Delete<ApprovalGroup>();
                    }
                    if (approvalGroup != null )
                    {
                        for (int i = 0; i < approvalGroup.Count(); i++)
                        {
                            ApprovalGroup appData = new ApprovalGroup();
                            appData.ApprovalUserGroup = model.UserGroupId;
                            appData.ApprovalId = approvalGroup[i];
                            appData.InsertSave<ApprovalGroup>();
                        }
                    }
                    
                }
                else
                {
                    UserGroupHeader newData = new UserGroupHeader();
                    newData.UserGroupId = model.UserGroupId;
                    newData.UserGroupName = model.UserGroupName;
                    newData.UserGroupLeader = model.UserGroupLeader;
                    newData.IsActive = model.IsActive;
                    newData.InsertSave<UserGroupHeader>();
                    //insert new fresh user group detail data
                    if (mutliselect_to != null)
                    {
                        for (int i = 0; i < mutliselect_to.Count(); i++)
                        {
                            UserGroupDetail usergroupdetail = new UserGroupDetail();
                            usergroupdetail.UserGroupID = model.UserGroupId;
                            usergroupdetail.UserID = mutliselect_to[i];
                            usergroupdetail.CreatedDate = DateTime.Now;
                            usergroupdetail.CreratedBy = loginID;
                            usergroupdetail.InsertSave<UserGroupDetail>();
                        }
                    }

                    if (approvalGroup != null)
                    {
                        //insert new fresh approval group
                        for (int i = 0; i < approvalGroup.Count(); i++)
                        {
                            ApprovalGroup appData = new ApprovalGroup();
                            appData.ApprovalUserGroup = model.UserGroupId;
                            appData.ApprovalId = approvalGroup[i];
                            appData.InsertSave<ApprovalGroup>();
                        }
                    }
                    
                }
            }
            return RedirectToAction(MVC.UserGroup.Index());
        }
        public virtual JsonResult CheckUserGroupCode(string search_keyword)
        {
            bool result = UserGroupHeader.CheckUserGroup(search_keyword.ToLower());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}