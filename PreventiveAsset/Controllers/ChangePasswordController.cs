﻿using PreventiveAssetManagement.dataRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreventiveAsset.Controllers
{
    [SessionTimeout]
    public partial class ChangePasswordController : Controller
    {
        // GET: ChangePassword
        public virtual ActionResult Index()
        {
            var userid = @Session["UserId"].ToString();
            var User = MasterUser.GetUserByID(userid);
            User.Password = PreventiveAssetManagement.commons.Security.DataEncription.Decrypt(User.Password);
            ViewData.Model = User;
            return View();
        }
        [HttpPost]
        public virtual ActionResult ChangePassword(string UserId, string Password)
        {
            var user = MasterUser.GetUserByID(UserId);
            if (user != null)
            {
                user.Password = PreventiveAssetManagement.commons.Security.DataEncription.Encrypt(Password);
                user.UpdatedDate = DateTime.Now;
                user.UpdatedBy = user.Name;
                user.UpdateSave<MasterUser>();
            }
            return RedirectToAction(MVC.ChangePassword.Index());
        }
    }
}